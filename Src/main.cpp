// Copyright RariumUnlimited - Licence : MIT
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include <AppBuilder.h>
#include <Core/Application/Application.h>
#include <Core/Assets/AssetManager.h>
#include <Core/Log.h>
#include <Core/Window/Window.h>

using namespace Mira;

int main(int argc, char* argv[])
{
    Mira::Logger::Log().AddStream(&std::cout);
    Mira::AssetManager assetManager("./Assets/");
    Mira::AssetManagerProxy::OpenProxy(assetManager);
    std::vector<std::unique_ptr<Mira::Application>> appList;
    Mira::AppBuilder().GetAllApplication(appList);
    Mira::Window window(appList);

    try 
    {
        window.Run();
    } 
    catch(std::string str) 
    {
        std::cerr << str << std::endl;
    }

    Mira::AssetManagerProxy::CloseProxy(assetManager);
}


