// Copyright RariumUnlimited - Licence : MIT
#include <EleCapture/IA/Random.h>

#include <chrono>
#include <vector>

namespace Mira::EleCapture 
{

Random::Random() 
{
    m_Generator = std::mt19937(static_cast<unsigned int>(std::chrono::system_clock::now().time_since_epoch().count()));
}

Player::Position Random::TakeTurn(const Board& b,
                                  const GameHelper* helper,
                                  const std::set<Position>& possibleCapture,
                                  const std::set<Position>& yourTiles,
                                  const std::set<Position>& opponentTiles) 
{
    std::vector<Position> v(possibleCapture.begin(), possibleCapture.end());

    using UIntDistrib = std::uniform_int_distribution<unsigned int>;
    UIntDistrib distrib(0, (unsigned int)v.size() - 1);

    return v[distrib(m_Generator)];
}

}  // namespace Mira::EleCapture
