// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <map>
#include <random>
#include <set>

#include <EleCapture/Game/Player.h>

namespace Mira::EleCapture 
{

// A Monte-Carlo IA
class MC : public Player 
{
public:
    MC();

    Position TakeTurn(const Board& b,
                      const GameHelper* helper,
                      const std::set<Position>& possibleCapture,
                      const std::set<Position>& yourTiles,
                      const std::set<Position>& opponentTiles) override;

private:
    bool PlayGame(const GameHelper* helper,
                  const std::set<Position>& yourTiles,
                  const std::set<Position>& opponentTiles,
                  bool IATurn);


    static constexpr unsigned int K_SIMULATION_COUNT = 10000;
    std::mt19937 m_Generator;
};

}  // namespace Mira::EleCapture
