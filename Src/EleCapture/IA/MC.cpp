// Copyright RariumUnlimited - Licence : MIT
#include <EleCapture/IA/MC.h>

#include <omp.h>

#include <atomic>
#include <chrono>
#include <utility>
#include <vector>

namespace Mira::EleCapture 
{

MC::MC() 
{
    m_Generator = std::mt19937(static_cast<unsigned int>(std::chrono::system_clock::now().time_since_epoch().count()));
}

Player::Position MC::TakeTurn(const Board& b,
                              const GameHelper* helper,
                              const std::set<Position>& possibleCapture,
                              const std::set<Position>& yourTiles,
                              const std::set<Position>& opponentTiles) 
{
    std::vector<Position> captureVector(possibleCapture.begin(), possibleCapture.end());
    std::vector<std::set<Position>> newYourTilesVector(captureVector.size());
    std::vector<std::pair<std::atomic_uint, std::atomic_uint>> playWinVector(captureVector.size());  // <Play, Win>

    for (auto i = 0; i < captureVector.size(); ++i) 
    {
        newYourTilesVector[i] = yourTiles;
        newYourTilesVector[i].insert(captureVector[i]);
        playWinVector[i].first = 0;
        playWinVector[i].second = 0;
    }

    #pragma omp parallel for
    for (int i = 0; i < K_SIMULATION_COUNT; ++i) 
    {
        using UIntDistrib = std::uniform_int_distribution<unsigned int>;
        unsigned int play = UIntDistrib(0, (unsigned int)captureVector.size() - 1)(m_Generator);
        bool won = PlayGame(helper, newYourTilesVector[play], opponentTiles, false);
        ++playWinVector[play].first;
        if (won)
        {
            ++playWinVector[play].second;
        }
    }

    float currentRatio = 0.f;
    Position bestPosition = {0, 0};

    for (auto i = 0; i < captureVector.size(); i++) 
    {
        float ratio = static_cast<float>(playWinVector[i].second) /
                      static_cast<float>(playWinVector[i].first);
        if (ratio >= currentRatio) 
        {
            currentRatio = ratio;
            bestPosition = captureVector[i];
        }
    }

    return bestPosition;
}

bool MC::PlayGame(const GameHelper* helper,
                  const std::set<Position>& yourTiles,
                  const std::set<Position>& opponentTiles,
                  bool IATurn) 
{
    bool won;

    if (yourTiles.size() + opponentTiles.size() == Board::K_WIDTH * Board::K_HEIGHT) 
    {
        won = helper->DidIWin(yourTiles, opponentTiles);
    } 
    else 
    {
        Position nextCapture;
        {
            std::set<Position> possibleCapture;
            if (IATurn)
            {
                possibleCapture = helper->GetPossibleCapture(yourTiles, opponentTiles);
            }
            else
            {
                possibleCapture = helper->GetPossibleCapture(opponentTiles, yourTiles);
            }

            std::vector<Position> captureVector(possibleCapture.begin(), possibleCapture.end());
            using UIntDistrib = std::uniform_int_distribution<unsigned int>;
            unsigned int play = UIntDistrib(0, (unsigned int)captureVector.size() - 1)(m_Generator);
            nextCapture = captureVector[play];
        }

        std::set<Position> newYourTiles = yourTiles;
        std::set<Position> newOpponentTiles = opponentTiles;
        if (IATurn)
        {
            newYourTiles.insert(nextCapture);
        }
        else
        {
            newOpponentTiles.insert(nextCapture);
        }

        won = PlayGame(helper, newYourTiles, newOpponentTiles, !IATurn);
    }

    return won;
}

}  // namespace Mira::EleCapture
