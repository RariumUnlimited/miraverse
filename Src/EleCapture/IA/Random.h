// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <random>
#include <set>

#include <EleCapture/Game/Player.h>

namespace Mira::EleCapture 
{

// A random IA
class Random : public Player 
{
public:
    Random();

    Position TakeTurn(const Board& b,
                      const GameHelper* helper,
                      const std::set<Position>& possibleCapture,
                      const std::set<Position>& yourTiles,
                      const std::set<Position>& opponentTiles) override;

private:
    std::mt19937 m_Generator;
};

}  // namespace Mira::EleCapture
