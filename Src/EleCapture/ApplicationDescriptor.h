// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <memory>
#include <vector>

#include <Core/Application/ApplicationDescriptor.h>
#include <EleCapture/EleCaptureApp.h>

namespace Mira 
{

class EleCaptureApplicationDescriptor : public ApplicationDescriptor 
{
public:
    void AddModuleApplicationToList(std::vector<std::unique_ptr<Application>>& applicationList) const override
    {
        applicationList.emplace_back(new EleCapture::EleCaptureApp);
    }
};

}  // namespace Mira
