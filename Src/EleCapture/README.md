Elemental Capture
=================

About the game
--------------

Turn-by-turn tile based board game. Each tile contains different amount of elements (fire, water, wind, earth, metal), player and IA will have to capture a tile turn by turn. When all tiles are captured, for each element the player/IA which captured the most will win that element, the player/IA that win the most element wins the game.

When it is the player turn, the text drawn on tiles is white with black outlines, when it is not the text is black with white outlines. A tile can only be captured if it touches another tile already captured. Player tiles have a red filter, IA tiles have a black filter.
