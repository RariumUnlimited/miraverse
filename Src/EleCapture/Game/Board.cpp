// Copyright RariumUnlimited - Licence : MIT
#include <EleCapture/Game/Board.h>

#include <chrono>
#include <random>

namespace Mira::EleCapture 
{

void Board::Init() 
{
    using UIntDistrib = std::uniform_int_distribution<unsigned int>;
    std::mt19937 generator(static_cast<unsigned int>(std::chrono::system_clock::now().time_since_epoch().count()));
    UIntDistrib distrib(0, K_MAX_ELEMENT_AMOUNT);
    for (auto i = 0; i < K_WIDTH * K_HEIGHT; ++i) 
    {
        m_Tiles[i].m_Fire = distrib(generator);
        m_Tiles[i].m_Water = distrib(generator);
        m_Tiles[i].m_Wind = distrib(generator);
        m_Tiles[i].m_Earth = distrib(generator);
        m_Tiles[i].m_Metal = distrib(generator);
    }
}

}  // namespace Mira::EleCapture
