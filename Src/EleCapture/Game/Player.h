// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <utility>
#include <set>

#include <EleCapture/Game/Board.h>
#include <EleCapture/Game/GameHelper.h>

namespace Mira::EleCapture 
{

class Player 
{
public:
    virtual ~Player() {}

    using Position = std::pair<unsigned int, unsigned int>;

    virtual Position TakeTurn(const Board& b,
                              const GameHelper* helper,
                              const std::set<Position>& possibleCapture,
                              const std::set<Position>& yourTiles,
                              const std::set<Position>& opponentTiles) = 0;

    virtual void NotifyCapture(const GameHelper* helper,
                               const std::set<Position>& yourTiles,
                               const std::set<Position>& opponentTiles) {}

    virtual void NotifyGameEnd(const GameHelper* helper, bool win) {}
};

}  // namespace Mira::EleCapture
