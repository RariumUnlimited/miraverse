// Copyright RariumUnlimited - Licence : MIT
#include <EleCapture/Game/Game.h>

#include <chrono>
#include <functional>
#include <future>

#include <Core/Math.h>

namespace Mira::EleCapture 
{

Game::Game(Board& board, Player* player1, Player* player2) 
    : m_Board(board)
    , m_Player1(player1)
    , m_Player2(player2) 
{
    assert(player1 && player2);
}

void Game::Init() 
{
    m_Player1Tiles.clear();
    m_Player2Tiles.clear();
    m_Player1Tiles.insert({(Board::K_WIDTH - 1) / 2, Board::K_HEIGHT - 1});
    m_Player2Tiles.insert({(Board::K_WIDTH - 1) / 2, 0});
    m_Player1->NotifyCapture(this, m_Player1Tiles, m_Player2Tiles);
    m_Player2->NotifyCapture(this, m_Player2Tiles, m_Player1Tiles);
}

void Game::Run() 
{
    Init();

    m_IsRunning = true;

    bool player1turn = true;

    while (m_IsRunning) 
    {
        std::set<Position> possibleCapture;  // List of possible capture for current player
        std::function<Player::Position()> turnFunction;  // Will hold either player1.TakeTurn or player2.TakeTurn depending on which turn it is

        // Assign the right method depending on which player turn it is
        if (player1turn) 
        {
            possibleCapture = GetPossibleCapture(m_Player1Tiles, m_Player2Tiles);
            turnFunction = std::bind(&Player::TakeTurn,
                                     m_Player1,
                                     m_Board,
                                     this,
                                     possibleCapture,
                                     m_Player1Tiles,
                                     m_Player2Tiles);
        } else 
        {
            possibleCapture = GetPossibleCapture(m_Player2Tiles, m_Player1Tiles);
            turnFunction = std::bind(&Player::TakeTurn,
                                     m_Player2,
                                     m_Board,
                                     this,
                                     possibleCapture,
                                     m_Player2Tiles,
                                     m_Player1Tiles);
        }

        Position playedPosition = turnFunction();

        if (possibleCapture.find(playedPosition) != possibleCapture.end()) 
        {
            if (player1turn)
            {
                m_Player1Tiles.insert(playedPosition);
            }
            else
            {
                m_Player2Tiles.insert(playedPosition);
            }

            player1turn = !player1turn;
            m_Player1->NotifyCapture(this, m_Player1Tiles, m_Player2Tiles);
            m_Player2->NotifyCapture(this, m_Player2Tiles, m_Player1Tiles);
        }

        if (m_Player1Tiles.size() + m_Player2Tiles.size() == Board::K_WIDTH * Board::K_HEIGHT) 
        {
            if (DidIWin(m_Player1Tiles, m_Player2Tiles)) 
            {
               m_Player1->NotifyGameEnd(this, true);
               m_Player2->NotifyGameEnd(this, false);
            } 
            else 
            {
                m_Player1->NotifyGameEnd(this, false);
                m_Player2->NotifyGameEnd(this, true);
            }

            m_IsRunning = false;
        }
    }
}

bool Game::DidIWin(std::set<Position> playerTiles, std::set<Position> opponentTiles) const 
{
    if (playerTiles.size() + opponentTiles.size() < Board::K_WIDTH * Board::K_HEIGHT)
    {
        return false;
    }

    Board::Tile playerElements = GetPlayerTotalElements(playerTiles);
    Board::Tile opponentElements = GetPlayerTotalElements(opponentTiles);

    unsigned int playerMostElement = 0;
    unsigned int opponentMostElement = 0;

    if (playerElements.m_Fire > opponentElements.m_Fire)
    {
        ++playerMostElement;
    }
    else if (opponentElements.m_Fire > playerElements.m_Fire)
    {
        ++opponentMostElement;
    }

    if (playerElements.m_Water > opponentElements.m_Water)
    {
        ++playerMostElement;
    }
    else if (opponentElements.m_Water > playerElements.m_Water)
    {
        ++opponentMostElement;
    }

    if (playerElements.m_Wind > opponentElements.m_Wind)
    {
        ++playerMostElement;
    }
    else if (opponentElements.m_Wind > playerElements.m_Wind)
    {
        ++opponentMostElement;
    }

    if (playerElements.m_Earth > opponentElements.m_Earth)
    {
        ++playerMostElement;
    }
    else if (opponentElements.m_Earth > playerElements.m_Earth)
    {
        ++opponentMostElement;
    }

    if (playerElements.m_Metal > opponentElements.m_Metal)
    {
        ++playerMostElement;
    }
    else if (opponentElements.m_Metal > playerElements.m_Metal)
    {
        ++opponentMostElement;
    }

    if (playerMostElement > opponentMostElement)
    {
        return true;
    }
    else
    {
        return false;
    }
}

Board::Tile Game::GetPlayerTotalElements(std::set<Position> playerTiles) const 
{
    Board::Tile playerElements;

    for (const Position& p : playerTiles) 
    {
        const Board::Tile& t = m_Board.T(p.first, p.second);
        playerElements.m_Fire += t.m_Fire;
        playerElements.m_Water += t.m_Water;
        playerElements.m_Wind += t.m_Wind;
        playerElements.m_Earth += t.m_Earth;
        playerElements.m_Metal += t.m_Metal;
    }

    return playerElements;
}

std::set<Position> Game::GetPossibleCapture(std::set<Position> playerTiles, std::set<Position> opponentTiles) const 
{
    assert(playerTiles.size() + opponentTiles.size() < Board::K_WIDTH * Board::K_HEIGHT);

    std::set<Position> result;
    std::set<Position> freeTiles;

    for (auto i = 0; i < Board::K_WIDTH; ++i) 
    {
        for (auto j = 0; j < Board::K_HEIGHT; ++j) 
        {
            Position p = {i, j};

            if (playerTiles.find(p) == playerTiles.end() &&
                opponentTiles.find(p) == opponentTiles.end()) 
            {
                freeTiles.insert(p);
            }
        }
    }

    for (const Position& p : playerTiles) 
    {
        for (auto i = Clamp<int>(p.first - 1, 0, Board::K_WIDTH - 1);
                  i <= Clamp<int>(p.first + 1, 0, Board::K_WIDTH - 1);
                ++i) 
        {
            Position pTemp = {i, p.second};
            if (freeTiles.find(pTemp) != freeTiles.end())
            {
                result.insert(pTemp);
            }
        }

        unsigned int min = Clamp<int>(p.second - 1, 0, Board::K_HEIGHT - 1);
        unsigned int max = Clamp<int>(p.second + 1, 0, Board::K_HEIGHT - 1);
        for (auto j = min; j <= max; ++j) 
        {
            Position pTemp = {p.first, j};
            if (freeTiles.find(pTemp) != freeTiles.end())
            {
                result.insert(pTemp);
            }
        }
    }

    if (result.size() > 0)
    {
        return result;
    }
    else
    {
        return freeTiles;
    }
}

}  // namespace Mira::EleCapture
