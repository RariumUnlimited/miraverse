// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <atomic>
#include <set>
#include <utility>

#include <EleCapture/Game/Board.h>
#include <EleCapture/Game/GameHelper.h>
#include <EleCapture/Game/Player.h>

namespace Mira::EleCapture 
{

class Game : public GameHelper 
{
public:
    Game(Board& board, Player* player1, Player* player2);

    void Init();
    void Run();
    void Stop() { m_IsRunning = false; }
    std::set<Position> GetPossibleCapture(std::set<Position> playerTiles, std::set<Position> opponentTiles) const override;
    bool DidIWin(std::set<Position> playerTiles, std::set<Position> opponentTiles) const override;
    Board::Tile GetPlayerTotalElements(std::set<Position> playerTiles) const override;

private:
    Board& m_Board;
    Player* m_Player1;
    Player* m_Player2;
    std::set<Position> m_Player1Tiles;
    std::set<Position> m_Player2Tiles;

    std::atomic_bool m_IsRunning;
};

}  // namespace Mira::EleCapture
