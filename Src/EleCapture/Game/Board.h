// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <array>
#include <cassert>

#include <SFML/System.hpp>

namespace Mira::EleCapture 
{

class Board 
{
public:
    struct Tile 
    {
        unsigned int m_Fire = 0;
        unsigned int m_Water = 0;
        unsigned int m_Wind = 0;
        unsigned int m_Earth = 0;
        unsigned int m_Metal = 0;
    };

    static constexpr unsigned int K_WIDTH = 5;
    static constexpr unsigned int K_HEIGHT = 8;
    static constexpr unsigned int K_MAX_ELEMENT_AMOUNT = 200;

    using Tiles = std::array<Tile, K_WIDTH * K_HEIGHT>;

    /// Get the tiles of the board
    const Tiles& GetTiles() const { return m_Tiles; }

    /// Get a tile of the board
    const Tile& T(unsigned int x, unsigned int y) const 
    {
        assert(x >= 0 && y >= 0 && x < K_WIDTH && y < K_HEIGHT);
        return m_Tiles[x + y * K_WIDTH];
    }

    void Init();

private:
    Tiles m_Tiles;  ///< Tile array of the board
};

}  // namespace Mira::EleCapture
