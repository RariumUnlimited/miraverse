// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <set>
#include <utility>

#include <EleCapture/Game/Board.h>

namespace Mira::EleCapture 
{

using Position = std::pair<unsigned int, unsigned int>;

class GameHelper {
public:
    virtual ~GameHelper() {}

    virtual std::set<Position> GetPossibleCapture(std::set<Position> playerTiles, std::set<Position> opponentTiles) const = 0;
    virtual bool DidIWin(std::set<Position> playerTiles, std::set<Position> opponentTiles) const = 0;
    virtual Board::Tile GetPlayerTotalElements(std::set<Position> playerTiles) const = 0;
};

}  // namespace Mira::EleCapture
