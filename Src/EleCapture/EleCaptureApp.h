// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <atomic>
#include <condition_variable>
#include <mutex>
#include <set>
#include <sstream>
#include <string>
#include <thread>

#include <imgui.h>
#include <SFML/Graphics.hpp>

#include <Core/Application/Application.h>
#include <Core/Assets/AssetUser.h>
#include <Core/Event/EventListener.h>
#include <Core/Log.h>
#include <EleCapture/Game/Board.h>
#include <EleCapture/Game/Game.h>
#include <EleCapture/Game/GameHelper.h>
#include <EleCapture/Game/Player.h>
#include <EleCapture/IA/MC.h>
#include <EleCapture/IA/Random.h>

namespace Mira::EleCapture 
{

class EleCaptureApp : public Application, public Player, public AssetUser, public EventListener<EleCaptureApp, MouseButtonPressedEvent>
{
public:
    EleCaptureApp();

    bool RequireGraphicDraw() const override { return true; }
    void OnEvent(const MouseButtonPressedEvent& e);

protected:
    enum class EGameStatus 
    {
        Win,
        Lose,
        PlayerTurn,
        AITurn
    };

    std::string GetGameStatusString(EGameStatus gs);
    void DrawImGuiInternal() override;
    void DrawGraphicInternal(sf::RenderTarget& renderTarget) override;
    void Update(float delta, const Mira::InputState& currentInput, const Mira::InputState& lastInput) override;
    Player::Position TakeTurn(const Board& b,
                              const GameHelper* helper,
                              const std::set<Position>& possibleCapture,
                              const std::set<Position>& yourTiles,
                              const std::set<Position>& opponentTiles) override;
    void NotifyCapture(const GameHelper* helper,
                       const std::set<Position>& yourTiles,
                       const std::set<Position>& opponentTiles) override;
    void NotifyGameEnd(const GameHelper* helper, bool win) override;
    void UpdateStatus(const GameHelper* helper);
    void OnStart() override;
    void OnStop() override;
    sf::Sprite& TSprite(unsigned int x, unsigned int y);
    sf::Text& TText(unsigned int x, unsigned int y);
    const sf::Texture& GetTileTexture(const Board::Tile& t);

private:
    static constexpr int K_TILE_SIZE = 64;  ///< Size in pixel of a tile
    static constexpr int K_WIDTH = Board::K_WIDTH * K_TILE_SIZE;  ///< Width of the window in pixel
    static constexpr int K_HEIGHT = Board::K_HEIGHT * K_TILE_SIZE;  ///< Height of the window in pixel

    std::atomic<EGameStatus> m_GameStatus;
    Board::Tile m_PlayerStatus;
    Board::Tile m_AIStatus;


    Board m_Board;
    MC m_AIMC;
    Game m_Game;
    std::vector<sf::Sprite> m_STiles;  ///< Sprites for each tiles of the board
    std::vector<sf::Text> m_TTiles;  ///< Texts for each tiles of the board
    InputState currentInput;

    std::mutex m_CapturedTilesMutex;  ///< Mutex used when accessing captured/oppenent tiles
    std::set<Position> m_CapturedTiles;  ///< Tiles that the user captured
    std::set<Position> m_OpponentTiles;  ///< Tiles that the opponent captured

    std::atomic_bool m_PlayerTurn;  ///< Set to true if it is the player turn

    std::mutex m_CVMutex;  ///< Mutex locked when using cvTurn
    std::condition_variable m_CVTurn;  ///< Condition variable used to get the tile the player clicked when waiting for him to take his turn
    Position m_PlayedPosition;
    std::thread m_Thread;
};

}  // namespace Mira::EleCapture
