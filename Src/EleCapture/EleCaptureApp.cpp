// Copyright RariumUnlimited - Licence : MIT
#include <EleCapture/EleCaptureApp.h>

#include <Core/Assets/Font.h>
#include <Core/Assets/Texture.h>

namespace Mira::EleCapture 
{

EleCaptureApp::EleCaptureApp() 
    : Application("/Game/Elemental Capture", "Elemental Capture")
    , m_Game(m_Board, this, &m_AIMC) 
{
    m_CloseBehaviour = ECloseBehaviour::Stop;

    OnMouseButtonPressed.RegisterListener(*this);
}

std::string EleCaptureApp::GetGameStatusString(EGameStatus gs)
{
    std::string result = "";

    switch (gs) 
    {
    case EGameStatus::Win:
        result = "You Win !";
        break;
    case EGameStatus::Lose:
        result = "You Lose !";
        break;
    case EGameStatus::PlayerTurn:
        result = "Your Turn !";
        break;
    case EGameStatus::AITurn:
        result = "AI Turn !";
        break;
    }

    return result;
}

void EleCaptureApp::DrawImGuiInternal()
{
    ImGui::Text("Game status : %s", GetGameStatusString(m_GameStatus.load()).c_str());

    ImGui::Separator();

    bool fireAdvantage = m_PlayerStatus.m_Fire > m_AIStatus.m_Fire;
    bool waterAdvantage = m_PlayerStatus.m_Water > m_AIStatus.m_Water;
    bool windAdvantage = m_PlayerStatus.m_Wind > m_AIStatus.m_Wind;
    bool earthAdvantage = m_PlayerStatus.m_Earth > m_AIStatus.m_Earth;
    bool metalAdvantage = m_PlayerStatus.m_Metal > m_AIStatus.m_Metal;

    int fireDiff = m_PlayerStatus.m_Fire - m_AIStatus.m_Fire;
    int waterDiff = m_PlayerStatus.m_Water - m_AIStatus.m_Water;
    int windDiff = m_PlayerStatus.m_Wind - m_AIStatus.m_Wind;
    int earthDiff = m_PlayerStatus.m_Earth - m_AIStatus.m_Earth;
    int metalDiff = m_PlayerStatus.m_Metal - m_AIStatus.m_Metal;

    ImGui::Text("Your elements :");

    if (fireAdvantage)
    {
        ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), "Fire : %d (+%d)", m_PlayerStatus.m_Fire, fireDiff);
    }
    else
    {
        ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "Fire : %d (%d)", m_PlayerStatus.m_Fire, fireDiff);
    }
    if (waterAdvantage)
    {
        ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), "Water : %d (+%d)", m_PlayerStatus.m_Water, waterDiff);
    }
    else
    {
        ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "Water : %d (%d)", m_PlayerStatus.m_Water, waterDiff);
    }
    if (windAdvantage)
    {
        ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), "Wind : %d (+%d)", m_PlayerStatus.m_Wind, windDiff);
    }
    else
    {
        ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "Wind : %d (%d)", m_PlayerStatus.m_Wind, windDiff);
    }
    if (earthAdvantage)
    {
        ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), "Earth : %d (+%d)", m_PlayerStatus.m_Earth, earthDiff);
    }
    else
    {
        ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "Earth : %d (%d)", m_PlayerStatus.m_Earth, earthDiff);
    }
    if (metalAdvantage)
    {
        ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), "Metal : %d (+%d)", m_PlayerStatus.m_Metal, metalDiff);
    }
    else
    {
        ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "Metal : %d (%d)", m_PlayerStatus.m_Metal, metalDiff);
    }

    ImGui::Separator();

    ImGui::Text("Opponent elements :");
    if (!fireAdvantage)
    {
        ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), "Fire : %d (+%d)", m_AIStatus.m_Fire, -fireDiff);
    }
    else
    {
        ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "Fire : %d (%d)", m_AIStatus.m_Fire, -fireDiff);
    }
    if (!waterAdvantage)
    {
        ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), "Water : %d (+%d)", m_AIStatus.m_Water, -waterDiff);
    }
    else
    {
        ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "Water : %d (%d)", m_AIStatus.m_Water, -waterDiff);
    }
    if (!windAdvantage)
    {
        ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), "Wind : %d (+%d)", m_AIStatus.m_Wind, -windDiff);
    }
    else
    {
        ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "Wind : %d (%d)", m_AIStatus.m_Wind, -windDiff);
    }
    if (!earthAdvantage)
    {
        ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), "Earth : %d (+%d)", m_AIStatus.m_Earth, -earthDiff);
    }
    else
    {
        ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "Earth : %d (%d)", m_AIStatus.m_Earth, -earthDiff);
    }
    if (!metalAdvantage)
    {
        ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), "Metal : %d (+%d)", m_AIStatus.m_Metal, -metalDiff);
    }
    else
    {
        ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "Metal : %d (%d)", m_AIStatus.m_Metal, -metalDiff);
    }
}

void EleCaptureApp::DrawGraphicInternal(sf::RenderTarget& renderTarget) 
{
    for (auto j = 0; j < Board::K_HEIGHT; ++j) 
    {
        for (auto i = 0; i < Board::K_WIDTH; ++i) 
        {
            renderTarget.draw(TSprite(i, j));

            if (currentInput.m_MousePosition.x >= i * K_TILE_SIZE &&
                currentInput.m_MousePosition.x <= (i + 1) * K_TILE_SIZE &&
                currentInput.m_MousePosition.y >= j * K_TILE_SIZE &&
                currentInput.m_MousePosition.y <= (j + 1) * K_TILE_SIZE) 
            {
                sf::Text& text = TText(i, j);

                if (m_PlayerTurn) 
                {
                    text.setFillColor(sf::Color::White);
                    text.setOutlineColor(sf::Color::Black);
                } 
                else 
                {
                    text.setFillColor(sf::Color::Black);
                    text.setOutlineColor(sf::Color::White);
                }
                renderTarget.draw(text);
            }
        }
    }

    sf::RectangleShape rect({static_cast<float>(K_TILE_SIZE), static_cast<float>(K_TILE_SIZE) });

    sf::Color playerColor = sf::Color::Red;
    playerColor.a = 127;
    sf::Color opponentColor = sf::Color::Black;
    opponentColor.a = 127;

    std::unique_lock lock(m_CapturedTilesMutex);
    rect.setFillColor(playerColor);
    for (const Position& p : m_CapturedTiles) 
    {
        rect.setPosition({ (float)p.first * K_TILE_SIZE, (float)p.second * K_TILE_SIZE });
        renderTarget.draw(rect);
    }

    rect.setFillColor(opponentColor);
    for (const Position& p : m_OpponentTiles) 
    {
        rect.setPosition({ (float)p.first * K_TILE_SIZE, (float)p.second * K_TILE_SIZE });
        renderTarget.draw(rect);
    }
    lock.unlock();
}

void EleCaptureApp::Update(float delta, const Mira::InputState& currentInput, const Mira::InputState& lastInput) 
{
    this->currentInput = currentInput;
}

Player::Position EleCaptureApp::TakeTurn(const Board& b,
                          const GameHelper* helper,
                          const std::set<Position>& possibleCapture,
                          const std::set<Position>& yourTiles,
                          const std::set<Position>& opponentTiles) 
{
    std::unique_lock lock(m_CapturedTilesMutex);
    m_CapturedTiles = yourTiles;
    this->m_OpponentTiles = opponentTiles;
    lock.unlock();

    m_GameStatus.store(EGameStatus::PlayerTurn);
    UpdateStatus(helper);

    m_PlayerTurn = true;

    std::unique_lock cvLock(m_CVMutex);
    m_CVTurn.wait(cvLock);

    m_GameStatus.store(EGameStatus::AITurn);

    m_PlayerTurn = false;

    return m_PlayedPosition;
}

void EleCaptureApp::NotifyCapture(const GameHelper* helper,
                   const std::set<Position>& yourTiles,
                   const std::set<Position>& opponentTiles) 
{
    std::unique_lock lock(m_CapturedTilesMutex);
    m_CapturedTiles = yourTiles;
    this->m_OpponentTiles = opponentTiles;
    lock.unlock();
}

void EleCaptureApp::NotifyGameEnd(const GameHelper* helper, bool win) 
{
    if (win)
    {
        m_GameStatus.store(EGameStatus::Win);
    }
    else
    {
        m_GameStatus.store(EGameStatus::Lose);
    }

    UpdateStatus(helper);
}

void EleCaptureApp::UpdateStatus(const GameHelper* helper) 
{
    m_PlayerStatus = helper->GetPlayerTotalElements(m_CapturedTiles);
    m_AIStatus = helper->GetPlayerTotalElements(m_OpponentTiles);
}

void EleCaptureApp::OnStart() 
{
    m_Board.Init();



    for (auto j = 0; j < Board::K_HEIGHT; ++j) 
    {
        for (auto i = 0; i < Board::K_WIDTH; ++i)
        {
            const Board::Tile& t = m_Board.T(i, j);
            sf::Sprite& sprite = m_STiles.emplace_back(GetTileTexture(t));
            sprite.setPosition({ (float)i * K_TILE_SIZE, (float)j * K_TILE_SIZE });

            sf::Text& text = m_TTiles.emplace_back(GetAsset<Font>(DefaultFontId).Get());
            text.setPosition({(float)i * K_TILE_SIZE + 2, (float)j * K_TILE_SIZE });
            text.setCharacterSize(11);
            text.setOutlineThickness(1.f);
            std::ostringstream oss;
            oss << "Fire: " << t.m_Fire << "\n" <<
                   "Water: " << t.m_Water << "\n" <<
                   "Wind: " << t.m_Wind << "\n" <<
                   "Earth: " << t.m_Earth << "\n" <<
                   "Metal: " << t.m_Metal;
            text.setString(oss.str());
        }
    }

    m_Thread = std::thread(&Game::Run, &m_Game);
}

void EleCaptureApp::OnStop() 
{
    m_Game.Stop();
    m_CVTurn.notify_one();
    if (m_Thread.joinable())
    {
        m_Thread.join();
    }
}

sf::Sprite& EleCaptureApp::TSprite(unsigned int x, unsigned int y) 
{
    assert(x >= 0 && y >= 0 && x < Board::K_WIDTH && y < Board::K_HEIGHT);
    return m_STiles[x + y * Board::K_WIDTH];
}

sf::Text& EleCaptureApp::TText(unsigned int x, unsigned int y) 
{
    assert(x >= 0 && y >= 0 && x < Board::K_WIDTH && y < Board::K_HEIGHT);
    return m_TTiles[x + y * Board::K_WIDTH];
}

const sf::Texture& EleCaptureApp::GetTileTexture(const Board::Tile& t) 
{
    if (t.m_Fire > t.m_Water &&
        t.m_Fire > t.m_Wind &&
        t.m_Fire > t.m_Earth &&
        t.m_Fire > t.m_Metal)
    {
        return GetAsset<Texture>("EleCapture/Fire.png"_id).Get();
    }
    else if (t.m_Water > t.m_Fire &&
             t.m_Water > t.m_Wind &&
             t.m_Water > t.m_Earth &&
             t.m_Water > t.m_Metal)
    {
        return GetAsset<Texture>("EleCapture/Water.png"_id).Get();
    }
    else if (t.m_Wind > t.m_Fire &&
             t.m_Wind > t.m_Water &&
             t.m_Wind > t.m_Earth &&
             t.m_Wind > t.m_Metal)
    {
        return GetAsset<Texture>("EleCapture/Wind.png"_id).Get();
    }
    else if (t.m_Earth > t.m_Fire &&
             t.m_Earth > t.m_Water &&
             t.m_Earth > t.m_Wind &&
             t.m_Earth > t.m_Metal)
    {
        return GetAsset<Texture>("EleCapture/Earth.png"_id).Get();
    }
    else
    {
        return GetAsset<Texture>("EleCapture/Metal.png"_id).Get();
    }
}

void EleCaptureApp::OnEvent(const MouseButtonPressedEvent& e)
{
    m_PlayedPosition.first = e.m_InputState.m_MousePosition.x / K_TILE_SIZE;
    m_PlayedPosition.second = e.m_InputState.m_MousePosition.y / K_TILE_SIZE;

    m_CVTurn.notify_one();
}

}  // namespace Mira::EleCapture
