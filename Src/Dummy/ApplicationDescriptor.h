// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <memory>
#include <vector>

#include <Core/Application/ApplicationDescriptor.h>
#include <Dummy/DummyApp.h>

namespace Mira 
{

class DummyApplicationDescriptor : public ApplicationDescriptor 
{
public:
    void AddModuleApplicationToList(std::vector<std::unique_ptr<Application>>& applicationList) const override
    {
        applicationList.emplace_back(new Dummy::DummyApp);
    }
};

}  // namespace Mira
