// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <Core/Assets/FileSystem.h>
#include <Core/Assets/AssetUser.h>
#include <Dummy/DummyAsset.h>

namespace Mira
{

class DummyAssetUser : public AssetUser
{
public:
	DummyAssetUser()
		: AssetUser()
	{ 
		PreloadAssets<DummyAsset, "Dummy/Dummy.txt"_id>();
	}

};

}  // namespace Mira
