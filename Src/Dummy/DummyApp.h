// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <limits>
#include <string>
#include <vector>

#include <Core/Application/Application.h>
#include <Core/Event/EventDispatcher.h>
#include <Core/Event/EventListener.h>
#include <Core/Window/InputState.h>
#include <Dummy/DummyAssetUser.h>
#include <Dummy/DummyEvent.h>
#include <Dummy/DummyPersistentObject.h>

namespace Mira::Dummy
{

class DummyApp 
    : public Application
    , public AssetUser
    , public EventListener<DummyApp, DummyEvent, AnotherDummyEvent, KeyPressedEvent, MouseDragEvent>
{
public:
    DummyApp();

    void Update(float delta, const InputState& currentInput, const InputState& lastInput) override;
    void OnEvent(const KeyPressedEvent& e);
    void OnEvent(const MouseDragEvent& e);
    void OnEvent(const DummyEvent& event);
    void OnEvent(const AnotherDummyEvent& event);

protected:
    void DrawImGuiInternal() override;

    bool RequireGraphicDraw() const override { return true; }

    void DrawGraphicInternal(sf::RenderTarget& renderTarget) override;
    void OnStart() override;
    void OnStop() override;
    void OnDisplay() override;
    void OnHide() override;

    void DrawTestAsset();
    void DrawTestEvent();
    void DrawTestPersistence();

private:
    // ====<ImGui input variable>====
    int m_I{ 0 };
    char m_ObjectName[128]{ "" };
    char m_FileName[128]{ "" };
    std::string m_PersistenceMessageToDisplay;
    std::string m_FileContent;
    char m_DummyEventInput[128]{ "A Test" };
    int m_AnotherDummyEventInput = 0;
    char m_RefDummyObjectName[128]{ "" };

    bool m_DrawTestAsset{ true };
    bool m_DrawTestEvent{ true };
    bool m_DrawTestPersistence{ true };

    std::size_t m_SelectedObject { std::numeric_limits<std::size_t>::max() };
    // ====</ImGui input variable>===

    sf::Clock m_OperationClock;
    bool m_DisplayOperatorText;
    std::string m_OperationText;
    sf::Vector2i m_MousePosition;
    sf::Vector2f m_CirclePosition;
    EventDispatcher<DummyEvent> m_DummyEvent;
    EventDispatcher<AnotherDummyEvent> m_AnotherDummyEvent;

    std::vector<DummyAssetUser> m_AssetUser;
};

}  // namespace Mira::Dummy

