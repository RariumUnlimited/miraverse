// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <string>

#include <Core/Id.h>
#include <Core/Persistence/PersistentObject.h>
#include <Core/Persistence/PORef.h>

namespace Mira 
{

class DummyPersistentObject : public PersistentObject
{
public:
	DECLARE_PERSISTENT_TYPE(DummyPersistentObject)

	DummyPersistentObject(const std::string& name) 
		: PersistentObject(name)
	{

	}

	int GetValue() const { return m_Value; }
	void SetValue(int value) { m_Value = value; }

	void Load(SerializerStream& stream) override
	{
		stream >> m_Value;
	}

	void Save(SerializerStream& stream) const override
	{
		stream << m_Value;
	}

protected:
	int m_Value{ 0 };
};

class DummyPersistentObjectSecond : public PersistentObject
{
public:
	DECLARE_PERSISTENT_TYPE(DummyPersistentObjectSecond)

	DummyPersistentObjectSecond(const std::string& name)
		: PersistentObject(name)
	{

	}

	DummyPersistentObject& GetDummy() const { return *m_Dummy; }
	void SetDummy(IdType id) { m_Dummy.LoadReference(id); }

	void Load(SerializerStream& stream) override
	{
		stream >> m_Dummy;
	}

	void Save(SerializerStream& stream) const override
	{
		stream << m_Dummy;
	}

protected:
	PORef<DummyPersistentObject> m_Dummy;
};

}  // namespace Mira
