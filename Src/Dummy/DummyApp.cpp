// Copyright RariumUnlimited - Licence : MIT
#include <Dummy/DummyApp.h>

#include <cstring>
#include <filesystem>
#include <limits>
#include <memory>

#include <imgui.h>

#include <Core/Assets/AssetManager.h>
#include <Core/Log.h>
#include <Core/Util.h>
#include <Dummy/DummyAsset.h>

namespace Mira::Dummy 
{

DummyApp::DummyApp() 
    : Application("/DummyApps/DummyMenu", "Dummy App", ECloseBehaviour::Stop) 
{
    OnKeyPressed.RegisterListener(*this);
    OnMouseDrag.RegisterListener(*this);
    m_DummyEvent.RegisterListener(*this);
    m_AnotherDummyEvent.RegisterListener(*this);
}

void DummyApp::Update(float delta, const InputState& currentInput, const InputState& lastInput) 
{
    m_MousePosition = currentInput.m_MousePosition;
}

void DummyApp::OnEvent(const KeyPressedEvent& e)
{
    if (e.m_Key == sf::Keyboard::Key::D)
    {
        m_CirclePosition.x += 5;
    }
    else if (e.m_Key == sf::Keyboard::Key::Q)
    {
        m_CirclePosition.x -= 5;
    }
    else if (e.m_Key == sf::Keyboard::Key::Z)
    {
        m_CirclePosition.y -= 5;
    }
    else if (e.m_Key == sf::Keyboard::Key::S)
    {
        m_CirclePosition.y += 5;
    }
}

void DummyApp::OnEvent(const MouseDragEvent& e)
{
    if (e.m_Button == sf::Mouse::Button::Left) 
    {
        m_CirclePosition.x = (float)e.m_InputState.m_MousePosition.x;
        m_CirclePosition.y = (float)e.m_InputState.m_MousePosition.y;
    }
}

void DummyApp::DrawImGuiInternal() 
{
    ImGui::Text("Mouse position : %i, %i", m_MousePosition.x, m_MousePosition.y);
    ImGui::Text("Circle position : %f, %f", m_CirclePosition.x, m_CirclePosition.y);

    ImGui::Separator();

    ImGui::Checkbox("Asset", &m_DrawTestAsset);
    if (m_DrawTestAsset)
    {
        DrawTestAsset();
    }

    ImGui::Separator();

    ImGui::Checkbox("Event", &m_DrawTestEvent);
    if (m_DrawTestEvent)
    {
        DrawTestEvent();
    }

    ImGui::Separator();

    ImGui::Checkbox("Persistence", &m_DrawTestPersistence);
    if (m_DrawTestPersistence)
    {
        DrawTestPersistence();
    }
}

void DummyApp::DrawTestAsset()
{
    ImGui::InputText("File", m_FileName, IM_ARRAYSIZE(m_FileName));
    if (ImGui::Button("Show File Content"))
    {
        std::string path(m_FileName);
        if (std::filesystem::exists(path) && std::filesystem::is_regular_file(path))
        {
            DummyAsset& dummy = AssetManagerProxy::Get().GetAsset<DummyAsset>(GetIdFromString(path));
            m_FileContent = dummy.GetContent();
        }
    }
    ImGui::Text("File Content : %s", m_FileContent.c_str());

    ImGui::Separator();

    ImGui::Text("Asset User Count : %u", m_AssetUser.size());
    if (ImGui::Button("Add Asset User"))
    {
        m_AssetUser.push_back(DummyAssetUser());
        m_AssetUser.back().GetAsset<DummyAsset>("Dummy/Dummy.txt"_id);
    }
    if (ImGui::Button("Remove Asset User"))
    {
        m_AssetUser.pop_back();
    }
    if (ImGui::Button("Remove All Asset User"))
    {
        m_AssetUser.clear();
    }
}

void DummyApp::DrawTestEvent()
{
    ImGui::InputText("Dummy Event Input", m_DummyEventInput, IM_ARRAYSIZE(m_DummyEventInput));
    if (ImGui::Button("Send Dummy Event"))
    {
        std::string textToSend = m_DummyEventInput;
        DummyEvent e(textToSend);
        m_DummyEvent.Dispatch(e);
    }

    ImGui::InputInt("Another Dummy Event Input", &m_AnotherDummyEventInput);
    if (ImGui::Button("Send Another Dummy Event"))
    {
        AnotherDummyEvent ae(m_AnotherDummyEventInput);
        m_AnotherDummyEvent.Dispatch(ae);
    }
}

void DummyApp::DrawTestPersistence()
{
    ImGui::InputText("Object Name", m_ObjectName, IM_ARRAYSIZE(m_ObjectName));
    ImGui::InputInt("Object Value", &m_I);

    if (ImGui::Button("Create Object"))
    {
        std::string objectNameStr = std::string(m_ObjectName);
        if (!Exists(GetIdFromString(objectNameStr)))
        {
            DummyPersistentObject& newObject = RequestPersistentObject<DummyPersistentObject>(objectNameStr);
            newObject.SetValue(m_I);
            SavePersistentObject(newObject);
            m_PersistenceMessageToDisplay = "Object " + objectNameStr + " created";
        }
        else
        {
            m_PersistenceMessageToDisplay = "An object with that name already exists";
        }
    }
    if (ImGui::Button("Read Object Value"))
    {
        std::string objectNameStr = std::string(m_ObjectName);
        IdType id = GetIdFromString(objectNameStr);
        if (Exists(id))
        {
            DummyPersistentObject& object = GetObject<DummyPersistentObject>(id);
            m_PersistenceMessageToDisplay = "Object " + objectNameStr + " has value " + ToString(object.GetValue());
        }
        else
        {
            m_PersistenceMessageToDisplay = "An object with that name doesn't exists";
        }
    }

    ImGui::InputText("Other Dummy Id", m_RefDummyObjectName, IM_ARRAYSIZE(m_RefDummyObjectName));
    if (ImGui::Button("Create Second Object"))
    {
        std::string objectNameStr = std::string(m_ObjectName);
        if (!Exists(GetIdFromString(objectNameStr)))
        {
            std::string refObjectNameStr = std::string(m_RefDummyObjectName);
            IdType refObjectId = GetIdFromString(refObjectNameStr);

            if (Exists(refObjectId))
            {
                DummyPersistentObjectSecond& newObject = RequestPersistentObject<DummyPersistentObjectSecond>(objectNameStr);
                newObject.SetDummy(refObjectId);
                SavePersistentObject(newObject);
                m_PersistenceMessageToDisplay = "Object " + objectNameStr + " created";
            }
            else
            {
                m_PersistenceMessageToDisplay = "Referenced dummy id doesn't exists";
            }
        }
        else
        {
            m_PersistenceMessageToDisplay = "An object with that name already exists";
        }
    }

    if (ImGui::Button("Read Second Object Reference Value"))
    {
        std::string objectNameStr = std::string(m_ObjectName);
        IdType id = GetIdFromString(objectNameStr);
        if (Exists(id))
        {
            DummyPersistentObjectSecond& object = GetObject<DummyPersistentObjectSecond>(id);
            m_PersistenceMessageToDisplay = "Object " + objectNameStr + " has value " + ToString(object.GetDummy().GetValue());
        }
        else
        {
            m_PersistenceMessageToDisplay = "An object with that name doesn't exists";
        }
    }

    ImGui::Separator();

    ImGui::Text(m_PersistenceMessageToDisplay.c_str());
}

void DummyApp::OnEvent(const DummyEvent& event)
{
    Logger::Log() << "Received Dummy Event : " << event.GetContent() << "\n";
}

void DummyApp::OnEvent(const AnotherDummyEvent& event)
{
    Logger::Log() << "Received Another Dummy Event : " << event.GetContent() << "\n";
}

void DummyApp::DrawGraphicInternal(sf::RenderTarget& renderTarget)
{
    sf::CircleShape shape(50.f);
    shape.setFillColor(sf::Color::Red);
    shape.setPosition(m_CirclePosition);
    renderTarget.draw(shape);
}

void DummyApp::OnStart() 
{
    Logger::Log() << "Dummy Start !\n";
}

void DummyApp::OnStop() 
{
    Logger::Log() << "Dummy Stop !\n";
}

void DummyApp::OnDisplay() 
{
    Logger::Log() << "Dummy Display !\n";
}

void DummyApp::OnHide() 
{
    Logger::Log() << "Dummy Hide !\n";
}

}  // namespace Mira::Dummy
