// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <string>

namespace Mira 
{

class DummyType { };

inline std::string Serialize(DummyType d)
{
    return "DummyType";
}

inline void Deserialize(DummyType& d, const std::string str)
{
    if (str != "DummyType") 
    {
        throw std::string("DummyType property value serialized is not DummyType : " + str);
    }
}

}  // namespace Mira
