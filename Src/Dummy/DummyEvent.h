// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <string>

namespace Mira
{

class DummyEvent
{
public:
	DummyEvent(std::string content)
		: m_Content(content)
	{

	}

	const std::string& GetContent() const { return m_Content; }

private:
	std::string m_Content;
};

class AnotherDummyEvent
{
public:
	AnotherDummyEvent(int content)
		: m_Content(content)
	{

	}

	int GetContent() const { return m_Content; }

private:
	int m_Content;
};

}  // namespace Mira
