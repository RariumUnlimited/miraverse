// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <Core/Assets/FileSystem.h>

namespace Mira
{

class DummyAsset
{
public:
	DummyAsset(File&& file)
		: m_Content(file.data(), file.size())
	{ 

	}

	const std::string& GetContent() const { return m_Content; }

private:
	std::string m_Content;
};

}  // namespace Mira
