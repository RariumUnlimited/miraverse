// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <atomic>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include <SFML/Graphics.hpp>

#include <Drawing/Window/App.h>

namespace Mira::Drawing 
{

class ImageApp : public App 
{
public:
    explicit ImageApp(const sf::Vector2u& size);

    void Update() final;
    void Draw(sf::RenderTarget &target) final;
    virtual void UpdateBuffer() = 0;

protected:
    sf::Image m_ImageBuffer;

private:
    sf::Texture m_Texture;  ///< Texture that we will use to draw on screen
    sf::Sprite m_Sprite;  ///< Sprite that will be drawn
};

}  // namespace Mira::Drawing
