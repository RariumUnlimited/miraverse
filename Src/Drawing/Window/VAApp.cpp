// Copyright RariumUnlimited - Licence : MIT
#include <Drawing/Window/VAApp.h>

#include <iostream>

namespace Mira::Drawing 
{

void VAApp::Update() 
{
    UpdateBuffer();
}

void VAApp::Draw(sf::RenderTarget &target) 
{
    target.draw(m_Buffer);
}

}  // namespace Mira::Drawing
