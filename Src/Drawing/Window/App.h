// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <atomic>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include <SFML/Graphics.hpp>

#include <Core/Window/InputState.h>

namespace Mira::Drawing 
{

class App 
{
public:
    virtual ~App() { }
    void Launch();
    void Stop();
    virtual void Reset() { }
    virtual void Update() = 0;
    virtual void Draw(sf::RenderTarget& target) = 0;
    virtual void DrawImGui() { }
    virtual void ProcessKeyInput(const InputState& is, sf::Keyboard::Key key) { }
    virtual void ProcessDoubleClickInput(const InputState& is, sf::Mouse::Button button) { }
};

}  // namespace Mira::Drawing
