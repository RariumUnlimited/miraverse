// Copyright RariumUnlimited - Licence : MIT
#include <Drawing/Window/App.h>

namespace Mira::Drawing
{

void App::Stop() 
{

}

void App::Launch() 
{
    Reset();
}

}  // namespace Mira::Drawing
