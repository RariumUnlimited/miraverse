// Copyright RariumUnlimited - Licence : MIT
#include <Drawing/Window/ImageApp.h>

namespace Mira::Drawing 
{

ImageApp::ImageApp(const sf::Vector2u& size)
    : m_ImageBuffer(size)
    , m_Texture(m_ImageBuffer)
    , m_Sprite(m_Texture)
{

}

void ImageApp::Update() 
{
    UpdateBuffer();
}

void ImageApp::Draw(sf::RenderTarget &target) 
{
    m_Texture.update(m_ImageBuffer);
    target.draw(m_Sprite);
}

}  // namespace Mira::Drawing
