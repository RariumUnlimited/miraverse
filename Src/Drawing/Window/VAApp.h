// Copyright RariumUnlimited - Licence : MIT
#ifndef SRC_DRAWING_WINDOW_VAAPP_H_

#define SRC_DRAWING_WINDOW_VAAPP_H_

#include <atomic>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include <SFML/Graphics.hpp>

#include <Drawing/Window/App.h>

namespace Mira::Drawing 
{

// An base app that draw using a vertex array
class VAApp : public App 
{
public:
    VAApp() = default;

    void Update() final;
    void Draw(sf::RenderTarget &target) final;
    virtual void UpdateBuffer() = 0;

protected:
    sf::VertexArray m_Buffer;  ///< Buffer that will need to be updated with shape to draw
};

}  // namespace Mira::Drawing

#endif  // SRC_DRAWING_WINDOW_VAAPP_H_
