// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <memory>
#include <string>

#include <SFML/Graphics.hpp>

#include <Core/Application/Application.h>
#include <Core/Event/EventListener.h>
#include <Drawing/Window/App.h>

namespace Mira::Drawing 
{

class DrawingApp : public Application, public EventListener<DrawingApp, KeyPressedEvent, MouseButtonDoubleClickEvent>
{
public:
    DrawingApp();

    void OnEvent(const KeyPressedEvent& e);
    void OnEvent(const MouseButtonDoubleClickEvent& e);

protected:
    bool RequireGraphicDraw() const override { return true; }

    void DrawImGuiInternal() override;
    void DrawGraphicInternal(sf::RenderTarget& renderTarget) override;
    void Update(float delta, const InputState& currentInput, const InputState& previousInput) override;
    void OnStart() override;
    void OnStop() override;

private:
    void ChangeApp(const InputState& is, sf::Keyboard::Key key);
    void ChangeApp(std::unique_ptr<App> newApp);

    std::unique_ptr<App> m_App;  ///< App used to draw things
};

}  // namespace Mira::Drawing