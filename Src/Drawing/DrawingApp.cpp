// Copyright RariumUnlimited - Licence : MIT
#include <Drawing/DrawingApp.h>

#include <cstring>
#include <limits>
#include <memory>
#include <vector>

#include <imgui.h>

#include <Core/Window/WindowHelper.h>
#include <Drawing/App/Voronoi.h>
#include <Drawing/App/Mandelbrot.h>
#include <Drawing/App/Julia.h>
#include <Drawing/App/ColorMapViewer.h>
#include <Drawing/App/VanKoch.h>
#include <Drawing/App/MultiTable.h>
#include <Drawing/App/Sierpinski.h>

namespace Mira::Drawing 
{

DrawingApp::DrawingApp() : Application("/Misc/Drawing", "Drawing", ECloseBehaviour::Stop) 
{
    OnKeyPressed.RegisterListener(*this);
    OnMouseButtonDoubleClick.RegisterListener(*this);
}

void DrawingApp::DrawGraphicInternal(sf::RenderTarget& renderTarget) 
{
    m_App->Draw(renderTarget);
}

void DrawingApp::OnStart() 
{
    m_App = std::make_unique<Voronoi>(WindowHelperProxy::Get().GetRenderSize(), 10);
    m_App->Launch();
}

void DrawingApp::OnStop() 
{
    if (m_App != nullptr) 
    {
        m_App->Stop();
    }
}

void DrawingApp::Update(float delta, const InputState& currentInput, const InputState& previousInput)
{
    m_App->Update();
}

void DrawingApp::DrawImGuiInternal()
{
    Mira::WindowHelper& window = WindowHelperProxy::Get();

    if (ImGui::Button("Voronoi"))
    {
        ChangeApp(std::make_unique<Voronoi>(window.GetRenderSize(), 10, true));
    }

    ImGui::SameLine();

    if (ImGui::Button("Mandelbrot"))
    {
        ChangeApp(std::make_unique<Mandelbrot>(window.GetRenderSize()));
    }

    if (ImGui::Button("Julia"))
    {
        ChangeApp(std::make_unique<Julia>(window.GetRenderSize()));
    }

    ImGui::SameLine();

    if (ImGui::Button("Color Map Viewer"))
    {
        ChangeApp(std::make_unique<ColorMapViewer>(window.GetRenderSize()));
    }

    if (ImGui::Button("Von Koch"))
    {
        ChangeApp(std::make_unique<VanKoch>(window.GetRenderSize()));
    }

    ImGui::SameLine();

    if (ImGui::Button("Multi Table"))
    {
        ChangeApp(std::make_unique<MultiTable>(window.GetRenderSize()));
    }

    if (ImGui::Button("Sierpinski"))
    {
        ChangeApp(std::make_unique<Sierpinski>(window.GetRenderSize()));
    }

    ImGui::Separator();

    m_App->DrawImGui();
}

void DrawingApp::OnEvent(const KeyPressedEvent& e)
{
    if (m_App != nullptr) 
    {
        m_App->ProcessKeyInput(e.m_InputState, e.m_Key);
    }
}

void DrawingApp::OnEvent(const MouseButtonDoubleClickEvent& e)
{
    if (m_App != nullptr) 
    {
        m_App->ProcessDoubleClickInput(e.m_InputState, e.m_Button);
    }
}

void DrawingApp::ChangeApp(std::unique_ptr<App> newApp)
{
    if (m_App != nullptr) 
    {
        m_App->Stop();
    }

    if (newApp == nullptr)
    {
        throw std::string("[DrawingApp::ChangeApp] new app must not be nullptr");
    }

    m_App = std::move(newApp);
    m_App->Launch();
}

}  // namespace Mira::Drawing
