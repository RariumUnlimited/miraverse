// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <memory>
#include <vector>

#include <Core/Application/ApplicationDescriptor.h>
#include <Drawing/DrawingApp.h>

namespace Mira
{

class DrawingApplicationDescriptor : public ApplicationDescriptor 
{
public:
    void AddModuleApplicationToList(std::vector<std::unique_ptr<Application>>& applicationList) const override
    {
        applicationList.emplace_back(new Drawing::DrawingApp);
    }
};

}  // namespace Mira

