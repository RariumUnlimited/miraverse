// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <mutex>
#include <string>

#include <SFML/Graphics.hpp>

#include <Core/Window/InputState.h>
#include <Drawing/Tools/ColorMap.h>
#include <Drawing/Window/VAApp.h>

namespace Mira::Drawing 
{

class Window;

class MultiTable : public VAApp 
{
public:
    explicit MultiTable(const sf::Vector2u& size);

    void UpdateBuffer() override;
    void DrawImGui() override;

protected:
    void SwitchUseMinUpdateRate();

    /// Reset the app : set Z to 0, Running to true and Iteration to 0 for each pixel; and maxIterations to 0
    void Reset() override;

    int m_Width;
    int m_Height;

    int m_MinUpdateRate;
    bool m_UseMinUpdateRate;

    ColorMap m_ColorMap;

    int CircleVertexCount;

    int m_Table;
    int m_CurrentI;
    int m_Mod;

    float m_R;
    sf::Vector2f m_Center;
};

}  // namespace Mira::Drawing