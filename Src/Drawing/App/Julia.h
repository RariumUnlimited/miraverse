// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <complex>
#include <random>
#include <string>
#include <vector>

#include <SFML/Graphics.hpp>

#include <Core/Window/InputState.h>
#include <Drawing/Tools/ColorMap.h>
#include <Drawing/Window/ImageApp.h>

namespace Mira::Drawing
{

class Window;

// Julia Set : https://en.wikipedia.org/wiki/Julia_set
class Julia : public ImageApp 
{
public:
    explicit Julia(const sf::Vector2u& size);

    void UpdateBuffer() override;
    void DrawImGui() override;

    void ProcessKeyInput(const InputState& is, sf::Keyboard::Key key) override;
    void ProcessDoubleClickInput(const InputState& is, sf::Mouse::Button button) override;

protected:
    void VerticalMove(const InputState& is, sf::Keyboard::Key k);
    void HorizontalMove(const InputState& is, sf::Keyboard::Key k);
    void Zoom(const InputState& is, sf::Keyboard::Key k);
    void RandomizeC();
    void UltraZoom(const InputState& is, sf::Mouse::Button b);

    void Reset() override;
    void ResetLayer(int layer);

    /// Get the C of a pixel
    virtual std::complex<double> GetC(sf::Vector2u pixel, sf::Vector2<double> sample);
    /// Get the Z0 of a pixel
    virtual std::complex<double> GetZ0(sf::Vector2u pixel, sf::Vector2<double> sample);

    sf::Vector3i ColorToVector(const sf::Color& color) { return sf::Vector3i(color.r, color.g, color.b); }

    double m_RMin;
    double m_RMax;
    double m_IMin;
    double m_IMax;
    int m_Iteration;
    int m_Width;
    int m_Height;
    int m_Dimension;
    ColorMap m_ColorMap;
    bool m_IsStopped;
    int m_Msaa;

    int m_MinUpdateRate;
    bool m_UseMinUpdateRate;
    int m_MaxIteration;
    bool m_UseMaxIteration;

    int m_IndexDisplayed;
    std::vector<std::complex<double>> m_CList;

    struct PixelData 
    {
        std::complex<double> m_Z;  ///< Z of the pixel
        std::complex<double> m_C;  ///< C of the pixel
        bool m_IsRunning;  ///< True if we must keep running the iteration for this pixel
        int m_Iteration;  ///< At which iteration we stopped running
    };

    std::mt19937 m_Generator;  ///< A random number generator
    std::uniform_real_distribution<double> m_DistribSample;  ///< Distribution for the point y coordinate

    std::vector<std::vector<PixelData>> m_Data;  ///< z of each pixel, the bool is set to true when
};

}  // namespace Mira::Drawing
