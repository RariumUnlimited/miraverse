// Copyright RariumUnlimited - Licence : MIT
#include <Drawing/App/Julia.h>

#include <imgui.h>
#include <omp.h>

#include <cfloat>
#include <chrono>
#include <cmath>
#include <iostream>
#include <limits>
#include <utility>

#include <Core/Math.h>

namespace Mira::Drawing 
{

Julia::Julia(const sf::Vector2u& size) 
    : ImageApp(size)
    , m_Generator((unsigned int)std::chrono::system_clock::now().time_since_epoch().count())
    , m_DistribSample(0.f, 1.f) 
{
    m_IMin = -1.f;
    m_IMax = 1.f;
    m_Width = size.x;
    m_Height = size.y;
    double temp = 3 * m_Width / (m_Height * 1.5);
    temp /= 2;
    m_RMin = temp * -1;
    m_RMax = temp;
    m_Iteration = 512;
    m_Dimension = 2;
    m_Msaa = 1;
    m_MinUpdateRate = 60;
    m_MaxIteration = 150;
    m_UseMinUpdateRate = false;
    m_UseMaxIteration = false;

    m_ColorMap.GetColorMap().insert(std::make_pair(0.f, sf::Color::Red));
    m_ColorMap.GetColorMap().insert(std::make_pair(0.2f, sf::Color::Yellow));
    m_ColorMap.GetColorMap().insert(std::make_pair(0.4f, sf::Color::Cyan));
    m_ColorMap.GetColorMap().insert(std::make_pair(0.6f, sf::Color::Blue));
    m_ColorMap.GetColorMap().insert(std::make_pair(0.8f, sf::Color::Magenta));
    m_ColorMap.GetColorMap().insert(std::make_pair(1.0f, sf::Color::Green));

    m_IndexDisplayed = 0;
    m_CList.push_back(std::complex<double>(0.285, 0.01));
    m_CList.push_back(std::complex<double>(0.3, 0.5));
    m_CList.push_back(std::complex<double>(-1.417022285618, 0.0099534));
    m_CList.push_back(std::complex<double>(-0.038088, 0.9754633));
    m_CList.push_back(std::complex<double>(0.285, 0.013));
    m_CList.push_back(std::complex<double>(-1.476, 0.0));
    m_CList.push_back(std::complex<double>(-0.4, 0.6));
    m_CList.push_back(std::complex<double>(-0.8, 0.156));
    m_CList.push_back(std::complex<double>(-0.123, 0.745));
}

void Julia::ProcessKeyInput(const InputState& is, sf::Keyboard::Key k) 
{
    switch (k) 
    {
    case sf::Keyboard::Key::Left:
    case sf::Keyboard::Key::Right:
        HorizontalMove(is, k);
        break;
    case sf::Keyboard::Key::Up:
    case sf::Keyboard::Key::Down:
        VerticalMove(is, k);
        break;
    case sf::Keyboard::Key::Z:
    case sf::Keyboard::Key::S:
        Zoom(is, k);
        break;
    default:
        break;
    }
}

void Julia::ProcessDoubleClickInput(const InputState& is, sf::Mouse::Button button) 
{
    UltraZoom(is, button);
}

void Julia::DrawImGui()
{
    ImGui::Text("Arrows: Move");
    ImGui::Text("Z/S: Zoom");
    ImGui::Text("Double Left Click : Zoom to cursor");
    ImGui::Text("Double Right Click : Center to cursor");
    ImGui::Separator();
    ImGui::Text("Iteration: %i", m_Iteration);
    if (ImGui::InputInt("Dimension", &m_Dimension))
    {
        if (m_Dimension < 2)
        {
            m_Dimension = 2;
        }
        Reset();
    }
    if (ImGui::InputInt("MSAA", &m_Msaa))
    {
        if (m_Msaa < 1)
        {
            m_Msaa = 1;
        }
        Reset();
    }

    if (ImGui::Button("Change C"))
    {
        RandomizeC();
    }

    if (m_UseMinUpdateRate)
    {
        if (ImGui::Button("Disable Min Update Rate"))
        {
            m_UseMinUpdateRate = false;
        }
        ImGui::SameLine();
        ImGui::InputInt("Min Update Rate:", &m_MinUpdateRate);
    }
    if (!m_UseMinUpdateRate && ImGui::Button("Enable Min Update Rate"))
    {
        m_UseMinUpdateRate = true;
    }

    if (m_UseMaxIteration)
    {
        if (ImGui::Button("Disable Max Iteration"))
        {
            m_UseMaxIteration = false;
        }
        ImGui::SameLine();
        ImGui::InputInt("Min Update Rate:", &m_MaxIteration);
    }
    if (!m_UseMaxIteration && ImGui::Button("Enable Max Iteration"))
    {
        m_UseMaxIteration = true;
    }

    if (m_IsStopped && ImGui::Button("Continue"))
    {
        m_IsStopped = false;
    }
    if (!m_IsStopped && ImGui::Button("Stop"))
    {
        m_IsStopped = true;
    }
}

std::complex<double> Julia::GetC(sf::Vector2u pixel, sf::Vector2<double> sample) 
{
    return m_CList[m_IndexDisplayed];
}

std::complex<double> Julia::GetZ0(sf::Vector2u pixel, sf::Vector2<double> sample) 
{
    return std::complex<double>(m_RMin + (pixel.x + sample.x) /
                                m_Width * (m_RMax - m_RMin),
                                m_IMin + (pixel.y + sample.y) /
                                m_Height * (m_IMax - m_IMin));
}

void Julia::Reset() 
{
    m_Iteration = 0;
    m_Data.resize(m_Msaa);
    for (int s = 0; s < m_Msaa; ++s) 
    {
        m_Data[s].resize(m_ImageBuffer.getSize().x * m_ImageBuffer.getSize().y);
        ResetLayer(s);
    }
    m_IsStopped = false;
}

void Julia::ResetLayer(int layer) 
{
    #pragma omp parallel for
    for (int i = 0; i < (int)m_ImageBuffer.getSize().x * (int)m_ImageBuffer.getSize().y; ++i)
    {
        sf::Vector2<double> sample(0.0, 0.0);

        if (layer > 0) 
        {
            sample.x = m_DistribSample(m_Generator);
            sample.y = m_DistribSample(m_Generator);
        }

        // Compute each pixel coord from the index
        unsigned int x = i % m_ImageBuffer.getSize().x;
        unsigned int y = (i - x) / m_ImageBuffer.getSize().x;
        m_Data[layer][i].m_Z = GetZ0(sf::Vector2u(x, y), sample);
        m_Data[layer][i].m_C = GetC(sf::Vector2u(x, y), sample);
        m_Data[layer][i].m_IsRunning = true;
        m_Data[layer][i].m_Iteration = 0;
    }
}

void Julia::UpdateBuffer() 
{
    sf::Clock clock;

    if (!m_IsStopped && ((m_Iteration < m_MaxIteration && m_UseMaxIteration)  || !m_UseMaxIteration)) 
    {
        for (int s = 0; s < m_Msaa; ++s) 
        {
            #pragma omp parallel for
            for (int i = 0; i < (int)m_ImageBuffer.getSize().x * (int)m_ImageBuffer.getSize().y; ++i)
            {
                if (m_Data[s][i].m_IsRunning) 
                {
                    ++m_Data[s][i].m_Iteration;
                    m_Data[s][i].m_Z = pow(m_Data[s][i].m_Z, m_Dimension) + m_Data[s][i].m_C;
                    if (std::abs(m_Data[s][i].m_Z) >= 2.0)
                    {
                        m_Data[s][i].m_IsRunning = false;
                    }
                }
            }
        }

        ++m_Iteration;
    }

    #pragma omp parallel for
    for (int i = 0; i < (int)m_ImageBuffer.getSize().x * (int)m_ImageBuffer.getSize().y; ++i)
    {
        unsigned int x = i % m_ImageBuffer.getSize().x;
        unsigned int y = (i - x) / m_ImageBuffer.getSize().x;

        sf::Vector3i colorSum(0, 0, 0);
        float factorSum = 0.0f;

        for (int s = 0; s < m_Msaa; ++s) 
        {
            if (m_Data[s][i].m_Iteration < m_Iteration) 
            {
                float factor = static_cast<float>(m_Data[s][i].m_Iteration) / static_cast<float>(m_Iteration);

                float power = Clamp(static_cast<float>(m_Iteration) / 255.f, 0.f, 1.f);
                power *= 8;
                power += 2;

                factor = 1 - std::pow(1 - factor, power);

                factorSum += factor;

                sf::Color color = m_ColorMap.GetColor(factor);
                float contrastValue = 0.2f;

                color.r *= (unsigned char)std::pow(factor, contrastValue);
                color.g *= (unsigned char)std::pow(factor, contrastValue);
                color.b *= (unsigned char)std::pow(factor, contrastValue);

                colorSum += ColorToVector(color);
            }
        }

        sf::Color colorFinal = sf::Color::Black;

        colorFinal.r = colorSum.x / m_Msaa;
        colorFinal.g = colorSum.y / m_Msaa;
        colorFinal.b = colorSum.z / m_Msaa;

        m_ImageBuffer.setPixel({ x, y }, colorFinal);
    }

    int timeMS = clock.getElapsedTime().asMilliseconds();
    float time = clock.getElapsedTime().asSeconds();

    std::cout << "Julia time : " << time << "s" << std::endl;

    if (timeMS < m_MinUpdateRate && m_UseMinUpdateRate) 
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(m_MinUpdateRate - timeMS));
    }
}

void Julia::VerticalMove(const InputState& is, sf::Keyboard::Key k) 
{
    double delta = m_IMax - m_IMin;
    delta *= 0.1;
    if (k == sf::Keyboard::Key::Up) 
    {
        m_IMin -= delta;
        m_IMax -= delta;
    } 
    else 
    {
        m_IMin += delta;
        m_IMax += delta;
    }
    Reset();
}

void Julia::HorizontalMove(const InputState& is, sf::Keyboard::Key k) 
{
    double delta = m_RMax - m_RMin;
    delta *= 0.1;
    if (k == sf::Keyboard::Key::Left) 
    {
        m_RMin -= delta;
        m_RMax -= delta;
    } 
    else 
    {
        m_RMin += delta;
        m_RMax += delta;
    }
    Reset();
}

void Julia::Zoom(const InputState& is, sf::Keyboard::Key k) 
{
    double deltaX = m_RMax - m_RMin;
    double deltaY = m_IMax - m_IMin;
    deltaX *= 0.1;
    deltaY *= 0.1;

    if (k == sf::Keyboard::Key::Z) 
    {
        m_RMin += deltaX;
        m_RMax -= deltaX;
        m_IMin += deltaY;
        m_IMax -= deltaY;
    } 
    else 
    {
        m_RMin -= deltaX;
        m_RMax += deltaX;
        m_IMin -= deltaY;
        m_IMax += deltaY;
    }
    Reset();
}

void Julia::RandomizeC() 
{
    ++m_IndexDisplayed;
    m_IndexDisplayed %= m_CList.size();
    std::cout << "C = " << m_CList[m_IndexDisplayed].real()
              << " + " << m_CList[m_IndexDisplayed].imag() << "i" << std::endl;

    m_IMin = -1.f;
    m_IMax = 1.f;
    double temp = 3 * m_Width / (m_Height * 1.5);
    temp /= 2;
    m_RMin = temp * -1;
    m_RMax = temp;

    Reset();
}

void Julia::UltraZoom(const InputState& is, sf::Mouse::Button b) 
{
    double deltaX = m_RMax - m_RMin;
    double deltaY = m_IMax - m_IMin;

    double r = static_cast<double>(is.m_MousePosition.x) / static_cast<double>(m_ImageBuffer.getSize().x) * deltaX + m_RMin;
    double i = static_cast<double>(is.m_MousePosition.y) / static_cast<double>(m_ImageBuffer.getSize().y) * deltaY + m_IMin;

    double zoomFactor = 2.0;

    if (b == sf::Mouse::Button::Left)
    {
        zoomFactor *= 7;
    }

    m_RMin = r - deltaX / zoomFactor;
    m_RMax = r + deltaX / zoomFactor;

    m_IMin = i - deltaY / zoomFactor;
    m_IMax = i + deltaY / zoomFactor;

    Reset();
}

}  // namespace Mira::Drawing
