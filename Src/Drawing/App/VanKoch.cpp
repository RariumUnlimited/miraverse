// Copyright RariumUnlimited - Licence : MIT
#include <Drawing/App/VanKoch.h>

#include <imgui.h>
#include <omp.h>

#include <cfloat>
#include <chrono>
#include <cmath>
#include <iostream>
#include <limits>
#include <numbers>
#include <string>

namespace Mira::Drawing 
{

VanKoch::VanKoch(const sf::Vector2u& size) 
{
    m_YMin = -1.f;
    m_YMax = 1.f;
    m_Width = size.x;
    m_Height = size.y;
    double temp = 3 * m_Width / (m_Height * 1.5);
    temp /= 2;
    m_XMin = temp * -1;
    m_XMax = temp;
    m_Iteration = 0;
    m_MaxIteration = 0;

    m_Refresh = true;
}

void VanKoch::ProcessDoubleClickInput(const InputState& is, sf::Mouse::Button button) 
{
    UltraZoom(is, button);
}

void VanKoch::DrawImGui()
{
    ImGui::Text("Iteration: %i", m_Iteration);
    ImGui::Text("Segments: %u", static_cast<unsigned int>(m_Segments.size()));
    ImGui::Text("MaxIteration: %i", m_MaxIteration);
    ImGui::Separator();
    ImGui::Text("Double Left Click: Zoom to cursor");
    if (ImGui::Button("Increase Iteration"))
    {
        IncreaseIteration();
    }
}

void VanKoch::IncreaseIteration() 
{
    ++m_MaxIteration;
}

void VanKoch::UpdateBuffer() 
{
    sf::Clock clock;

    sf::Rect<double> rect({ m_XMin, m_YMin }, { m_XMax - m_XMin, m_YMax - m_YMin });

    if (m_Iteration < m_MaxIteration) 
    {
        std::vector<Segment> newSegments;

#pragma omp parallel
        {
            std::vector<Segment> newSeg_private;
#pragma omp for
            for (int i = 0; i < m_Segments.size(); ++i) 
            {
                Segment& seg = m_Segments[i];

                sf::Vector2<double> c((2.0 * seg.m_A.x + seg.m_B.x) / 3.0, (2.0 * seg.m_A.y + seg.m_B.y) / 3.0);
                sf::Vector2<double> d((seg.m_A.x + 2.0 * seg.m_B.x) / 3.0, (seg.m_A.y + 2.0 * seg.m_B.y) / 3.0);
                sf::Vector2<double> e;
                e.x = c.x + (d.x - c.x) * cos(std::numbers::pi / 3.0) + (d.y - c.y) * sin(std::numbers::pi / 3.0);
                e.y = c.y - (d.x - c.x) * sin(std::numbers::pi / 3.0) + (d.y - c.y) * cos(std::numbers::pi / 3.0);
                if (rect.contains(seg.m_A) || rect.contains(c))
                {
                    newSeg_private.push_back(Segment(seg.m_A, c));
                }
                if (rect.contains(c) || rect.contains(e))
                {
                    newSeg_private.push_back(Segment(c, e));
                }
                if (rect.contains(e) || rect.contains(d))
                {
                    newSeg_private.push_back(Segment(e, d));
                }
                if (rect.contains(d) || rect.contains(seg.m_B))
                {
                    newSeg_private.push_back(Segment(d, seg.m_B));
                }
            }
#pragma omp critical
            newSegments.insert(newSegments.end(), newSeg_private.begin(), newSeg_private.end());
        }

        m_Segments = newSegments;

        ++m_Iteration;
        m_Refresh = true;
    }

    if (m_Refresh) 
    {
        std::vector<sf::Vertex> vertices;

#pragma omp parallel
        {
            std::vector<sf::Vertex> vertices_private;
#pragma omp for
            for (int i = 0; i < m_Segments.size(); ++i) 
            {
                Segment& seg = m_Segments[i];
                if (rect.contains(seg.m_A) || rect.contains(seg.m_B)) 
                {
                    sf::Vector2<double> a = seg.m_A;
                    sf::Vector2<double> b = seg.m_B;

                    a.x = ((a.x - m_XMin) / (m_XMax - m_XMin)) * m_Width;
                    a.y = ((a.y - m_YMin) / (m_YMax - m_YMin)) * m_Height;
                    b.x = ((b.x - m_XMin) / (m_XMax - m_XMin)) * m_Width;
                    b.y = ((b.y - m_YMin) / (m_YMax - m_YMin)) * m_Height;

                    sf::Vertex v1(sf::Vector2f((float)a.x, (float)a.y), sf::Color::White);
                    sf::Vertex v2(sf::Vector2f((float)b.x, (float)b.y), sf::Color::White);
                    vertices_private.push_back(v1);
                    vertices_private.push_back(v2);
                }
            }
#pragma omp critical
            vertices.insert(vertices.end(), vertices_private.begin(), vertices_private.end());
        }

        m_Buffer = sf::VertexArray(sf::PrimitiveType::Lines, vertices.size());

        for (unsigned int i = 0; i < vertices.size(); ++i) 
        {
            m_Buffer[i] = vertices[i];
        }

        m_Refresh = false;
    }

    float time = clock.getElapsedTime().asSeconds();

    std::cout << "VanKoch time : " << time << "s" << std::endl;

    std::this_thread::sleep_for(std::chrono::milliseconds(20));
}


void VanKoch::UltraZoom(const InputState& is, sf::Mouse::Button b) 
{
    if (b != sf::Mouse::Button::Left)
    {
        return;
    }

    double deltaX = m_XMax - m_XMin;
    double deltaY = m_YMax - m_YMin;

    double r = static_cast<double>(is.m_MousePosition.x) / static_cast<double>(m_Width) * deltaX + m_XMin;
    double i = static_cast<double>(is.m_MousePosition.y) / static_cast<double>(m_Height) * deltaY + m_YMin;

    double zoomFactor = 2.0;

    zoomFactor *= 7;

    m_XMin = r - deltaX / zoomFactor;
    m_XMax = r + deltaX / zoomFactor;

    m_YMin = i - deltaY / zoomFactor;
    m_YMax = i + deltaY / zoomFactor;

    m_Refresh = true;
}

void VanKoch::Reset() 
{
    m_Iteration = 0;
    m_Segments.clear();
    Segment s;
    s.m_A = sf::Vector2<double>(-1.0, -0.5);
    s.m_B = sf::Vector2<double>(1.0, 0.5);
    m_Segments.push_back(s);
}

}  // namespace Mira::Drawing
