// Copyright RariumUnlimited - Licence : MIT
#include <Drawing/App/Sierpinski.h>

#include <imgui.h>
#include <omp.h>

#include <cfloat>
#include <chrono>
#include <cmath>
#include <iostream>
#include <limits>
#include <numbers>
#include <string>

namespace Mira::Drawing 
{

Sierpinski::Sierpinski(const sf::Vector2u& size)
    : m_Generator((unsigned int)std::chrono::system_clock::now().time_since_epoch().count())
    , m_Distrib(0.f, 1.f)
    , m_DistribVertex(0, 2) 
{
    m_Triangles.resize(3);
    m_Triangles[0].x = size.x / 8.f;
    m_Triangles[0].y = size.y * 7 / 8.f;

    m_Triangles[1].x = size.x * 7 / 8.f;
    m_Triangles[1].y = size.y * 7 / 8.f;

    m_Triangles[2].x = size.x / 2.f;
    m_Triangles[2].y = size.y / 8.f;

    m_A = m_Triangles[1] - m_Triangles[0];
    m_B = m_Triangles[2] - m_Triangles[0];

    float u1 = m_Distrib(m_Generator);
    float u2 = m_Distrib(m_Generator);

    if (u1 + u2 > 1.f) 
    {
        u1 = 1 - u1;
        u2 = 1 - u2;
    }

    sf::Vector2f w = m_A * u1 + m_B * u2;
    m_CurrentPosition = w + m_Triangles[0];

    m_Buffer = sf::VertexArray(sf::PrimitiveType::Points, 1);
    m_Buffer[0].position = m_CurrentPosition;

    m_Points.push_back(m_CurrentPosition);
}

void Sierpinski::ProcessKeyInput(const InputState& is, sf::Keyboard::Key k) 
{
    switch (k) 
    {
    case sf::Keyboard::Key::Up:
        IncreasePointPerUpdate(is, k);
        break;
    case sf::Keyboard::Key::Down:
        DecreasePointPerUpdate(is, k);
        break;
    default:
        break;
    }
}

void Sierpinski::DrawImGui()
{
    ImGui::Text("Point count: %u", static_cast<unsigned int>(m_Points.size()));
    ImGui::InputInt("Point per update", &m_PointPerUpdate);
}

void Sierpinski::UpdateBuffer() 
{
    sf::Clock clock;

    for (int i = 0; i < m_PointPerUpdate; ++i) 
    {
        sf::Vector2f vertex = m_Triangles[m_DistribVertex(m_Generator)];

        m_CurrentPosition = (m_CurrentPosition + vertex) / 2.f;
        m_Points.push_back(m_CurrentPosition);
    }

    m_Buffer.resize(m_Points.size());

    for (int i = 0; i < m_PointPerUpdate; ++i)
    {
        m_Buffer[m_Points.size() - i - 1].position = m_Points[m_Points.size() - i - 1];
    }

    float time = clock.getElapsedTime().asSeconds();

    std::cout << "Sierpinski time : " << time << "s" << std::endl;

    std::this_thread::sleep_for(std::chrono::milliseconds(20));
}

void Sierpinski::IncreasePointPerUpdate(const InputState& is, sf::Keyboard::Key k) 
{
    ++m_PointPerUpdate;
}

void Sierpinski::DecreasePointPerUpdate(const InputState& is, sf::Keyboard::Key k) 
{
    if (m_PointPerUpdate > 1)
    {
        --m_PointPerUpdate;
    }
}

void Sierpinski::Reset() 
{
    m_Points.clear();
    m_Buffer.resize(0);
}

}  // namespace Mira::Drawing
