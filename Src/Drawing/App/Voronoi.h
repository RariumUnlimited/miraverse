// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <random>
#include <string>
#include <vector>

#include <SFML/Graphics.hpp>

#include <Core/Window/InputState.h>
#include <Drawing/Window/ImageApp.h>

namespace Mira::Drawing 
{

class Window;

class Voronoi : public ImageApp 
{
public:
    Voronoi(const sf::Vector2u& size, int pointCount, bool color = true);

    void UpdateBuffer() override;
    void DrawImGui() override;

protected:
    void RandomizePoints();
    void RandomizeColors();
    void SwitchColor();
    void SwitchPoints();
    void AddPoint();
    void RemovePoint();
    void Reset() override;
    float DistanceSquared(const sf::Vector2f& a, const sf::Vector2f& b) const;
    sf::Color GetSampleColor(const sf::Vector2f& sample) const;

    sf::Vector3i ColorToVector(const sf::Color& color) const { return sf::Vector3i(color.r, color.g, color.b); }

    std::vector<sf::Vector2f> m_Points;  ///< List of points
    std::vector<sf::Color> m_Colors;  ///< Color of each point
    std::vector<sf::Vector3i> m_Pixels;  ///< Color of each samples of each pixels
    std::mutex m_ResetMtx;  ///< Mutex used when to avoid data race when resetting
    bool m_UseColor;  ///< True if we have rgb colors, false if we have grey scale
    std::mt19937 m_Generator;  ///< A random number generator
    std::uniform_real_distribution<float> m_DistribX;  ///< Distribution for the point x coordinate
    std::uniform_real_distribution<float> m_DistribY;  ///< Distribution for the point y coordinate
    std::uniform_real_distribution<float> m_DistribSample;  ///< Distribution for the point y coordinate
    std::uniform_int_distribution<unsigned int> m_DistribC;  ///< Distribution the color channels
    bool m_DrawPoints;  ///< If the points are drawn
    int m_Msaa;  ///< Multi sample anti aliasing level
};

}  // namespace Mira::Drawing
