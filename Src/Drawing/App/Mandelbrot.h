// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include "Drawing/App/Julia.h"

namespace Mira::Drawing
{

class Window;

class Mandelbrot : public Julia 
{
public:
    explicit Mandelbrot(const sf::Vector2u& size);

protected:
    std::complex<double> GetC(sf::Vector2u pixel, sf::Vector2<double> sample) override;
    std::complex<double> GetZ0(sf::Vector2u pixel, sf::Vector2<double> sample) override;
};

}  // namespace Mira::Drawing
