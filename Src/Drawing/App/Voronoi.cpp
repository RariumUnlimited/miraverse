// Copyright RariumUnlimited - Licence : MIT
#include <Drawing/App/Voronoi.h>

#include <imgui.h>
#include <omp.h>

#include <chrono>
#include <iostream>
#include <cfloat>

namespace Mira::Drawing 
{

Voronoi::Voronoi(const sf::Vector2u& size, int pointCount, bool color) 
    : ImageApp(size)
    , m_Generator((unsigned int)std::chrono::system_clock::now().time_since_epoch().count())
    , m_DistribX(0, static_cast<float>(size.x - 1))
    , m_DistribY(0, static_cast<float>(size.y - 1))
    , m_DistribSample(0.f, 1.f)
    , m_DistribC(0, 255) 
{
    this->m_UseColor = color;

    m_Points.resize(pointCount);
    m_Colors.resize(pointCount);

    // Generate random points
    for (sf::Vector2f& p : m_Points) 
    {
        p.x = m_DistribX(m_Generator);
        p.y = m_DistribY(m_Generator);
    }

    // Generate random colors for each point
    for (sf::Color& c : m_Colors) 
    {
        c.r = m_DistribC(m_Generator);
        if (color) 
        {
            c.g = m_DistribC(m_Generator);
            c.b = m_DistribC(m_Generator);
        } 
        else 
        {
            c.g = c.r;
            c.b = c.r;
        }
    }

    m_DrawPoints = true;

    m_Pixels.resize(size.x * size.y);
}

float Voronoi::DistanceSquared(const sf::Vector2f& a, const sf::Vector2f& b) const
{
    return (b.x - a .x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y);
}

void Voronoi::DrawImGui()
{
    ImGui::Text("MSAA: %i", m_Msaa);

    ImGui::Separator();

    if (ImGui::Button("Randomize Points"))
    {
        RandomizePoints();
    }
    ImGui::SameLine();
    if (ImGui::Button("Randomize Colors"))
    {
        RandomizeColors();
    }

    if (m_UseColor && ImGui::Button("Switch to Grey Scale"))
    {
        SwitchColor();
    }
    if (!m_UseColor && ImGui::Button("Swith to Color"))
    {
        SwitchColor();
    }
    ImGui::SameLine();
    if (m_DrawPoints && ImGui::Button("Hide Points"))
    {
        SwitchPoints();
    }
    if (!m_DrawPoints && ImGui::Button("Show Points"))
    {
        SwitchPoints();
    }

    int pointCount = (int)m_Points.size();
    ImGui::InputInt("Point Count", &pointCount);
    if (pointCount > m_Points.size())
    {
        while (pointCount != m_Points.size())
        {
            AddPoint();
        }
    }
    else if (pointCount < m_Points.size())
    {
        while (pointCount != m_Points.size())
        {
            RemovePoint();
        }
    }
}

void Voronoi::RandomizePoints() 
{
    for (sf::Vector2f& p : m_Points)
    {
        p.x = m_DistribX(m_Generator);
        p.y = m_DistribY(m_Generator);
    }

    Reset();
}

void Voronoi::RandomizeColors()
{
    for (sf::Color& c : m_Colors)
    {
        c.r = m_DistribC(m_Generator);
        if (m_UseColor)
        {
            c.g = m_DistribC(m_Generator);
            c.b = m_DistribC(m_Generator);
        } 
        else 
        {
            c.g = c.r;
            c.b = c.r;
        }
    }

    Reset();
}

void Voronoi::SwitchColor()
{
    m_UseColor = !m_UseColor;

    RandomizeColors();
    Reset();
}

void Voronoi::SwitchPoints() 
{
    m_DrawPoints = !m_DrawPoints;
    Reset();
}

void Voronoi::AddPoint() 
{
    m_Points.push_back(sf::Vector2f(m_DistribX(m_Generator), m_DistribY(m_Generator)));

    sf::Color c;
    c.r = m_DistribC(m_Generator);
    if (m_UseColor) 
    {
        c.g = m_DistribC(m_Generator);
        c.b = m_DistribC(m_Generator);
    } 
    else 
    {
        c.g = c.r;
        c.b = c.r;
    }
    m_Colors.push_back(c);
    Reset();
}

void Voronoi::RemovePoint() 
{
    if (m_Points.size() > 1) 
    {
        m_Points.pop_back();
        m_Colors.pop_back();
    }
    Reset();
}

sf::Color Voronoi::GetSampleColor(const sf::Vector2f& sample) const
{
    // We consider our first point to be our nearest neighbour
    unsigned int neighbour = 0;
    float neighbourDistance = DistanceSquared(m_Points[0], sample);

    // We check each other point if he is a closer neighbour
    for (unsigned int j = 1; j < m_Points.size(); ++j) 
    {
        float dTemp = DistanceSquared(m_Points[j], sample);
        if (dTemp < neighbourDistance) 
        {
            neighbourDistance = dTemp;
            neighbour = j;
        }
    }

    return m_Colors[neighbour];
}

void Voronoi::Reset() 
{
    m_Msaa = 1;
    #pragma omp parallel for
    for (int i = 0; i < (int)m_ImageBuffer.getSize().x * (int)m_ImageBuffer.getSize().y; ++i)
    {
        m_Pixels[i] = sf::Vector3i(0, 0, 0);
    }
}

void Voronoi::UpdateBuffer() 
{
    if (m_Msaa < 32) 
    {
        sf::Clock clock;

        // For each pixel, only one for loop for easier parallelization
        #pragma omp parallel for
        for (int i = 0; i < (int)m_ImageBuffer.getSize().x * (int)m_ImageBuffer.getSize().y; ++i)
        {
            // Compute each pixel coord from the index
            unsigned int x = i % m_ImageBuffer.getSize().x;
            unsigned int y = (i - x) / m_ImageBuffer.getSize().x;



            float sampleX = m_DistribSample(m_Generator);
            float sampleY = m_DistribSample(m_Generator);

            sf::Vector2f pixel(static_cast<float>(x) + sampleX,
                               static_cast<float>(y) + sampleY);

            m_Pixels[i] += ColorToVector(GetSampleColor(pixel));

            m_ImageBuffer.setPixel({ x, y }, sf::Color(m_Pixels[i].x / m_Msaa, m_Pixels[i].y / m_Msaa, m_Pixels[i].z / m_Msaa));
        }

        if (m_DrawPoints) 
        {
            // For each point we draw a black square of 3 by 3 pixel
            for (sf::Vector2f& p : m_Points) 
            {
                for (unsigned int j = (unsigned int)p.y - 1; j <= (unsigned int)p.y + 1; ++j)
                {
                    for (unsigned int i = (unsigned int)p.x - 1; i <= (unsigned int)p.x + 1; ++i)
                    {
                        if (i > 0 && j > 0 && i < m_ImageBuffer.getSize().x && j < m_ImageBuffer.getSize().y)
                        {
                            m_ImageBuffer.setPixel({ i, j }, sf::Color::Black);
                        }
                    }
                }
            }
        }

        ++m_Msaa;

        std::cout << "Voronoi time : " << clock.getElapsedTime().asSeconds() << "s" << std::endl;
    }
}

}  // namespace Mira::Drawing
