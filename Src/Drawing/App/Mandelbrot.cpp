// Copyright RariumUnlimited - Licence : MIT
#include <Drawing/App/Mandelbrot.h>

#include <omp.h>

#include <chrono>
#include <iostream>

namespace Mira::Drawing 
{

Mandelbrot::Mandelbrot(const sf::Vector2u& size) 
    : Julia(size) 
{ }

std::complex<double> Mandelbrot::GetC(sf::Vector2u pixel, sf::Vector2<double> sample) 
{
    return std::complex<double>(m_RMin + (pixel.x + sample.x) /
                                m_Width * (m_RMax - m_RMin),
                                m_IMin + (pixel.y + sample.y) /
                                m_Height * (m_IMax - m_IMin));
}

std::complex<double> Mandelbrot::GetZ0(sf::Vector2u pixel, sf::Vector2<double> sample) 
{
    return 0;
}

}  // namespace Mira::Drawing
