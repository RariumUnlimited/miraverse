// Copyright RariumUnlimited - Licence : MIT
#include <Drawing/App/ColorMapViewer.h>

#include <omp.h>

#include <chrono>
#include <cfloat>
#include <iostream>
#include <utility>

namespace Mira::Drawing 
{

ColorMapViewer::ColorMapViewer(const sf::Vector2u& size) 
    : ImageApp(size) 
{
    m_ColorMap.GetColorMap().insert(std::make_pair(0.f, sf::Color::Red));
    m_ColorMap.GetColorMap().insert(std::make_pair(0.2f, sf::Color::Yellow));
    m_ColorMap.GetColorMap().insert(std::make_pair(0.4f, sf::Color::Cyan));
    m_ColorMap.GetColorMap().insert(std::make_pair(0.6f, sf::Color::Blue));
    m_ColorMap.GetColorMap().insert(std::make_pair(0.8f, sf::Color::Magenta));
    m_ColorMap.GetColorMap().insert(std::make_pair(1.0f, sf::Color::Green));
}

void ColorMapViewer::UpdateBuffer() 
{
    sf::Clock clock;

    #pragma omp parallel for
    for (int i = 0; i < (int)m_ImageBuffer.getSize().x * (int)m_ImageBuffer.getSize().y; ++i)
    {
        // Compute each pixel coord from the index
        unsigned int x = i % m_ImageBuffer.getSize().x;
        unsigned int y = (i - x) / m_ImageBuffer.getSize().x;

        float factor = static_cast<float>(x) / static_cast<float>(m_ImageBuffer.getSize().x);

        sf::Color color = m_ColorMap.GetColor(factor);

        m_ImageBuffer.setPixel({ x, y }, color);
    }

    std::cout << "ColorMapViewer time : " << clock.getElapsedTime().asSeconds() << "s" << std::endl;
}

}  // namespace Mira::Drawing
