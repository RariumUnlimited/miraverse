// Copyright RariumUnlimited - Licence : MIT
#include <Drawing/App/MultiTable.h>

#include <imgui.h>
#include <omp.h>

#include <cfloat>
#include <chrono>
#include <cmath>
#include <iostream>
#include <limits>
#include <utility>

namespace Mira::Drawing 
{

MultiTable::MultiTable(const sf::Vector2u& size) 
{
    m_Width = size.x;
    m_Height = size.y;
    m_MinUpdateRate = 60;
    m_UseMinUpdateRate = true;

    CircleVertexCount = 360;
    m_Buffer = sf::VertexArray(sf::PrimitiveType::Lines);
    float step = 2.f * 3.14f / 180.f;
    m_Center = sf::Vector2f(m_Width / 2.f, m_Height / 2.f);
    m_R = m_Height / 2.f - 30.f;

    for (int i = 0; i < 180; ++i) 
    {
        float x1 = m_R * cos(step * i) + m_Center.x;
        float y1 = m_R * sin(step * i) + m_Center.y;
        float x2 = m_R * cos(step * (i + 1)) + m_Center.x;
        float y2 = m_R * sin(step * (i + 1)) + m_Center.y;
        sf::Vertex v1(sf::Vector2f(x1, y1), sf::Color::White);
        sf::Vertex v2(sf::Vector2f(x2, y2), sf::Color::White);
        m_Buffer.append(v1);
        m_Buffer.append(v2);
    }

    m_Table = 2;
    m_Mod = 6;

    m_ColorMap.GetColorMap().insert(std::make_pair(0.f, sf::Color::Red));
    m_ColorMap.GetColorMap().insert(std::make_pair(0.2f, sf::Color::Yellow));
    m_ColorMap.GetColorMap().insert(std::make_pair(0.4f, sf::Color::Cyan));
    m_ColorMap.GetColorMap().insert(std::make_pair(0.6f, sf::Color::Blue));
    m_ColorMap.GetColorMap().insert(std::make_pair(0.8f, sf::Color::Magenta));
    m_ColorMap.GetColorMap().insert(std::make_pair(1.0f, sf::Color::Green));
}

void MultiTable::DrawImGui()
{
    if (m_UseMinUpdateRate)
    {
        if (ImGui::Button("Disable Min Update Rate"))
        {
            SwitchUseMinUpdateRate();
        }
        ImGui::SameLine();
        ImGui::InputInt("Min Update Rate:", &m_MinUpdateRate);
    }
    if (!m_UseMinUpdateRate && ImGui::Button("Enable Min Update Rate"))
    {
        SwitchUseMinUpdateRate();
    }
    if (ImGui::InputInt("Table", &m_Table))
    {
        Reset();
    }
    if (ImGui::InputInt("Mod", &m_Mod))
    {
        if (m_Mod < 2)
        {
            m_Mod = 2;
        }
        Reset();
    }
    ImGui::Text("I: %i", m_CurrentI);
}

void MultiTable::SwitchUseMinUpdateRate() 
{
    m_UseMinUpdateRate = !m_UseMinUpdateRate;
}

void MultiTable::Reset() 
{
    m_CurrentI = 1;
    m_Buffer.resize(CircleVertexCount);
}

void MultiTable::UpdateBuffer() 
{
    sf::Clock clock;

    float step = 2.f * 3.14f / m_Mod;

    if (m_CurrentI < m_Mod) 
    {
        int nextI = ((m_CurrentI * m_Table) % m_Mod);
        float x1 = m_R * cos(step * m_CurrentI) + m_Center.x;
        float y1 = m_R * sin(step * m_CurrentI) + m_Center.y;
        float x2 = m_R * cos(step * nextI) + m_Center.x;
        float y2 = m_R * sin(step * nextI) + m_Center.y;
        sf::Vertex v1(sf::Vector2f(x1, y1), m_ColorMap.GetColor(static_cast<double>(m_CurrentI) / static_cast<double>(m_Mod)));
        sf::Vertex v2(sf::Vector2f(x2, y2), m_ColorMap.GetColor(static_cast<double>(nextI) / static_cast<double>(m_Mod)));
        m_Buffer.append(v1);
        m_Buffer.append(v2);
        ++m_CurrentI;
    }

    int timeMS = clock.getElapsedTime().asMilliseconds();
    float time = clock.getElapsedTime().asSeconds();

    std::cout << "MultiTable time : " << time << "s" << std::endl;

    if (timeMS < m_MinUpdateRate && m_UseMinUpdateRate) 
    {
        std::this_thread::sleep_for( std::chrono::milliseconds(m_MinUpdateRate - timeMS));
    }
}

}  // namespace Mira::Drawing
