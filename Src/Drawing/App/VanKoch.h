// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <vector>
#include <string>

#include <SFML/Graphics.hpp>

#include <Core/Window/InputState.h>
#include <Drawing/Window/VAApp.h>

namespace Mira::Drawing 
{

class Window;

struct Segment 
{
    sf::Vector2<double> m_A;
    sf::Vector2<double> m_B;

    Segment() = default;
    Segment(sf::Vector2<double> a, sf::Vector2<double> b) : m_A(a), m_B(b) { }
};

// An app that draw Koch snowflake
class VanKoch : public VAApp 
{
public:
    explicit VanKoch(const sf::Vector2u& size);

    void UpdateBuffer() override;
    void DrawImGui() override;
    void ProcessDoubleClickInput(const InputState& is, sf::Mouse::Button button) override;

protected:
    void IncreaseIteration();
    void UltraZoom(const InputState& is, sf::Mouse::Button b);
    void Reset() override;

    double m_XMin;
    double m_XMax;
    double m_YMin;
    double m_YMax;
    int m_Iteration;
    int m_Width;
    int m_Height;

    int m_MaxIteration;
    bool m_Refresh;

    std::vector<Segment> m_Segments;
};

}  // namespace Mira::Drawing
