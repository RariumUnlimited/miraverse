// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <random>
#include <string>
#include <vector>

#include <SFML/Graphics.hpp>

#include <Drawing/Tools/ColorMap.h>
#include <Drawing/Window/ImageApp.h>

namespace Mira::Drawing 
{

class Window;

class ColorMapViewer : public ImageApp 
{
public:
    explicit ColorMapViewer(const sf::Vector2u& size);

    void UpdateBuffer() override;

protected:
    ColorMap m_ColorMap;
};

}  // namespace Mira::Drawing
