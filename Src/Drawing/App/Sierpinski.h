// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <vector>
#include <random>
#include <string>

#include <SFML/Graphics.hpp>

#include <Core/Window/InputState.h>
#include <Drawing/Window/VAApp.h>

namespace Mira::Drawing
{

class Window;

// An app that draw a Sierpinski triangle
class Sierpinski : public VAApp 
{
public:
    explicit Sierpinski(const sf::Vector2u& size);

    void UpdateBuffer() override;
    void DrawImGui() override;
    void IncreasePointPerUpdate(const InputState& is, sf::Keyboard::Key k);
    void DecreasePointPerUpdate(const InputState& is, sf::Keyboard::Key k);
    void ProcessKeyInput(const InputState& is, sf::Keyboard::Key k) override;

protected:
    void Reset() override;

    int m_PointPerUpdate{ 1 };  ///< Point to compute per update call

    // sf::Vector2f p1;  ///< Bottom left vertex of the triangle
    // sf::Vector2f p2;  ///< Bottom right vertex of the triangle
    // sf::Vector2f p3;  ///< Top vertex of the triangle
    std::vector<sf::Vector2f> m_Triangles;  ///< Triangle ([0] => p1, [1] => p2, [2] => p3)
    sf::Vector2f m_A;  ///< p2 - p1
    sf::Vector2f m_B;  ///< p3 - p1

    sf::Vector2f m_CurrentPosition;  ///< Current position in the triangle;

    std::mt19937 m_Generator;  ///< A random number generator
    std::uniform_real_distribution<float> m_Distrib;  ///< Distribution for the random point
    std::uniform_int_distribution<int> m_DistribVertex;  ///< Distribution for the random vertex choice

    std::vector<sf::Vector2f> m_Points;
};

}  // namespace Mira::Drawing
