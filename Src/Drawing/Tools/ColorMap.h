// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <map>
#include <utility>

#include <SFML/Graphics.hpp>

namespace Mira::Drawing 
{

class ColorMap 
{
public:
    ColorMap() {}
    explicit ColorMap(const std::map<double, sf::Color>& cM);

    const std::map<double, sf::Color>& GetColorMap() const { return m_ColorMap; }
    std::map<double, sf::Color>& GetColorMap() { return m_ColorMap; }

    sf::Color GetColor(double f) const;

protected:
    std::map<double, sf::Color> m_ColorMap;

    static sf::Color Lerp(sf::Color a, sf::Color b, double t);
};

}  // namespace Mira::Drawing
