// Copyright RariumUnlimited - Licence : MIT
#include <Drawing/Tools/ColorMap.h>

#include <cassert>
#include <cmath>
#include <iostream>

#include <Core/Math.h>

namespace Mira::Drawing 
{

ColorMap::ColorMap(const std::map<double, sf::Color>& cM) 
    : m_ColorMap(cM) 
{

}

sf::Color ColorMap::GetColor(double f) const 
{
    assert(m_ColorMap.size() >= 2);

    auto it = m_ColorMap.begin();
    ++it;

    for (; it != m_ColorMap.end(); ++it) 
    {
        if (f < it->first) 
        {
            auto it2 = it;
            --it2;
            sf::Color a = it2->second;
            sf::Color b = it->second;
            double lerpFactor = (f - it2->first) / (it->first - it2->first);
            return Lerp(a, b, lerpFactor);
        }
    }

    return Lerp(m_ColorMap.begin()->second, (++m_ColorMap.begin())->second, f);
}

sf::Color ColorMap::Lerp(sf::Color a, sf::Color b, double t) 
{
    sf::Color result;
    result.r = (unsigned char)(a.r + (b.r - a.r) * t);
    result.g = (unsigned char)(a.g + (b.g - a.g) * t);
    result.b = (unsigned char)(a.b + (b.b - a.b) * t);
    return result;
}

}  // namespace Mira::Drawing
