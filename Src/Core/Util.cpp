// Copyright RariumUnlimited - Licence : MIT
#include <Core/Util.h>

namespace Mira {

std::vector<std::string> Split(const std::string& stringToSplit, const std::string& separator) 
{
    // Where we store the list of strings
    std::vector<std::string> strings;

    // Store the value of the current split string
    std::string currentString = stringToSplit;

    std::size_t pos = currentString.find(separator);

    while (pos != std::string::npos) 
    {
        if (pos > 0) 
        {
            std::string subStr = currentString.substr(0, pos);

            strings.push_back(subStr);
        }

        currentString.erase(currentString.begin(), currentString.begin() + pos + separator.size());

        pos = currentString.find(separator);
    }

    // If we have something in the value of the current, we must add it to the argument list.
    if (currentString != "") 
    {
        strings.push_back(currentString);
        currentString = "";
    }

    return strings;
}

void RemoveChar(std::string& str, char c) 
{
    std::size_t pos = str.find(c);

    while (pos != std::string::npos) 
    {
        str.erase(pos, 1);
    }
}

std::vector<std::string> Split(const std::string& stringToSplit, char separator, char ignore) 
{
    // Where we store the list of strings
    std::vector<std::string> strings;

    // Store the value of the current split string
    std::string currentString;

    for (unsigned int i = 0; i < stringToSplit.size(); ++i) 
    {
        // If we encounter a space while we are not in an argument it means it's the end of the current argument
        if (stringToSplit[i] == separator) 
        {
            if (currentString != "") 
            {
                strings.push_back(currentString);
                currentString = "";
            }
        } 
        else if (stringToSplit[i] == ignore && ignore != std::numeric_limits<char>::min()) 
        {
            continue;
        } 
        else 
        {
            currentString += stringToSplit[i];
        }
    }

    // If we have something in the value of the current, we must add it to the argument list.
    if (currentString != "") 
    {
        strings.push_back(currentString);
        currentString = "";
    }

    return strings;
}

}  // namespace Mira
