// Copyright RariumUnlimited - Licence : MIT
#include <Core/Log.h>

#include <chrono>
#include <iostream>
#include <string>
#include <ctime>

namespace Mira 
{

void Logger::AddStream(std::ostream* stream) 
{
    m_Streams.push_back(stream);
}

Logger& Logger::Log(bool outputTime) 
{
    static Logger instance;

    if (outputTime) 
    {
        std::chrono::system_clock::time_point today = std::chrono::system_clock::now();
        std::time_t tt = std::chrono::system_clock::to_time_t(today);

        std::tm* ptm = std::localtime(&tt);
        char timeBuff[K_TIME_BUFFER_SIZE];
        // Format: Mo, 15.06.2009 20:20:00
        std::strftime(timeBuff, 32, "%a, %d.%m.%Y %H:%M:%S", ptm);


        std::string timeStr(timeBuff);

        instance << timeStr << ": ";
    }

    return instance;
}

}  // namespace Mira
