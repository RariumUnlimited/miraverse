// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <Core/CompileTimeCrc.h>

namespace Mira
{

using IdType = CrcType;
static constexpr IdType Invalid_IdType = 0;

constexpr IdType operator""_id(const char* str, std::size_t size)
{
	return crcdetail::compute(str, (uint32_t)size);
}

static IdType GetIdFromString(const std::string& str)
{
	return CRC(str);
}

}  // namespace Mira