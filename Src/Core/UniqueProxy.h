// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <string>

namespace Mira
{

template<class C>
class UniqueProxy
{
public:
	static void OpenProxy(C& proxy)
	{
		if (m_Proxy)
		{
			throw std::string("Proxy already open for : " + std::string(typeid(C).name()));
		}
		m_Proxy = &proxy;
	}

	static void CloseProxy(C& proxy)
	{
		if (m_Proxy != &proxy)
		{
			throw std::string("Closing proxy error for : " + std::string(typeid(C).name()));
		}
		m_Proxy = nullptr;
	}

	static C& Get()
	{
		if (!m_Proxy)
		{
			throw std::string("Proxy not opened when getting for : " + std::string(typeid(C).name()));
		}

		return *m_Proxy;
	}

	static bool IsOpen()
	{
		return m_Proxy;
	}

private:
	static inline C* m_Proxy{ nullptr };
};


}  // namespace Mira