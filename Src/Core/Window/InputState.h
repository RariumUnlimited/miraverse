// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <unordered_set>
#include <string>

#include <SFML/Window.hpp>

#include <Core/Event/EventDispatcher.h>

namespace Mira 
{

struct InputState 
{
    sf::Vector2i m_MousePosition = sf::Vector2i(0, 0);  ///< Mouse position on screen
    float m_WheelAmount = 0.f;  ///< Middle scroll wheel amount
    std::unordered_set<sf::Keyboard::Key> m_KeyPressed;  ///< The list of key that are being pressed
    std::unordered_set<sf::Mouse::Button> m_ButtonPressed;  ///< The list of key that are being pressed
};

struct InputEvent
{
    InputEvent(const InputState& inputState) : m_InputState(inputState) {}
    const InputState& m_InputState;
};

struct MouseButtonEvent : public InputEvent
{
    MouseButtonEvent(const InputState& inputState, sf::Mouse::Button button)
        : InputEvent(inputState)
        , m_Button(button) {}
    sf::Mouse::Button m_Button;
};

struct MouseDragEvent : public MouseButtonEvent
{
    MouseDragEvent(const InputState& inputState, sf::Mouse::Button button, sf::Vector2i offset)
        : MouseButtonEvent(inputState, button)
        , m_Offset(offset) {}
    sf::Vector2i m_Offset;
};

struct MouseWheelEvent : public InputEvent
{
    MouseWheelEvent(const InputState& inputState, float delta)
        : InputEvent(inputState)
        , m_Delta(delta) {}
    float m_Delta;
};

struct KeyEvent : public InputEvent
{
    KeyEvent(const InputState& inputState, sf::Keyboard::Key key)
        : InputEvent(inputState)
        , m_Key(key) {}
    sf::Keyboard::Key m_Key;
};

struct MouseButtonPressedEvent : MouseButtonEvent
{
    using MouseButtonEvent::MouseButtonEvent;
};

struct MouseButtonReleasedEvent : MouseButtonEvent
{
    using MouseButtonEvent::MouseButtonEvent;
};

struct MouseButtonClickEvent : MouseButtonEvent
{
    using MouseButtonEvent::MouseButtonEvent;
};

struct MouseButtonDoubleClickEvent : MouseButtonEvent
{
    using MouseButtonEvent::MouseButtonEvent;
};

struct MouseBeginDrag : MouseButtonEvent
{
    using MouseButtonEvent::MouseButtonEvent;
};

struct MouseEndDragEvent : MouseButtonEvent
{
    using MouseButtonEvent::MouseButtonEvent;
};

struct KeyPressedEvent : KeyEvent
{
    using KeyEvent::KeyEvent;
};

struct KeyReleasedEvent : KeyEvent
{
    using KeyEvent::KeyEvent;
};

}  // namespace Mira
