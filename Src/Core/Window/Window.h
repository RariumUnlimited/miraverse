// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <memory>
#include <vector>

#include <SFML/Graphics.hpp>

#include <Core/Application/ApplicationManager.h>
#include <Core/Assets/AssetManager.h>
#include <Core/Hierarchy.h>
#include <Core/Window/InputState.h>
#include <Core/Window/WindowHelper.h>

namespace Mira 
{

class Window : public WindowHelper
{
public:
    explicit Window(const std::vector<std::unique_ptr<Application>>& applicationList);
    ~Window();

    void Run();

    sf::Vector2u GetSize() override { return m_Window.getSize(); }
    void SetSize(sf::Vector2u size) override;
    sf::Vector2u GetRenderSize() override { return sf::Vector2u(m_Window.getSize().x, m_Window.getSize().y - (unsigned int)std::ceil(m_AppManager.GetMenuHeight())); }

protected:
     sf::RenderWindow m_Window;  ///< Our sfml window
     ApplicationManager m_AppManager;
     Hierarchy<Application> m_AppHierarchy;
     InputState m_CurrentInput;  ///< User input state of the current update
     InputState m_PreviousInput;  ///< User input state of the last update
     sf::Clock m_DoubleClickClock[sf::Mouse::ButtonCount];  ///< Clock used to check if we double click
     sf::Clock m_ClickClock[sf::Mouse::ButtonCount];  ///< Clock used to check clicking
     bool m_Dragging[sf::Mouse::ButtonCount];  ///< If we are dragging
     sf::ContextSettings m_Settings;

     bool m_ResizeRequested{ false };
     sf::Vector2u m_RequestedSize;

private:
     bool HandleEvents();
     void HandleMouseButtonDown(sf::Event e);
     void HandleMouseButtonUp(sf::Event e);
     void HandleMouseWheel(sf::Event e);
     void HandleKeyPressed(sf::Event e);
     void HandleKeyReleased(sf::Event e);
     void CloseWindow();
     /**
      * @brief Take of a screenshot of the window content
      */
     void TakeScreenshot();
     void DrawTestRectangles();
};

}  // namespace Mira