// Copyright RariumUnlimited - Licence : MIT
#include <Core/Window/Window.h>

#include <queue>
#include <string>

#include <imgui.h>
#include <imgui-SFML.h>

#include <Core/Log.h>
#include <Core/Math.h>

namespace Mira 
{

Window::Window(const std::vector<std::unique_ptr<Application>>& applicationList)
{
    WindowHelperProxy::OpenProxy(*this);

    m_Settings.depthBits = 24;
    m_Settings.stencilBits = 8;
    m_Settings.antiAliasingLevel = 0;
    m_Settings.majorVersion = 2;
    m_Settings.minorVersion = 1;

    m_Window.create(sf::VideoMode({ 1280, 720 }), "Miraverse", sf::Style::Default, sf::State::Windowed, m_Settings);
    m_Window.setFramerateLimit(60);
    
    if (m_Window.isOpen())
    {
        Logger::Log() << "Window opened, Width: " << m_Window.getSize().x << "px, Height: " << m_Window.getSize().x << "px\n";
    }
    else
    {
        throw std::string("Unable to open SFML window.");
    }

    if (!ImGui::SFML::Init(m_Window))
    {
        throw std::string("Unable to init SFML window");
    }

    for (const std::unique_ptr<Application>& app : applicationList) 
    {
        m_AppHierarchy.Insert(app.get()->GetPath(), app.get());
    }

    m_AppManager.SetAppHierarchy(&m_AppHierarchy);
}

Window::~Window()
{
    WindowHelperProxy::CloseProxy(*this);
}

void Window::SetSize(sf::Vector2u size) 
{
    size.x = Clamp<unsigned int>(size.x, 0, K_MAX_RENDERING_WIDTH);
    size.y = Clamp<unsigned int>(size.y, 0, K_MAX_RENDERING_WIDTH);
    m_RequestedSize = size;
    m_ResizeRequested = true;
}

void Window::Run() 
{
    // Reset the clock for delta time calculation
    sf::Clock clock;
    while (m_Window.isOpen()) 
    {
        if (!HandleEvents())
        {
            break;
        }

        float delta = clock.restart().asSeconds();
        ImGui::SFML::Update(m_Window, sf::seconds(delta));

        m_AppManager.Update(delta, m_CurrentInput, m_PreviousInput);
        m_AppManager.DrawImGui();

        sf::Vector2u renderSize = GetRenderSize();

        sf::View view({ {renderSize.x / 2.f, renderSize.y / 2.f}, { (float)renderSize.x, (float)renderSize.y } });

        float viewportMenuRatio = m_AppManager.GetMenuHeight() / (float)m_Window.getSize().y;
        view.setViewport({{0.f, viewportMenuRatio}, {1.f, 1.f - viewportMenuRatio}});

        m_Window.setView(view);

        m_Window.clear(sf::Color::Black);

        m_AppManager.DrawGraphic(m_Window);

        ImGui::SFML::Render(m_Window);

        m_Window.display();

        if (m_ResizeRequested)
        {
            m_Window.setSize(m_RequestedSize);
            m_AppManager.NotifyWindowResize(m_Window.getSize());
        }
    }
}

bool Window::HandleEvents() 
{
    m_PreviousInput = m_CurrentInput;
    m_CurrentInput.m_WheelAmount = 0;

    std::queue<sf::Event> eventQueue;
    while (const std::optional<sf::Event> eventOpt = m_Window.pollEvent())
    {
        if (!eventOpt.has_value())
        {
            continue;
        }
        sf::Event event = eventOpt.value();


        ImGui::SFML::ProcessEvent(m_Window, event);
        eventQueue.push(event);

        if (event.is<sf::Event::Closed>())
        {
            CloseWindow();
            return false;
        }
        else if (event.is<sf::Event::Resized>())
        {
            m_AppManager.NotifyWindowResize(GetRenderSize());
        }
    }

    m_CurrentInput.m_MousePosition = sf::Mouse::getPosition(m_Window);
    m_CurrentInput.m_MousePosition.y -= (int)std::ceil(m_AppManager.GetMenuHeight());

    while (!eventQueue.empty()) 
    {
        sf::Event& e = eventQueue.front();

        if (e.is<sf::Event::MouseButtonPressed>() && !ImGui::GetIO().WantCaptureMouse)
        {
            HandleMouseButtonDown(e);
        }
        else if (e.is<sf::Event::MouseButtonReleased>() && !ImGui::GetIO().WantCaptureMouse)
        {
            HandleMouseButtonUp(e);
        }
        else if (e.is<sf::Event::MouseWheelScrolled>() && !ImGui::GetIO().WantCaptureMouse)
        {
            HandleMouseWheel(e);
        }
        else if (e.is<sf::Event::KeyPressed>() && !ImGui::GetIO().WantCaptureKeyboard)
        {
            HandleKeyPressed(e);
        }
        else if (e.is<sf::Event::KeyReleased>() && !ImGui::GetIO().WantCaptureKeyboard)
        {
            HandleKeyReleased(e);
        }

        eventQueue.pop();
    }

    if (!ImGui::GetIO().WantCaptureMouse) 
    {
        Application* onScreenApp = m_AppManager.GetOnScreenGraphicApp();
        if (onScreenApp != nullptr) 
        {
            for (int b = 0; b < sf::Mouse::ButtonCount; ++b) 
            {
                if (m_CurrentInput.m_ButtonPressed.find(sf::Mouse::Button(b)) == m_CurrentInput.m_ButtonPressed.cend()) 
                {
                    if (m_Dragging[b]) 
                    {
                        onScreenApp->OnMouseEndDrag.Dispatch({ m_CurrentInput, sf::Mouse::Button(b) });
                        m_Dragging[b] = false;
                    }
                } 
                else 
                {
                    if (!m_Dragging[b] && m_ClickClock[b].getElapsedTime().asMilliseconds() > 200) 
                    {
                        onScreenApp->OnMouseBeginDrag.Dispatch({ m_CurrentInput, sf::Mouse::Button(b) });
                        m_Dragging[b] = true;
                    } 
                    else if (m_Dragging[b]) 
                    {
                        sf::Vector2i offset = m_CurrentInput.m_MousePosition - m_PreviousInput.m_MousePosition;
                        if (offset.x != 0 || offset.y != 0)
                        {
                            onScreenApp->OnMouseDrag.Dispatch({ m_CurrentInput, sf::Mouse::Button(b), offset });
                        }
                    }
                }
            }
        }
    }

    return true;
}

void Window::HandleMouseButtonDown(sf::Event e) 
{
    const sf::Event::MouseButtonPressed* mouseButtonPressed = e.getIf<sf::Event::MouseButtonPressed>();
    m_CurrentInput.m_ButtonPressed.insert(mouseButtonPressed->button);

    m_ClickClock[(int)mouseButtonPressed->button].restart();

    Application* onScreenApp = m_AppManager.GetOnScreenGraphicApp();
    if (onScreenApp != nullptr)
    {
        onScreenApp->OnMouseButtonPressed.Dispatch({ m_CurrentInput, mouseButtonPressed->button });
    }
}

void Window::HandleMouseButtonUp(sf::Event e) 
{
    const sf::Event::MouseButtonReleased* mouseButtonReleased = e.getIf<sf::Event::MouseButtonReleased>();
    m_CurrentInput.m_ButtonPressed.erase(mouseButtonReleased->button);

    if (m_ClickClock[(int)mouseButtonReleased->button].getElapsedTime().asMilliseconds() < 200)
    {
        if (m_DoubleClickClock[(int)mouseButtonReleased->button].getElapsedTime().asMilliseconds() < 200)
        {
            Application* onScreenApp = m_AppManager.GetOnScreenGraphicApp();
            if (onScreenApp != nullptr)
            {
                onScreenApp->OnMouseButtonDoubleClick.Dispatch({ m_CurrentInput, mouseButtonReleased->button });
            }
        } 
        else 
        {
            Application* onScreenApp = m_AppManager.GetOnScreenGraphicApp();
            if (onScreenApp != nullptr)
            {
                onScreenApp->OnMouseButtonClick.Dispatch({ m_CurrentInput, mouseButtonReleased->button });
            }
            m_DoubleClickClock[(int)mouseButtonReleased->button].restart();
        }
    }

    Application* onScreenApp = m_AppManager.GetOnScreenGraphicApp();
    if (onScreenApp != nullptr)
    {
        onScreenApp->OnMouseButtonReleased.Dispatch({ m_CurrentInput, mouseButtonReleased->button });
    }
}

void Window::HandleMouseWheel(sf::Event e) 
{
    const sf::Event::MouseWheelScrolled* mouseWheelScrolled = e.getIf<sf::Event::MouseWheelScrolled>();
    m_CurrentInput.m_WheelAmount = mouseWheelScrolled->delta;
}

void Window::HandleKeyPressed(sf::Event e) 
{
    const sf::Event::KeyPressed* keyPressed = e.getIf<sf::Event::KeyPressed>();
    m_CurrentInput.m_KeyPressed.insert(keyPressed->code);

    if (keyPressed->code == sf::Keyboard::Key::F12)
    {
        TakeScreenshot();
    }

    Application* onScreenApp = m_AppManager.GetOnScreenGraphicApp();
    if (onScreenApp != nullptr)
    {
        onScreenApp->OnKeyPressed.Dispatch({ m_CurrentInput, keyPressed->code });
    }
}

void Window::HandleKeyReleased(sf::Event e) 
{
    const sf::Event::KeyReleased* keyReleased = e.getIf<sf::Event::KeyReleased>();
    m_CurrentInput.m_KeyPressed.erase(keyReleased->code);

    Application* onScreenApp = m_AppManager.GetOnScreenGraphicApp();
    if (onScreenApp != nullptr)
    {
        onScreenApp->OnKeyReleased.Dispatch({ m_CurrentInput, keyReleased->code });
    }
}

void Window::CloseWindow() 
{
    m_Window.close();
    ImGui::SFML::Shutdown();
}

void Window::TakeScreenshot() 
{
    sf::Texture screenTex(m_Window.getSize());
    screenTex.update(m_Window);
    sf::Image screenImg = screenTex.copyToImage();
    std::time_t time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    char datetime[512];
    std::size_t datetimesize = strftime(datetime, 512, "%Y%m%d-%H%M%S", std::localtime(&time));
    std::string filename(datetime, datetimesize);
    filename = "screenshot-"+filename+".png";
    if (!screenImg.saveToFile(filename))
    {
        throw std::string("Unable to save screenshot to directory" + filename);
    }
}

void Window::DrawTestRectangles()
{
    sf::Vector2u renderSize = GetRenderSize();
    sf::RectangleShape rect1({ (float)renderSize.x, (float)renderSize.y });
    rect1.setFillColor(sf::Color::Green);
    m_Window.draw(rect1);

    sf::RectangleShape rect2({ (float)renderSize.x - 30.f, (float)renderSize.y - 30.f });
    rect2.setPosition({ 15.f, 15.f });
    rect2.setFillColor(sf::Color::Red);
    m_Window.draw(rect2);

    sf::RectangleShape rect3({ (float)renderSize.x, m_AppManager.GetMenuHeight() });
    rect3.setPosition({ 0, m_Window.getSize().y - m_AppManager.GetMenuHeight() });
    rect3.setFillColor(sf::Color::Blue);
    m_Window.draw(rect3);
}

}  // namespace Mira
