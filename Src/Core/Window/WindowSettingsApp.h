// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <imgui.h>

#include <Core/Application/Application.h>
#include <Core/Math.h>
#include <Core/Window/WindowHelper.h>

namespace Mira 
{

class WindowSettingsApp : public Application 
{
public:
    WindowSettingsApp();

protected:
    void DrawImGuiInternal() override;
    void OnDisplay() override;
    void OnRenderSizeChanged(sf::Vector2u newSize) override;

private:
    // ====<ImGui input variable>====
    unsigned int m_Width{};
    unsigned int m_Height{};
    // ====</ImGui input variable>===
};

}  // namespace Mira
