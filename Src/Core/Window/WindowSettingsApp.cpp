// Copyright RariumUnlimited - Licence : MIT
#include <Core/Window/WindowSettingsApp.h>

namespace Mira 
{

WindowSettingsApp::WindowSettingsApp() 
    : Application("/Core/Window Settings", "Window Settings") 
{
    m_AutoResize = true;
}

void WindowSettingsApp::DrawImGuiInternal() 
{
    if (WindowHelperProxy::IsOpen()) 
    {
        WindowHelper& window = WindowHelperProxy::Get();
        ImGui::Text("Window Size : %ux%u", window.GetSize().x, window.GetSize().y);
        ImGui::Text("Render Size : %ux%u", window.GetRenderSize().x, window.GetRenderSize().y);
        ImGui::Separator();
        ImGui::InputScalar("Width", ImGuiDataType_U32, &m_Width);
        m_Width = Clamp<unsigned int>(m_Width, 0, WindowHelper::K_MAX_RENDERING_WIDTH);
        ImGui::InputScalar("Height", ImGuiDataType_U32, &m_Height);
        m_Height = Clamp<unsigned int>(m_Height, 0, WindowHelper::K_MAX_RENDERING_HEIGHT);
        if (ImGui::Button("Resize")) 
        {
            window.SetSize(sf::Vector2u(m_Width, m_Height));
        }
    } 
    else 
    {
        ImGui::Text("No WindowHelper service");
    }
}

void WindowSettingsApp::OnDisplay() 
{
    if (WindowHelperProxy::IsOpen())
    {
        WindowHelper& window = WindowHelperProxy::Get();
        m_Width = window.GetSize().x;
        m_Height = window.GetSize().y;
    }
}

void WindowSettingsApp::OnRenderSizeChanged(sf::Vector2u newSize)
{
    m_Width = newSize.x;
    m_Height = newSize.y;
}

}  // namespace Mira
