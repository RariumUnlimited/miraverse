// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <SFML/System.hpp>

#include <Core/UniqueProxy.h>

namespace Mira 
{

class WindowHelper
{
public:
    virtual sf::Vector2u GetSize() = 0;
    virtual void SetSize(sf::Vector2u size) = 0;
    virtual sf::Vector2u GetRenderSize() = 0;

    static constexpr unsigned int K_MAX_RENDERING_WIDTH = 3840;
    static constexpr unsigned int K_MAX_RENDERING_HEIGHT = 2160;
};

class WindowHelperProxy : public UniqueProxy<WindowHelper>
{

};

}  // namespace Mira
