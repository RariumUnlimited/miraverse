// Copyright RariumUnlimited - Licence : MIT
#include <Core/ImGuiDemoApp.h>

namespace Mira 
{

ImGuiDemoApp::ImGuiDemoApp()
    : Application("/Core/ImGui Demo", "ImGui Demo") 
{

}

void ImGuiDemoApp::DrawImGuiInternal()
{
    ImGui::ShowDemoWindow(&m_IsOpen);
}

}  // namespace Mira
