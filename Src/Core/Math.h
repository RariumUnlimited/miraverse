// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <cassert>

#include <SFML/Graphics.hpp>

namespace Mira 
{

template <typename T>
T Lerp(T a, T b, float t) 
{
    assert(t >= 0.f && t <= 1.f);
    return (1 - t) * a + t * b;
}

template <typename T>
T Clamp(T value, T min, T max) 
{
    return value <= max ? (value >= min ? value : min) : max;
}

}  // namespace Mira
