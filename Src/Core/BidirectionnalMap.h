// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include <Core/Log.h>

namespace Mira 
{

template<typename T, typename U>
class BidirectionnalMap 
{
public:
    enum class EResult 
    {
        Success,
        TAlreadyExist,
        UAlreadyExist,
        TDoesntExist,
        UDoesntExist
    };

    EResult Insert(T t, U u) 
    {
        auto itT = m_TIndexer.find(t);
        auto itU = m_UIndexer.find(u);

        if (itT != m_TIndexer.end())
        {
            return EResult::TAlreadyExist;
        }

        if (itU != m_UIndexer.end())
        {
            return EResult::UAlreadyExist;
        }

        std::size_t index = m_Map.size();
        m_Map.push_back(std::make_pair(t, u));
        m_TIndexer.insert(std::make_pair(t, index));
        m_UIndexer.insert(std::make_pair(u, index));

        return EResult::Success;
    }

    EResult GetU(T t, U& u) 
    {
        auto itT = m_TIndexer.find(t);

        if (itT == m_TIndexer.end())
        {
            return EResult::TDoesntExist;
        }

        u = m_Map[itT->second].second;

        return EResult::Success;
    }

    EResult GetT(T& t, U u) 
    {
        auto itU = m_UIndexer.find(u);

        if (itU == m_UIndexer.end())
        {
            return EResult::UDoesntExist;
        }

        t = m_Map[itU->second].first;

        return EResult::Success;
    }

    EResult Erase(T t) 
    {
        auto itT = m_TIndexer.find(t);

        if (itT == m_TIndexer.end())
        {
            return EResult::TDoesntExist;
        }

        std::size_t index = itT->second;

        m_TIndexer.erase(itT);
        m_UIndexer.erase(m_Map[index].second);

        RemoveAt(index);

        return EResult::Success;
    }

    EResult Erase(U u) 
    {
        auto itU = m_UIndexer.find(u);

        if (itU == m_UIndexer.end())
        {
            return EResult::UDoesntExist;
        }

        std::size_t index = itU->second;

        m_UIndexer.erase(itU);
        m_TIndexer.erase(m_Map[index].first);

        RemoveAt(index);

        return EResult::Success;
    }

    void Display() 
    {
        Logger::Log() << "Bidirectionnal Map : " << m_Map.size() << " elements\n";
        for (std::size_t i = 0; i < m_Map.size(); ++i) 
        {
            Logger::Log() << m_Map[i].first << " <=> " << m_Map[i].second << "\n";
        }
    }

    /// Execute a function for each entry, in the  chronological order in which entries are added to the map
    template<typename Func>
    void ForEachEntry(Func func) 
    {
        for (std::size_t i = 0; i < m_Map.size(); ++i) 
        {
            func(m_Map[i].first, m_Map[i].second);
        }
    }

private:
    void RemoveAt(std::size_t index) 
    {
        for (std::size_t i = index + 1; i < m_Map.size(); ++i) 
        {
            --m_TIndexer[m_Map[i].first];
            --m_UIndexer[m_Map[i].second];
        }

        m_Map.erase(m_Map.begin() + index);
    }

    std::vector<std::pair<T, U>> m_Map;
    std::unordered_map<T, std::size_t> m_TIndexer;
    std::unordered_map<U, std::size_t> m_UIndexer;
};

}  // namespace Mira
