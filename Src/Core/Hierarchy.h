// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <string>
#include <map>
#include <memory>
#include <vector>

#include <Core/Util.h>

namespace Mira 
{

template<typename T>
class Hierarchy 
{
public:
    Hierarchy() 
    {
        m_Data = nullptr;
    }

    /// Get the name of the node
    const std::string& GetName() const { return m_Name; }
    /// Get the subnodes of the node
    const std::map<std::string, Hierarchy>& GetSubNodes() const { return m_SubNodes; }
    T* GetData() const { return m_Data; }
    void SetData(T* data) { this->m_Data = data; }

    /// If the node is an end node
    bool IsEndNode() const { return m_Data != nullptr && m_SubNodes.size() == 0; }
    /// If the node is not an end node
    bool IsParentNode() const { return m_Data == nullptr && m_SubNodes.size() > 0; }
    /// If the node is valid
    bool IsValid() const 
    {
        bool subNodesValid = true;
        for (const auto& p : m_SubNodes) 
        {
            if (!p.second.IsValid()) 
            {
                subNodesValid = false;
                break;
            }
        }

        return !subNodesValid ? false : IsEndNode() != IsParentNode();
    }

    // Insert data in the hierarchy
    const Hierarchy& Insert(std::string path, T* data) 
    {
        std::vector<std::string> split = Split(path, '/');

        return InsertInternal(split, 0, data);
    }

private:
    Hierarchy& InsertInternal(const std::vector<std::string>& path, int depth, T* data) 
    {
        if (path.size() > depth) 
        {
            Hierarchy& node = m_SubNodes[path[depth]];
            node.m_Name = path[depth];

            if (path.size() == depth + 1) 
            {
                node.m_Data = data;
                return node;
            } 
            else 
            {
                return node.InsertInternal(path, depth + 1, data);
            }
        } 
        else 
        {
            throw std::string("depth >= path.size() in Hierarchy::InsertInternal.");
        }
    }

    std::string m_Name;  ///< Name of the node
    T* m_Data;  ///< Data of the node, nullptr if an end node
    std::map<std::string, Hierarchy> m_SubNodes;  ///< Subnodes, if not an end node
};

}  // namespace Mira
