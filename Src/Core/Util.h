// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <limits>
#include <sstream>
#include <string>
#include <vector>

namespace Mira 
{

std::vector<std::string> Split(const std::string& stringToSplit, const std::string& separator);
void RemoveChar(std::string& str, char c);

template <typename T>
std::string ToString(const T& t) 
{
    std::ostringstream os;
    os << t;
    return os.str();
}


/**
 *    Convert a string to an object.
 */
template <typename T>
T ToObject(const std::string& str) 
{
    T result;
    std::istringstream is(str);
    is >> result;
    return result;
}

std::vector<std::string> Split(const std::string& stringToSplit, char separator, char ignore = std::numeric_limits<char>::min());

}  // namespace Mira
