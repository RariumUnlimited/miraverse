// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <memory>
#include <vector>

#include <Core/Application/ApplicationDescriptor.h>
#include <Core/Assets/AssetManagerApp.h>
#include <Core/Formula/FormulaApp.h>
#include <Core/ImGuiDemoApp.h>
#include <Core/Window/WindowSettingsApp.h>

namespace Mira 
{

class CoreApplicationDescriptor : public ApplicationDescriptor 
{
public:
    void AddModuleApplicationToList(std::vector<std::unique_ptr<Application>>& applicationList) const override
    {
        applicationList.emplace_back(new NodeApp);
        applicationList.emplace_back(new WindowSettingsApp);
        applicationList.emplace_back(new ImGuiDemoApp);
        applicationList.emplace_back(new AssetManagerApp);
    }
};

}  // namespace Mira
