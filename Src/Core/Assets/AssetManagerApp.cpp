// Copyright RariumUnlimited - Licence : MIT
#include <Core/Assets/AssetManagerApp.h>

#include <Core/Assets/AssetManager.h>
#include <Core/Util.h>

namespace Mira 
{

AssetManagerApp::AssetManagerApp()
    : Application("/Core/Asset Manager", "Asset Manager") 
{

}

void AssetManagerApp::DrawImGuiInternal()
{
    std::vector<std::reference_wrapper<const AssetInfo>> allInfos;
    std::vector<std::string> allPath;
    std::vector<const char*> allPathForListBox;

    for (auto const&[key, value] : AssetManagerProxy::Get().m_Assets)
    {
        allInfos.push_back(std::ref(value));
        std::string str = ToString(value.m_Path);
        if (value.m_Asset)
        {
            str = "[*] " + str;
        }
        allPath.push_back(str);
        allPathForListBox.push_back(allPath.back().c_str());
    }

    ImGui::ListBox("All Assets", &m_SelectedIndex, allPathForListBox.data(), (int)allPathForListBox.size());

    if (m_SelectedIndex >= 0)
    {
        const AssetInfo& info = allInfos[m_SelectedIndex];
        ImGui::Separator();
        ImGui::Text("Asset : %u", info.m_Id);
        ImGui::Text("Path : %s", info.m_Path.string().c_str());

        ImGui::Text("Status : ");
        ImGui::SameLine();

        if (info.m_Asset)
        {
            ImGui::Text("Loaded");
            ImGui::Text("Ref Count : %i", info.m_Asset.use_count());
        }
        else
        {
            ImGui::Text("Unloaded");
        }

        if (info.m_Metadata.has_value())
        {
            ImGui::Separator();
            const PersistentObjectMetadata& metadata = info.m_Metadata.value();
            ImGui::Text("Persistent Object Metadata : ");
            ImGui::Indent();
            ImGui::Text("Object Name : %s", metadata.m_Name.c_str());
            ImGui::Text("Persistent Type Id : %u", metadata.m_PersistentTypeId);
            ImGui::Text("Object Id : %u", metadata.m_Id);
            ImGui::Text("Version : %i", metadata.m_Version);
            ImGui::Unindent();
        }
    }
}

}  // namespace Mira
