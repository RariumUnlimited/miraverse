// Copyright RariumUnlimited - Licence : MIT
#include <Core/Assets/FileSystem.h>

#include <fstream>

namespace Mira 
{

File LoadFile(const std::filesystem::path& path)
{
    File result;
    std::ifstream ifs;

    ifs.open(path, std::ifstream::in);

    if (!ifs.good())
    {
        throw std::string("Could not read file : " + path.string());
    }

    ifs.seekg(0, ifs.end);
    result.resize(ifs.tellg());
    ifs.seekg(0, ifs.beg);

    ifs.read(result.data(), result.size());

    ifs.close();

    return result;
}

SerializerStream::SerializerStream(std::basic_iostream<char>& stdStream)
    : m_StdStream(stdStream)
{

}

}  // namespace Mira
