// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <filesystem>
#include <iostream>
#include <vector>

namespace Mira 
{
class PersistentObject;

using File = std::vector<char>;

File LoadFile(const std::filesystem::path& path);

class SerializerStream
{
	template<typename T>
	friend SerializerStream& operator<<(SerializerStream& stream, const T& value);

	template<typename T>
	friend SerializerStream& operator>>(SerializerStream& stream, T& value);
public:
	SerializerStream(std::basic_iostream<char>& stdStream);

private:
	std::basic_iostream<char>& m_StdStream;
};

template<typename T>
static SerializerStream& operator<<(SerializerStream& stream, const T& value)
{
	stream.m_StdStream.write((const char*)(&value), sizeof(T));
	return stream;
}

template<typename T>
static SerializerStream& operator>>(SerializerStream& stream, T& value)
{
	stream.m_StdStream.read((char*)(&value), sizeof(T));
	return stream;
}

template<>
static SerializerStream& operator<<(SerializerStream& stream, const std::string& value)
{
	stream << value.size();
	stream.m_StdStream.write(value.data(), value.size());
	return stream;
}

template<>
static SerializerStream& operator>>(SerializerStream& stream, std::string& value)
{
	std::size_t size;
	stream >> size;
	value.resize(size);
	stream.m_StdStream.read(value.data(), size);
	return stream;
}

template<>
static SerializerStream& operator<<(SerializerStream& stream, const PersistentObject& value) = delete;

template<>
static SerializerStream& operator>>(SerializerStream& stream, PersistentObject& value) = delete;

}  // namespace Mira

