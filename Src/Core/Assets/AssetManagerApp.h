// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <imgui.h>

#include <Core/Application/Application.h>
#include <Core/Window/WindowHelper.h>

namespace Mira 
{

class AssetManagerApp : public Application 
{
public:
    AssetManagerApp();

protected:
    void DrawImGuiInternal() override;

    // ====<ImGui input variable>====
    int m_SelectedIndex{ -1 };
    // ====</ImGui input variable>===
};

}  // namespace Mira
