// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <Core/Assets/AssetManager.h>

namespace Mira
{

class AssetUser
{
public:
	~AssetUser()
	{
		for (const auto&[key, value] : m_InUseAssets)
		{
			if (value.use_count() == 2 && AssetManagerProxy::IsOpen()) // Us and the asset manager
			{
				AssetManagerProxy::Get().UnloadAsset(key);
			}
		}
	}

	template<typename T, IdType Id, typename... PreloadInfo>
	void PreloadAssets()
	{
		m_InUseAssets[Id] = AssetManagerProxy::Get().LoadAsset<T>(Id);

		if constexpr(sizeof...(PreloadInfo) > 0)
		{
			PreloadAssets<PreloadInfo...>();
		}
	}

	template<typename T>
	T& GetAsset(IdType id)
	{
		auto it = m_InUseAssets.find(id);

		if (it == m_InUseAssets.end())
		{
			std::shared_ptr<AssetBase>& assetPtr = AssetManagerProxy::Get().LoadAsset<T>(id);
			m_InUseAssets[id] = assetPtr;
			return assetPtr->GetAs<T>();
		}

		return it->second->GetAs<T>();
	}

	template<typename T>
	T& GetObject(IdType id)
	{
		return GetAsset<T>(id);
	}

	template<typename T>
	T& GetObject(const std::string& name)
	{
		return GetAsset<T>(GetIdFromString(name));
	}

	template<typename T>
	T& RequestPersistentObject(const std::string& objectName)
	{
		IdType id = GetIdFromString(objectName);
		auto it = m_InUseAssets.find(GetIdFromString(objectName));

		if (it == m_InUseAssets.end())
		{
			std::shared_ptr<AssetBase>& assetPtr = AssetManagerProxy::Get().RequestPersistentObjectAsset<T>(objectName);
			m_InUseAssets[id] = assetPtr;
			return assetPtr->GetAs<T>();
		}

		return it->second->GetAs<T>();
	}

	void SavePersistentObject(const PersistentObject& object)
	{
		AssetManagerProxy::Get().SavePersistentObject(object);
	}

	bool Exists(IdType id)
	{
		return AssetManagerProxy::Get().Exists(id);
	}

private:
	std::unordered_map<IdType, std::shared_ptr<AssetBase>> m_InUseAssets;
};


}  // namespace Mira