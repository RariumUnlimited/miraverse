// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <fstream>

#include <Core/Assets/AssetBase.h>
#include <Core/Assets/AssetInfo.h>
#include <Core/Assets/FileSystem.h>
#include <Core/Persistence/PersistentObject.h>

namespace Mira 
{

template<typename T>
class Asset : public AssetBase
{
public:
	template<typename U = T>
	Asset(CrcType id, const AssetInfo& info, std::enable_if_t<std::is_base_of_v<PersistentObject, U>>* = 0)
		: AssetBase(id)
		, m_Asset(info.m_Metadata.value().m_Name)
	{
		if (std::filesystem::exists(info.m_Path))
		{
			std::fstream file(info.m_Path, std::ios::in | std::ios::binary);
			SerializerStream stream(file);
			m_Asset.Load(stream);
			file.close();
		}
		// file doesn't exist == new persistent object
	}


	template<typename U = T>
	Asset(CrcType id, const AssetInfo& info, std::enable_if_t<std::is_constructible_v<U, const std::filesystem::path&>>* = 0)
		: AssetBase(id)
		, m_Asset(info.m_Path)
	{

	}

	template<typename U = T>
	Asset(CrcType id, const AssetInfo& info, std::enable_if_t<std::is_constructible_v<U, File&&>>* = 0)
		: AssetBase(id)
		, m_Asset(std::move(LoadFile(info.m_Path)))
	{

	}

	AssetType GetOwnedAssetType() override { return GetAssetType<T>(); }

protected:
	void* GetAsset() override { return (void*)(&m_Asset); }

private:
	T m_Asset;
};

}  // namespace Mira
