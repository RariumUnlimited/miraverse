// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <Core/Assets/FileSystem.h>
#include <Core/CompileTimeCrc.h>

namespace Mira 
{

using AssetType = std::size_t;

class AssetBase
{
public:
	template<typename T>
	static AssetType GetAssetType() { return typeid(T).hash_code(); }

	template<typename T>
	T& GetAs()
	{
		if (GetAssetType<T>() != GetOwnedAssetType())
		{
			throw std::string("Trying to get asset with incorrect type");
		}

		return *((T*)(GetAsset()));
	}

	virtual AssetType GetOwnedAssetType() = 0;
	CrcType GetId() { return m_Id; }

protected:
	AssetBase(CrcType id)
		: m_Id(id)
	{

	}

	virtual void* GetAsset() = 0;
	 
	CrcType m_Id;
};

}  // namespace Mira
