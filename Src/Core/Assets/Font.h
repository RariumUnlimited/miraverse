// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <SFML/Graphics.hpp>

#include <Core/Assets/FileSystem.h>

namespace Mira 
{

static constexpr IdType DefaultFontId = "Core/arial.ttf"_id;

class Font
{
public:
	Font(const std::filesystem::path& path)
		: m_Font(path)
	{

	}

	const sf::Font& Get() const { return m_Font; }

private:
	sf::Font m_Font;
};

}  // namespace Mira
