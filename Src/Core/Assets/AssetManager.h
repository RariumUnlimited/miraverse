// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <filesystem>
#include <unordered_map>
#include <unordered_set>

#include <Core/Assets/Asset.h>
#include <Core/Assets/AssetInfo.h>
#include <Core/Assets/FileSystem.h>
#include <Core/CompileTimeCrc.h>
#include <Core/Id.h>
#include <Core/Persistence/PersistentObject.h>
#include <Core/UniqueProxy.h>

namespace Mira
{

class AssetManagerApp;

class AssetManager
{
	friend class AssetManagerApp;

	using AssetMap = std::unordered_map<IdType, AssetInfo>;

public:
	AssetManager(std::string basePath);

	template<typename T>
	std::shared_ptr<AssetBase>& LoadAsset(IdType id)
	{
		return LoadAssetInternal<T>(id, {});
	}

	template<typename T>
	T& GetAsset(IdType id)
	{
		return GetAssetInternal<T>(id, {});
	}

	void UnloadAsset(IdType id);
	bool Exists(IdType id);
	bool IsLoaded(IdType id);

	void SavePersistentObject(const PersistentObject& object);

	template<typename T>
	std::shared_ptr<AssetBase>& RequestPersistentObjectAsset(const std::string& objectName)
	{
		static_assert(std::is_base_of_v<PersistentObject, T>);
		IdType id = GetIdFromString(objectName);

		auto it = m_Assets.find(id);

		if (it == m_Assets.end())
		{
			AssetInfo& info = m_Assets[id];
			info.m_Id = id;
			info.m_Path = m_BasePath + objectName;
			PersistentObjectMetadata metadata;
			metadata.m_PersistentTypeId = T::mg_PersistentTypeId;
			metadata.m_Name = objectName;
			metadata.m_Version = T::Version;
			metadata.m_Id = id;
			info.m_Metadata = metadata;
			info.m_Asset = std::make_shared<Asset<T>>(id, info);

			return info.m_Asset;
		}
		else
		{
			if (!it->second.m_Metadata.has_value())
			{
				throw std::string("Requesting existing persistent object " + objectName + " but no metadata are present.");
			}

			PersistentObjectMetadata& metadata = it->second.m_Metadata.value();

			if (T::mg_PersistentTypeId != metadata.m_PersistentTypeId)
			{
				throw std::string("Requesting existing persistent object " + objectName + " but type mismatch.");
			}

			return LoadAssetInternal<T>(id, it);
		}
	}

protected:
	void GatherAllAssetsInfo();

	template<typename T>
	std::shared_ptr<AssetBase>& LoadAssetInternal(IdType id, std::optional<AssetMap::iterator> itOptional)
	{
		auto it = itOptional.has_value() ? itOptional.value() : m_Assets.find(id);
		if (it == m_Assets.end())
		{
			if constexpr (std::is_base_of_v<PersistentObject, T>)
			{
				throw std::string("Trying to load a non-existing object");
			}
			else
			{
				throw std::string("Trying to load a non-existing asset");
			}
		}

		if (!it->second.m_Asset)
		{
			if (std::is_base_of_v<PersistentObject, T> && !it->second.m_Metadata.has_value())
			{
				throw std::string("Trying to load a persistent object with missing metadata");
			}

			it->second.m_Asset = std::make_shared<Asset<T>>(id, it->second);
		}

		return it->second.m_Asset;
	}

	template<typename T>
	T& GetAssetInternal(IdType id, std::optional<AssetMap::iterator> it)
	{
		return LoadAssetInternal<T>(id, it)->GetAs<T>();
	}

	AssetMap m_Assets;
	std::unordered_map<IdType, std::vector<std::reference_wrapper<AssetInfo>>> m_PersistentTypeToPersistentObjectId;
	std::unordered_set<IdType> m_PersistentObjectMetadata;
	std::string m_BasePath;
};

class AssetManagerProxy : public UniqueProxy<AssetManager>
{

};

}  // namespace Mira