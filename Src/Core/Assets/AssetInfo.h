// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <filesystem>

#include <Core/Assets/AssetBase.h>
#include <Core/Id.h>
#include <Core/Persistence/PersistentObjectMetadata.h>

namespace Mira
{

struct AssetInfo
{
	IdType m_Id;
	std::filesystem::path m_Path;
	std::shared_ptr<AssetBase> m_Asset;
	std::optional<PersistentObjectMetadata> m_Metadata;
};

}  // namespace Mira