// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <SFML/Graphics.hpp>

#include <Core/Assets/FileSystem.h>

namespace Mira 
{

class Texture
{
public:
	Texture(const std::filesystem::path& path)
		: m_Texture(path)
	{

	}

	const sf::Texture& Get() const { return m_Texture; }

private:
	sf::Texture m_Texture;
};

}  // namespace Mira
