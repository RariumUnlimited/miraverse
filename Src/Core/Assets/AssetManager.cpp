// Copyright RariumUnlimited - Licence : MIT
#include <Core/Assets/AssetManager.h>

#include <fstream>

#include <Core/Log.h>

namespace Mira 
{

AssetManager::AssetManager(std::string basePath)
    : m_BasePath(basePath)
{
    GatherAllAssetsInfo();
}

void AssetManager::UnloadAsset(IdType id)
{
    auto it = m_Assets.find(id);

    if (it != m_Assets.end())
    {
        if (it->second.m_Asset.use_count() > 1)
        {
            throw std::string("Trying to unload asset while someone is still using that");
        }
        it->second.m_Asset.reset();
    }
}

bool AssetManager::Exists(IdType id)
{
    return m_Assets.contains(id);
}

bool AssetManager::IsLoaded(IdType id)
{
    auto it = m_Assets.find(id);

    if (it != m_Assets.end())
    {
        return it->second.m_Asset.get() != nullptr;
    }

    return false;
}

void AssetManager::GatherAllAssetsInfo()
{
    m_Assets.clear();
    for (const std::filesystem::directory_entry& dirEntry :
        std::filesystem::recursive_directory_iterator(m_BasePath))
    {
        if (!dirEntry.is_regular_file())
        {
            continue;
        }

        std::string pathAsString = dirEntry.path().string();
        std::string strToFind = "\\";

        std::size_t pos = pathAsString.find(strToFind);
        while (pos != std::string::npos)
        {
            pathAsString.replace(pos, strToFind.size(), "/");
            pos = pathAsString.find(strToFind);
        }

        std::string pathForId = pathAsString;
        pathForId.erase(0, m_BasePath.size());

        AssetInfo info;
        info.m_Id = GetIdFromString(pathForId);
        info.m_Path = pathAsString;

        if (info.m_Path.string().find(PersistentObject::MetadataPostfix) != std::string::npos)
        {
            m_PersistentObjectMetadata.insert(info.m_Id);
        }

        m_Assets[info.m_Id] = info;
    }

    for (IdType metadataId : m_PersistentObjectMetadata)
    {
        PersistentObjectMetadata& metadata = GetAsset<PersistentObjectMetadata>(metadataId);

        auto it = m_Assets.find(metadata.m_Id);

        if (it == m_Assets.end())
        {
            throw std::string(metadata.m_Name + " object metadata exists but no the object");
        }
        m_PersistentTypeToPersistentObjectId[metadata.m_PersistentTypeId].push_back(std::ref(it->second));
        it->second.m_Metadata.emplace(metadata);
        m_Assets.erase(metadataId); // Don't do anything after that, metadata and it are invalid;
    }
}

void AssetManager::SavePersistentObject(const PersistentObject& object)
{
    auto it = m_Assets.find(object.GetId());

    if (it == m_Assets.end())
    {
        throw std::string("Trying to save a non existing object");
    }

    if (!it->second.m_Metadata.has_value())
    {
        throw std::string("Trying to save an object without metadata");
    }

    PersistentObjectMetadata& metadata = it->second.m_Metadata.value();

    std::filesystem::path metadataPath(m_BasePath + object.GetMetadataName());

    std::filesystem::path directoryPath = metadataPath;
    directoryPath.remove_filename();
    std::filesystem::create_directories(directoryPath);

    std::fstream metadataFile(m_BasePath + object.GetMetadataName(), std::ios::out | std::ios::binary | std::ios::trunc);

    if (!metadataFile.is_open())
    {
        throw std::string("Unable to open file " + metadataPath.string());
    }

    SerializerStream metadataStream(metadataFile);

    metadata.Save(metadataStream);

    metadataFile.close();

    std::filesystem::path objectPath(m_BasePath + object.GetName());
    std::fstream objectFile(m_BasePath + object.GetName(), std::ios::out | std::ios::binary | std::ios::trunc);

    SerializerStream objectStream(objectFile);

    object.Save(objectStream);

    objectFile.close();
}

}  // namespace Mira
