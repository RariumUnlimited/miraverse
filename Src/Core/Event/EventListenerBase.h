// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <functional>
#include <unordered_map>
#include <unordered_set>

namespace Mira
{

class EventDispatcherBase;

class EventListenerBase
{
public:
	virtual ~EventListenerBase() {}
	virtual void Dispatch(std::size_t eventId, void* event) = 0;
	virtual void SetRegistration(EventDispatcherBase& dispatcher) = 0;
	virtual void UnsetRegistration(EventDispatcherBase& dispatcher) = 0;
};

}  // namespace Mira
