// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <type_traits>
#include <unordered_set>

#include <Core/Event/EventDispatcherBase.h>
#include <Core/Event/EventListenerBase.h>

namespace Mira
{

template<class E>
class EventDispatcher : public EventDispatcherBase
{
public:
	virtual ~EventDispatcher()
	{
		for (EventListenerBase* listener : m_Listeners)
		{
			listener->UnsetRegistration(*this);
		}
	}

	void RegisterListener(EventListenerBase& listener) override
	{
		m_Listeners.insert(&listener);
		listener.SetRegistration(*this);
	}

	void UnregisterListener(EventListenerBase& listener) override
	{
		listener.UnsetRegistration(*this);
		m_Listeners.erase(&listener);
	}

	void Dispatch(const E& event)
	{
		for (EventListenerBase* listener : m_Listeners)
		{
			listener->Dispatch(typeid(E).hash_code(), (void*)(&event));
		}
	}

private:
	std::unordered_set<EventListenerBase*> m_Listeners;
};

}  // namespace Mira
