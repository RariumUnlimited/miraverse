// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <functional>
#include <unordered_map>
#include <unordered_set>

#include <Core/Event/EventDispatcherBase.h>
#include <Core/Event/EventListenerBase.h>

namespace Mira
{

template<class CRTP, class... Events>
class EventListener : public EventListenerBase
{
	static_assert(sizeof...(Events) > 0);

public:
	EventListener()
	{
		ProcessEvent<Events...>();
	}

	virtual ~EventListener()
	{
		std::unordered_set<EventDispatcherBase*> tempSet = m_DispatcherRegisteredTo;
		for (EventDispatcherBase* dispatcher : tempSet)
		{
			dispatcher->UnregisterListener(*this);
		}
	}

	void SetRegistration(EventDispatcherBase& dispatcher) override
	{
		m_DispatcherRegisteredTo.insert(&dispatcher);
	}

	void UnsetRegistration(EventDispatcherBase& dispatcher) override
	{
		m_DispatcherRegisteredTo.erase(&dispatcher);
	}

private:
	std::unordered_map<std::size_t, std::function<void(void*)>> m_EventIdToCall;
	std::unordered_set<EventDispatcherBase*> m_DispatcherRegisteredTo;

	void Dispatch(std::size_t eventId, void* event)
	{
		auto it = m_EventIdToCall.find(eventId);

		if (it != m_EventIdToCall.end())
		{
			it->second(event);
		}
	}

	template<class E, class... Es>
	void ProcessEvent()
	{
		m_EventIdToCall[typeid(E).hash_code()] = [&](void* event) { static_cast<CRTP*>(this)->OnEvent(*static_cast<E*>(event)); };
		ProcessEvent<Es...>();
	}

	template<class... Es> requires(sizeof...(Es) == 0)
	void ProcessEvent() { }
};

}  // namespace Mira
