// Copyright RariumUnlimited - Licence : MIT
#pragma once

namespace Mira
{

class EventListenerBase;

class EventDispatcherBase
{
public:
	virtual ~EventDispatcherBase() {}
	virtual void RegisterListener(EventListenerBase& listener) = 0;
	virtual void UnregisterListener(EventListenerBase& listener) = 0;
};

}  // namespace Mira
