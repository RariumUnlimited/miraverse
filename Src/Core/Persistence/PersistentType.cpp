// Copyright RariumUnlimited - Licence : MIT
#include <Core/Persistence/PersistentType.h>


namespace Mira 
{

PersistentType::PersistentType(IdType id)
	: m_Id(id)
{

}

}  // namespace Mira
