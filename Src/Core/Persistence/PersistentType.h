// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <functional>
#include <unordered_map>

#include <Core/Id.h>

namespace Mira 
{

class PersistentTypeManager
{
public:
	static void RegisterType(IdType id);

private:
	std::unordered_map<IdType, IdType> m_AllPersistentTypes;
};

class PersistentType
{
public:
	PersistentType(IdType id);

	IdType GetId() const { return m_Id; }

private:
	IdType m_Id;
};

}  // namespace Mira
