// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <Core/Assets/FileSystem.h>
#include <Core/Id.h>

namespace Mira 
{

#define DECLARE_PERSISTENT_TYPE(type) static inline IdType mg_PersistentTypeId = GetIdFromString(#type);

class PersistentObject
{
public:
	static constexpr const char* MetadataPostfix = ".metadata";
	static constexpr int Version = 1;

	const std::string& GetName() const { return m_Name; }
	const std::string& GetMetadataName() const { return m_MetadataName; }
	IdType GetId() const { return m_Id; }
	IdType GetMetadataId() const { return m_MetadataId; }

	virtual void Save(SerializerStream& stream) const {}
	virtual void Load(SerializerStream& stream) {}

protected:
	PersistentObject(const std::string& name);

private:
	std::string m_Name;
	std::string m_MetadataName;
	IdType m_Id;
	IdType m_MetadataId;
};

}  // namespace Mira
