// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <Core/Assets/AssetUser.h>
#include <Core/Id.h>

namespace Mira 
{

template<typename T>
class PORef : protected AssetUser
{
public:

	T* operator->() const
	{
		if (!m_Object)
		{
			throw std::string("Trying to access unloaded PORef");
		}

		return m_Object;
	}

	void LoadReference(IdType id)
	{
		m_Object = &GetObject<T>(id);
	}

	operator bool() const { return m_Object != nullptr; }
	T& operator*() const
	{
		if (!m_Object)
		{
			throw std::string("Trying to access unloaded PORef");
		}
		std::unique_ptr<int> ind;
		return *m_Object; 
	}

private:
	T* m_Object{ nullptr };
};

template<typename T>
static SerializerStream& operator<<(SerializerStream& stream, const PORef<T>& value)
{
	if (value)
	{
		stream << value->GetId();
	}
	return stream;
}

template<typename T>
static SerializerStream& operator>>(SerializerStream& stream, PORef<T>& value)
{
	IdType id{ 0 };
	stream >> id;
	if (id != 0)
	{
		value.LoadReference(id);
	}
	return stream;
}

}  // namespace Mira
