// Copyright RariumUnlimited - Licence : MIT
#include <Core/Persistence/PersistentObject.h>


namespace Mira 
{

PersistentObject::PersistentObject(const std::string& name)
	: m_Name(name)
	, m_MetadataName(name + MetadataPostfix)
	, m_Id(GetIdFromString(name))
	, m_MetadataId(GetIdFromString(m_MetadataName))
{

}

}  // namespace Mira
