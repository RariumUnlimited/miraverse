// Copyright RariumUnlimited - Licence : MIT
#include <Core/Persistence/PersistentObjectMetadata.h>

#include <sstream>

namespace Mira 
{

PersistentObjectMetadata::PersistentObjectMetadata(File&& file)
{
	std::stringstream sstream(std::ios::in | std::ios::out | std::ios::binary);
	sstream.write(file.data(), file.size());
	sstream.seekp(0);
	SerializerStream stream(sstream);

	Load(stream);
}

void PersistentObjectMetadata::Load(SerializerStream& stream)
{
	int version;
	stream >> version >> m_PersistentTypeId >> m_Id >> m_Name >> m_Version;
}

void PersistentObjectMetadata::Save(SerializerStream& stream)
{
	stream << Version << m_PersistentTypeId << m_Id << m_Name << m_Version;
}

}  // namespace Mira
