// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <Core/Assets/FileSystem.h>
#include <Core/Id.h>

namespace Mira 
{

struct PersistentObjectMetadata
{
	IdType m_PersistentTypeId;
	IdType m_Id;
	std::string m_Name;
	int m_Version;

	static constexpr int Version = 1;

	PersistentObjectMetadata() = default;
	PersistentObjectMetadata(const PersistentObjectMetadata& other) = default;
	PersistentObjectMetadata(File&& file);
	void Load(SerializerStream& stream);
	void Save(SerializerStream& stream);
};

}  // namespace Mira
