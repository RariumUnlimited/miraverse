// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <map>
#include <memory>
#include <string>
#include <utility>

namespace Mira 
{

/// A catalog of unique assets
template<typename A>
class Catalog
{
public:
    Catalog() = default;
    Catalog(const Catalog& other) = delete;
    Catalog(const Catalog&& other) = delete;

    // Add a new asset to the catalog.
    template<typename... Args>
    A* Add(std::string name, Args... args) 
    {
        auto[it, result] = m_Assets.try_emplace(name, nullptr);

        if (result)
        {
            it->second = std::make_unique<A>(std::forward<Args>(args)...);
        }

        return it->second.get();
    }

    // Add a new asset to the catalog while defining a child class of A.
    template<typename C, typename... Args>
    A* Add(std::string name, Args... args) 
    {
        auto[it, result] = m_Assets.try_emplace(name, nullptr);

        if (result)
        {
            it->second = std::make_unique<C>(std::forward<Args>(args)...);
        }

        return it->second.get();
    }

    // Override an existing asset to the catalog. The given pointer is now the property of this catalog, no need to delete it.
    template<typename... Args>
    A* Override(std::string name, Args... args) 
    {
        A* result;
        std::unique_ptr<A> temp = std::make_unique<A>(std::forward<Args>(args)...);
        result = temp.get();
        m_Assets[name] = std::move(temp);

        return result;
    }

    // Override an existing asset to the catalog using a child class. The given pointer is now the property of this catalog, no need to delete it.
    template<typename C, typename... Args>
    A* Override(std::string name, Args... args) 
    {
        A* result;
        std::unique_ptr<A> temp = std::make_unique<C>(std::forward<Args>(args)...);
        result = temp.get();
        m_Assets[name] = std::move(temp);

        return result;
    }

    void Delete(std::string name)  { m_Assets.erase(name); }
    A* operator[](std::string name) { return Get(name); }

    // Purge the catalog of its assets
    void Purge() { m_Assets.clear(); }

    // Get an asset, or nullptr if it doesn't exist
    A* Get(std::string name)  
    {
        auto it = m_Assets.find(name);
        return it != m_Assets.end() ? it->second.get() : nullptr;
    }

private:
    std::map<std::string, std::unique_ptr<A>> m_Assets;  ///< All assets currently loaded
};

}  // namespace Mira
