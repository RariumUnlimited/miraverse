// Copyright RariumUnlimited - Licence : MIT
#include <Core/CompileTimeCrc.h>

CrcType CRC(std::string str)
{
    return crcdetail::compute(str.c_str(), (uint32_t)str.size());
}