// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <string>

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include <Core/Event/EventDispatcher.h>
#include <Core/Window/InputState.h>

namespace Mira 
{

class Entry;

class Application
{
public:
    enum class ECloseBehaviour 
    {
        Minimize,
        Stop
    };

    Application(std::string path, std::string name, ECloseBehaviour closeBehaviour = ECloseBehaviour::Minimize);
    Application(const Application& other) = delete;
    Application(const Application&& other) = delete;

    virtual ~Application() { }

    bool DrawImGui();
    void DrawGraphic(sf::RenderTarget& renderTarget);
    virtual void Update(float delta, const InputState& currentInput, const InputState& previousInput) { }
    void OnApplicationLaunched();
    void OnApplicationClosed();

    virtual void OnRenderSizeChanged(sf::Vector2u newRenderSize) { }

    const std::string& GetPath() const { return m_Path; }
    bool IsFocused() { return m_IsFocused; }
    bool IsHovered() { return m_IsHovered; }
    bool IsAppearing() { return m_IsAppearing; }
    bool IsCollapsed() { return m_IsCollapsed; }
    ECloseBehaviour GetCloseBehaviour() { return m_CloseBehaviour; }
    virtual bool RequireGraphicDraw() const { return false; }

    // =======<Events>========
    // These events are only call if the app requires graphic and is currently being display
    EventDispatcher<MouseButtonPressedEvent> OnMouseButtonPressed;  ///< Event when the user press a mouse button
    EventDispatcher<MouseButtonReleasedEvent> OnMouseButtonReleased;  ///< Event when the user release a mouse button
    EventDispatcher<MouseButtonClickEvent> OnMouseButtonClick;  ///< Event when the use click
    EventDispatcher<MouseButtonDoubleClickEvent> OnMouseButtonDoubleClick;  ///< Event when the user double click
    EventDispatcher<MouseBeginDrag> OnMouseBeginDrag;  ///< Event when the user start dragging
    EventDispatcher<MouseEndDragEvent> OnMouseEndDrag;  ///< Event when the user start dragging
    EventDispatcher<MouseDragEvent> OnMouseDrag;  ///< Event when the user drag the mouse
    EventDispatcher<MouseWheelEvent> OnMouseWheelScrolled;  ///< Event when the user uses his mouse wheel
    EventDispatcher<KeyPressedEvent> OnKeyPressed;  ///< For each key we have a corresponding event (when the user press a key)
    EventDispatcher<KeyReleasedEvent> OnKeyReleased;  ///< For each key we have a corresponding event (when the user releases a key)


protected:
    virtual void DrawImGuiInternal() { }
    virtual void DrawGraphicInternal(sf::RenderTarget& renderTarget) { }
    virtual void OnStart() { }
    virtual void OnStop() { }
    virtual void OnDisplay() { } // Called each time the application is shown on screen : after starting or after the app is maximized
    virtual void OnHide() { } // Called each time the application is hidden from screen : after closing or after the app is minimized

    std::string m_Path;
    std::string m_Name;
    bool m_IsStarted{ false };
    bool m_AutoResize{ false };
    bool m_IsFocused{ false };
    bool m_IsHovered{ false };
    bool m_IsAppearing{ false };
    bool m_IsCollapsed{ false };
    ECloseBehaviour m_CloseBehaviour;
};

}  // namespace Mira
