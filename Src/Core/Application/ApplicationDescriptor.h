// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <memory>
#include <vector>

#include <Core/Application/Application.h>

namespace Mira 
{

class ApplicationDescriptor 
{
public:
    virtual void AddModuleApplicationToList(std::vector<std::unique_ptr<Application>>& applicationList) const = 0;
};

}  // namespace Mira
