// Copyright RariumUnlimited - Licence : MIT
#include <Core/Application/ApplicationManager.h>

#include <imgui.h>

#include <Core/Log.h>

namespace Mira 
{

ApplicationManager::~ApplicationManager() 
{
    for (Application* app : m_AppToDraw) 
    {
        app->OnApplicationClosed();
    }
    m_AppToDraw.clear();
}

void ApplicationManager::DrawImGui() 
{
    if (ImGui::BeginMainMenuBar()) 
    {
        for (const auto& p : m_AppHierarchy->GetSubNodes()) 
        {
            DrawHierarchyImGui(p.second);
        }
        m_MenuHeight = ImGui::GetWindowHeight();
        ImGui::EndMainMenuBar();
    }

    std::unordered_set<Application*> appToStopDraw;

    for (Application* app : m_AppToDraw) 
    {
        if (!app->DrawImGui())
        {
            appToStopDraw.insert(app);
        }

        if (app->IsFocused() && m_GraphicApp != app && app->RequireGraphicDraw())
        {
            m_GraphicApp = app;
        }
    }

    for (Application* app : appToStopDraw) 
    {
        app->OnApplicationClosed();
        if (m_GraphicApp == app)
        {
            m_GraphicApp = nullptr;
        }
        m_AppToDraw.erase(app);
    }

    ImGui::IsWindowFocused();

    appToStopDraw.clear();
}

void ApplicationManager::DrawGraphic(sf::RenderTarget& renderTarget) 
{
    if (m_GraphicApp != nullptr)
    {
        m_GraphicApp->DrawGraphic(renderTarget);
    }
}

void ApplicationManager::Update(float delta, const InputState& currentInput, const InputState& previousInput) 
{
    for (Application* app : m_AppToDraw) 
    {
        app->Update(delta, currentInput, previousInput);
    }
}

void ApplicationManager::DrawHierarchyImGui(const Hierarchy<Application>& hierarchy) 
{
    if (hierarchy.IsParentNode()) 
    {
        if (ImGui::BeginMenu(hierarchy.GetName().c_str())) 
        {
            for (const auto& p : hierarchy.GetSubNodes()) 
            {
                DrawHierarchyImGui(p.second);
            }
            ImGui::EndMenu();
        }

    } 
    else if (hierarchy.IsEndNode()) 
    {
        if (ImGui::MenuItem(hierarchy.GetName().c_str())) 
        {
            m_AppToDraw.insert(hierarchy.GetData());
            hierarchy.GetData()->OnApplicationLaunched();
        }
    }
}

void ApplicationManager::NotifyWindowResize(sf::Vector2u newRenderSize) 
{
    for (Application* app : m_AppToDraw) 
    {
        app->OnRenderSizeChanged(newRenderSize);
    }
}

}  // namespace Mira
