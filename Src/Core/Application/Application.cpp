// Copyright RariumUnlimited - Licence : MIT
#include <Core/Application/Application.h>

#include <imgui.h>

#include <Core/Log.h>

namespace Mira 
{

Application::Application(std::string path, std::string name, ECloseBehaviour closeBehaviour)
    : m_Path(path)
    , m_Name(name)
    , m_CloseBehaviour(closeBehaviour) { }

bool Application::DrawImGui() 
{
    bool result = true;

    ImGuiWindowFlags flags = ImGuiWindowFlags_None;

    if (m_AutoResize)
    {
        flags |= ImGuiWindowFlags_AlwaysAutoResize;
    }

    if (ImGui::Begin(m_Name.c_str(), &result, flags))
    {
        DrawImGuiInternal();
    }

    m_IsFocused = ImGui::IsWindowFocused();
    m_IsHovered = ImGui::IsWindowHovered();
    m_IsAppearing = ImGui::IsWindowAppearing();
    m_IsCollapsed = ImGui::IsWindowCollapsed();
    ImGui::End();

    return result;
}

void Application::DrawGraphic(sf::RenderTarget& renderTarget) 
{
    DrawGraphicInternal(renderTarget);
}

void Application::OnApplicationLaunched() 
{
    if (!m_IsStarted) 
    {
        OnStart();
        m_IsStarted = true;
    }

    OnDisplay();
}

void Application::OnApplicationClosed() 
{
    switch (m_CloseBehaviour) 
    {
    case ECloseBehaviour::Stop:
        OnHide();
        OnStop();
        break;
    case ECloseBehaviour::Minimize:
        OnHide();
        break;
    }
}

}  // namespace Mira
