// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <unordered_set>

#include <SFML/Graphics.hpp>

#include <Core/Application/Application.h>
#include <Core/Hierarchy.h>

namespace Mira 
{

class ApplicationManager
{
public:
    ApplicationManager() = default;
    ApplicationManager(const ApplicationManager& other) = delete;
    ApplicationManager(const ApplicationManager&& other) = delete;
    ~ApplicationManager();

    void DrawImGui();
    void DrawGraphic(sf::RenderTarget& renderTarget);
    void Update(float delta, const InputState& currentInput, const InputState& previousInput);
    void NotifyWindowResize(sf::Vector2u newRenderSize);

    void SetAppHierarchy(Hierarchy<Application>* appHierarchy) { this->m_AppHierarchy = appHierarchy; }
    float GetMenuHeight() { return m_MenuHeight; }
    Application* GetOnScreenGraphicApp() { return m_GraphicApp; }

private:
    void DrawHierarchyImGui(const Hierarchy<Application>& hierarchy);

    std::unordered_set<Application*> m_AppToDraw;
    Hierarchy<Application>* m_AppHierarchy;
    Application* m_GraphicApp{ nullptr };
    float m_MenuHeight{ 10.f };
};

}  // namespace Mira
