// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <imgui.h>

#include <Core/Application/Application.h>
#include <Core/Window/WindowHelper.h>

namespace Mira 
{

class ImGuiDemoApp : public Application 
{
public:
    ImGuiDemoApp();

protected:
    void DrawImGuiInternal() override;

private:
    // ====<ImGui input variable>====
    bool m_IsOpen{ true };
    // ====</ImGui input variable>===
};

}  // namespace Mira
