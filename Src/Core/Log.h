// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <ostream>
#include <string>
#include <vector>

namespace Mira 
{

/**
 *    Singleton used to log message to a file, this class is thread safe
 */
class Logger 
{
public:
    static Logger& Log(bool outputTime = true);
    void AddStream(std::ostream* stream);

    template<typename T>
    friend Logger& operator<<(Logger& log, T t);

private:
    /// Build a logger
    Logger() = default;
    Logger(const Logger& other) = delete;
    Logger(const Logger&& other) = delete;

    static constexpr std::size_t K_TIME_BUFFER_SIZE = 512;

    std::vector<std::ostream*> m_Streams;
};

template<typename T>
Logger& operator<<(Logger& log, T t) 
{
    for (std::ostream* s : log.m_Streams) 
    {
        *s << t;
    }
    return log;
}

}  // namespace Mira
