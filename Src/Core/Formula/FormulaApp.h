// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <cstring>
#include <functional>
#include <limits>
#include <map>
#include <string>
#include <vector>

#include <imgui.h>

#include <Core/Application/Application.h>
#include <Core/Formula/Formula.h>
#include <Core/Formula/OpeNodes.h>

namespace Mira 
{

class NodeApp : public Application 
{
public:
    NodeApp();

protected:
    struct Result 
    {
        enum class EStatus 
        {
            Idle,
            Deleted,
            Added
        } m_Status{ EStatus::Idle };

        OpeNode* m_NewNode{ nullptr };
    };

    void DrawImGuiInternal() override;
    Result DisplayNode(OpeNode* n, int& id);
    OpeNode* DrawNewLine();
    std::map<std::string, std::function<OpeNode*(void)>>& GetNewNodeMap();

    // return true if the node has been deleted by the user.
    bool DrawNodeLine(OpeNode* n);
    void ComputeGraphValues();

    // ====<ImGui variable>====
    float m_PlotHeight{ 160.f };
    std::vector<float> m_GraphValues;
    int m_SampleCount{ 200 };
    float m_XMin{ 0.f };
    float m_XMax{ 1.f };
    float m_YMin{ std::numeric_limits<float>::max() };
    float m_YMax{ std::numeric_limits<float>::min() };
    std::string m_GraphLabel{ "Graph" };
    std::string m_NewNodeSelected{ "C" };
    char m_FormulaStr[512];
    std::string m_FormulaErrorText{ "" };
    // ====</ImGui variable>===

    Formula m_Formula;
};

}  // namespace Mira