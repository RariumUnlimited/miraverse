// Copyright RariumUnlimited - Licence : MIT
#include <Core/Formula/FormulaApp.h>

#include <algorithm>
#include <utility>

namespace Mira 
{

NodeApp::NodeApp() : Application("/Core/Formula", "Formula") 
{
    C* c = new C();
    c->m_Value = 2.f;

    Sin* sin = new Sin();
    sin->SetSubnode(0, new T());

    Pow* p = new Pow();
    p->SetSubnode(0, sin);
    p->SetSubnode(1, c);
    m_Formula.SetFormulaNode(p);

    std::snprintf(m_FormulaStr, IM_ARRAYSIZE(m_FormulaStr), "%s", m_Formula.GetFormulaNode()->GetString().c_str());

    ComputeGraphValues();
}

void NodeApp::DrawImGuiInternal()  
{
    ImGui::PlotLines(m_GraphLabel.c_str(), m_GraphValues.data(), (int)m_GraphValues.size(), 0, NULL, m_YMin, m_YMax, ImVec2(0, m_PlotHeight));

    ImGui::Separator();

    ImGui::InputText("Formula", m_FormulaStr, IM_ARRAYSIZE(m_FormulaStr));
    if (ImGui::Button("Interpret")) 
    {
        if (!m_Formula.Interpret(std::string(m_FormulaStr)))
        {
            m_FormulaErrorText = "Error !";
        }
        else
        {
            m_FormulaErrorText = "";
        }
    }
    ImGui::SameLine();
    ImGui::Text("%s", m_FormulaErrorText.c_str());

    ImGui::Separator();

    ImGui::Text("Y Min : %f", m_YMin);
    ImGui::Text("Y Max : %f", m_YMax);
    ImGui::InputInt("Sample Count", &m_SampleCount);
    ImGui::InputFloat("X min", &m_XMin);
    ImGui::InputFloat("X Max", &m_XMax);
    ImGui::InputFloat("Plot Height", &m_PlotHeight);

    if (m_XMin > m_XMax)
    {
        std::swap(m_XMin, m_XMax);
    }

    ImGui::Separator();

    int id = 0;
    Result r = DisplayNode(m_Formula.GetFormulaNode(), id);
    if (r.m_Status == Result::EStatus::Deleted) 
    {
        m_Formula.SetFormulaNode(nullptr);
    } 
    else if (r.m_Status == Result::EStatus::Added) 
    {
        m_Formula.SetFormulaNode(r.m_NewNode);
    }

    if (m_Formula.GetFormulaNode() != nullptr && m_Formula.GetFormulaNode()->Validate())
    {
        ComputeGraphValues();
    }
}

NodeApp::Result NodeApp::DisplayNode(OpeNode* n, int& id) 
{
    Result r;
    r.m_Status = Result::EStatus::Idle;

    ImGui::PushID(id++);

    if (n == nullptr) 
    {
        OpeNode* newNode = DrawNewLine();
        if (newNode != nullptr) 
        {
            r.m_Status = Result::EStatus::Added;
            r.m_NewNode = newNode;
        }
    } 
    else 
    {
        if (n->IsLeaf()) 
        {
            if (DrawNodeLine(n)) 
            {
                r.m_Status = Result::EStatus::Deleted;
            }
        } 
        else 
        {
            if (DrawNodeLine(n)) 
            {
                r.m_Status = Result::EStatus::Deleted;
            } 
            else 
            {
                for (int i = 0; i < n->GetSubNodesCount(); ++i) 
                {
                    ImGui::Indent();
                    ImGui::PushID(id++);

                    OpeNode* next = n->GetSubnode(i);

                    Result displayResult = DisplayNode(next, id);
                    if (displayResult.m_Status == Result::EStatus::Deleted) 
                    {
                        delete next;
                        n->SetSubnode(i, nullptr);
                    } 
                    else if (displayResult.m_Status == Result::EStatus::Added) 
                    {
                        n->SetSubnode(i, displayResult.m_NewNode);
                    }

                    ImGui::PopID();
                    ImGui::Unindent();
                }
            }
        }
    }

    ImGui::PopID();

    return r;
}

OpeNode* NodeApp::DrawNewLine() 
{
    OpeNode* newNode = nullptr;

    ImGui::Text("...");
    ImGui::SameLine();
    if (ImGui::Button("New"))
    {
        ImGui::OpenPopup("NewNodePopup");
    }

    if (ImGui::BeginPopup("NewNodePopup")) 
    {
        std::map<std::string, std::function<OpeNode*(void)>>& newNodeMap = GetNewNodeMap();

        if (ImGui::BeginListBox("##NewNodeListBox")) 
        {
            for (auto pair : newNodeMap) 
            {
                const bool isSelected = m_NewNodeSelected == pair.first;
                if (ImGui::Selectable(pair.first.c_str(), isSelected))
                {
                    m_NewNodeSelected = pair.first;
                }

                if (isSelected)
                {
                    ImGui::SetItemDefaultFocus();
                }
            }

           ImGui::EndListBox();
        }

        ImGui::Separator();
        if (ImGui::Button("Create")) 
        {
            newNode = newNodeMap[m_NewNodeSelected]();
            ImGui::CloseCurrentPopup();
        }

        ImGui::SameLine();

        if (ImGui::Button("Cancel")) 
        {
            ImGui::CloseCurrentPopup();
        }

        ImGui::EndPopup();
    }

    return newNode;
}

std::map<std::string, std::function<OpeNode*(void)>>& NodeApp::GetNewNodeMap() 
{
    static std::map<std::string, std::function<OpeNode*(void)>> newNodeMap
    {
        { "C", []() { return new C(); } },
        { "T", []() { return new T(); } },
        { "Pi", []() { return new Pi(); } },
        { "Add", []() { return new Add(); } },
        { "Mult", []() { return new Mult(); } },
        { "Sub", []() { return new Sub(); } },
        { "Div", []() { return new Div(); } },
        { "Sin", []() { return new Sin(); } },
        { "Cos", []() { return new Cos(); } },
        { "Tan", []() { return new Tan(); } },
        { "ASin", []() { return new ASin(); } },
        { "ACos", []() { return new ACos(); } },
        { "ATan", []() { return new ATan(); } },
        { "Abs", []() { return new Abs(); } },
        { "Exp", []() { return new Exp(); } },
        { "Ln", []() { return new Ln(); } },
        { "Log", []() { return new Log(); } },
        { "Sqrt", []() { return new Sqrt(); } },
        { "Max", []() { return new Max(); } },
        { "Min", []() { return new Min(); } },
        { "Pow", []() { return new Pow(); } }
    };

    return newNodeMap;
}

bool NodeApp::DrawNodeLine(OpeNode* n) 
{
    bool result = false;
    ImGui::Text("%s _ %s", n->GetDesc().c_str(), n->GetString().c_str());
    n->DrawCustomWidget();
    ImGui::SameLine();
    if (ImGui::Button("Delete")) 
    {
        result = true;
    }
    return result;
}

void NodeApp::ComputeGraphValues() 
{
    m_GraphValues.resize(m_SampleCount);
    m_YMin = std::numeric_limits<float>::max();
    m_YMax = std::numeric_limits<float>::min();
    m_GraphLabel = m_Formula.GetFormulaNode()->GetString();

    float step = (m_XMax - m_XMin) / static_cast<float>(m_SampleCount);
    float t = m_XMin;

    for (int i = 0; i < m_SampleCount; ++i) 
    {
        float value = m_Formula.GetFormulaNode()->Evaluate(t);
        m_GraphValues[i] = value;
        t += step;
        m_YMin = std::min(value, m_YMin);
        m_YMax = std::max(value, m_YMax);
    }
}

}  // namespace Mira
