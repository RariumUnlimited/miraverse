// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <functional>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <Core/Formula/OpeNodes.h>

namespace Mira 
{

class Formula 
{
public:
    Formula();
    ~Formula();
    bool Interpret(std::string formula);
    void SetFormulaNode(OpeNode* newNode);
    OpeNode* GetFormulaNode() { return m_Node; }

private:
    // Interpret a formula
    OpeNode* InterpretInternal(std::string formula);
    // Interpret a formula of type : (<Something>).
    OpeNode* InterpretInsideParenthesis(std::string formula);
    // Interpret a formation of type : <Function>(<Something). Ex : sin(t + 2) or pow(t, 2)
    OpeNode* InterpretFunctionOperator(std::string formula);
    // Interpret an operand of a basic operation. There must not be any simple operation inside : *, /, + or -
    OpeNode* InterpretOperand(std::string operand);
    // Check if the formula is a letter constant. Ex : pi.
    bool GetNumber(std::string formula, float& result);
    // Return true if c is a simple operation : *, /, + or -
    bool IsCharSimpleOperation(char c);
    int GetNextHighPriorityOperationIndex(const std::vector<std::string>& operations);

    std::unordered_map<std::string, std::function<OpeNode*()>> m_Functions;
    std::unordered_set<std::string> m_LetterConstant;
    std::unordered_set<char> m_SimpleOperation;
    OpeNode* m_Node{ nullptr };
};

}  // namespace Mira
