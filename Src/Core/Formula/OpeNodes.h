// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <cmath>
#include <numbers>
#include <string>

#include <imgui.h>

#include <Core/Math.h>
#include <Core/Util.h>

namespace Mira 
{

class OpeNode 
{
public:
    virtual ~OpeNode() { }
    virtual float Evaluate(float t) const = 0;
    virtual bool Validate() const { return true; }
    virtual std::string GetString() const = 0;
    virtual std::string GetDesc() const = 0;
    virtual int GetSubNodesCount() const { return 0; }
    virtual void SetSubnode(int idx, OpeNode* subnode) { }
    virtual OpeNode* GetSubnode(int idx) { return nullptr; }
    virtual void DrawCustomWidget() { }
    bool IsLeaf() const { return GetSubNodesCount() == 0; }
};

template<int N>
class OpeNodeWithSub : public OpeNode 
{
public:
    static_assert(N > 0, "Sub nodes count needs to be > 0");

    OpeNodeWithSub() 
    {
        for (int i = 0; i < N; ++i)
        {
            m_Subnodes[i] = nullptr;
        }
    }

    ~OpeNodeWithSub() 
    {
        for (int i = 0; i < N; ++i)
        {
            if (m_Subnodes[i] != nullptr)
            {
                delete m_Subnodes[i];
            }
        }
    }

    virtual float SubEvaluate(float t) const = 0;

    float Evaluate(float t) const final 
    {
        return Validate() ? SubEvaluate(t) : 0.f;
    }

    bool Validate() const override 
    {
        bool valid = true;
        for (int i = 0; i < N; ++i)
        {
            valid = valid && (m_Subnodes[i] != nullptr && m_Subnodes[i]->Validate());
        }
        return valid;
    }

    int GetSubNodesCount() const override { return N; }
    void SetSubnode(int idx, OpeNode* subnode) override { m_Subnodes[Clamp(idx, 0, N - 1)] = subnode; }
    OpeNode* GetSubnode(int idx) override { return m_Subnodes[Clamp(idx, 0, N - 1)]; }

protected:
    OpeNode* m_Subnodes[N];
};

class C : public OpeNode 
{
public:
    C() { }
    explicit C(float value) : m_Value(value) { }
    float Evaluate(float t) const override { return m_Value; }
    bool Validate() const override { return true; }
    std::string GetString() const override { return ToString(m_Value); }
    std::string GetDesc() const override { return "C"; }

    void DrawCustomWidget() override 
    {
        ImGui::SameLine();
        ImGui::InputFloat("", &m_Value);
    }

    float m_Value{ 0.f };
};

class T : public OpeNode 
{
public:
    float Evaluate(float t) const override { return t; }
    bool Validate() const override { return true; }
    std::string GetString() const override { return "t"; }
    std::string GetDesc() const override { return "t"; }
};

class Pi : public OpeNode 
{
public:
    float Evaluate(float t) const override { return (float)std::numbers::pi; }
    bool Validate() const override { return true; }
    std::string GetString() const override { return "Pi"; }
    std::string GetDesc() const override { return "Pi"; }
};

class Add : public OpeNodeWithSub<2> 
{
public:
    float SubEvaluate(float t) const override { return m_Subnodes[0]->Evaluate(t) + m_Subnodes[1]->Evaluate(t); }
    std::string GetString() const override 
    {
        std::string result;

        if (m_Subnodes[0] != nullptr)
        {
            result += ToString(m_Subnodes[0]->GetString());
        }
        else
        {
            result += "...";
        }

        result += " + ";

        if (m_Subnodes[1] != nullptr)
        {
            result += ToString(m_Subnodes[1]->GetString());
        }
        else
        {
            result += "...";
        }

        return result;
    }
    std::string GetDesc() const override { return "... + ..."; }
};

class Mult : public OpeNodeWithSub<2> 
{
public:
    float SubEvaluate(float t) const override { return m_Subnodes[0]->Evaluate(t) * m_Subnodes[1]->Evaluate(t); }
    std::string GetString() const override 
    {
        std::string result;

        if (m_Subnodes[0] != nullptr)
        {
            result += ToString(m_Subnodes[0]->GetString());
        }
        else
        {
            result += "...";
        }

        result += " * ";

        if (m_Subnodes[1] != nullptr)
        {
            result += ToString(m_Subnodes[1]->GetString());
        }
        else
        {
            result += "...";
        }

        return result;
    }
    std::string GetDesc() const override { return "... * ..."; }
};

class Sub : public OpeNodeWithSub<2> 
{
public:
    float SubEvaluate(float t) const override { return m_Subnodes[0]->Evaluate(t) - m_Subnodes[1]->Evaluate(t); }
    std::string GetString() const override 
    {
        std::string result;

        if (m_Subnodes[0] != nullptr)
        {
            result += ToString(m_Subnodes[0]->GetString());
        }
        else
        {
            result += "...";
        }

        result += " - ";

        if (m_Subnodes[1] != nullptr)
        {
            result += ToString(m_Subnodes[1]->GetString());
        }
        else
        {
            result += "...";
        }

        return result;
    }
    std::string GetDesc() const override { return "... - ..."; }
};

class Div : public OpeNodeWithSub<2> 
{
public:
    float SubEvaluate(float t) const override { return m_Subnodes[0]->Evaluate(t) / m_Subnodes[1]->Evaluate(t); }
    std::string GetString() const override 
    {
        std::string result;

        if (m_Subnodes[0] != nullptr)
        {
            result += ToString(m_Subnodes[0]->GetString());
        }
        else
        {
            result += "...";
        }

        result += " / ";

        if (m_Subnodes[1] != nullptr)
        {
            result += ToString(m_Subnodes[1]->GetString());
        }
        else
        {
            result += "...";
        }

        return result;
    }
    std::string GetDesc() const override { return "... / ..."; }
};

class Sin : public OpeNodeWithSub<1> 
{
public:
    float SubEvaluate(float t) const override { return std::sin(m_Subnodes[0]->Evaluate(t)); }
    std::string GetString() const override 
    {
        std::string result = "sin(";

        if (m_Subnodes[0] != nullptr)
        {
            result += ToString(m_Subnodes[0]->GetString());
        }
        else
        {
            result += "...";
        }

        result += ")";
        return result;
    }
    std::string GetDesc() const override { return "sin(...)"; }
};

class Cos : public OpeNodeWithSub<1> 
{
public:
    float SubEvaluate(float t) const override { return std::cos(m_Subnodes[0]->Evaluate(t)); }
    std::string GetString() const override 
    {
        std::string result = "cos(";

        if (m_Subnodes[0] != nullptr)
        {
            result += ToString(m_Subnodes[0]->GetString());
        }
        else
        {
            result += "...";
        }

        result += ")";
        return result;
    }
    std::string GetDesc() const override { return "cos(...)"; }
};

class Tan : public OpeNodeWithSub<1> 
{
public:
    float SubEvaluate(float t) const override { return std::tan(m_Subnodes[0]->Evaluate(t)); }
    std::string GetString() const override 
    {
        std::string result = "tan(";

        if (m_Subnodes[0] != nullptr)
        {
            result += ToString(m_Subnodes[0]->GetString());
        }
        else
        {
            result += "...";
        }

        result += ")";
        return result;
    }
    std::string GetDesc() const override { return "tan(...)"; }
};

class ASin : public OpeNodeWithSub<1> 
{
public:
    float SubEvaluate(float t) const override { return std::asin(m_Subnodes[0]->Evaluate(t)); }
    std::string GetString() const override 
    {
        std::string result = "asin(";

        if (m_Subnodes[0] != nullptr)
        {
            result += ToString(m_Subnodes[0]->GetString());
        }
        else
        {
            result += "...";
        }

        result += ")";
        return result;
    }
    std::string GetDesc() const override { return "asin(...)"; }
};

class ACos : public OpeNodeWithSub<1> 
{
public:
    float SubEvaluate(float t) const override { return std::acos(m_Subnodes[0]->Evaluate(t)); }
    std::string GetString() const override 
    {
        std::string result = "acos(";

        if (m_Subnodes[0] != nullptr)
        {
            result += ToString(m_Subnodes[0]->GetString());
        }
        else
        {
            result += "...";
        }

        result += ")";
        return result;
    }
    std::string GetDesc() const override { return "acos(...)"; }
};

class ATan : public OpeNodeWithSub<1> 
{
public:
    float SubEvaluate(float t) const override { return std::atan(m_Subnodes[0]->Evaluate(t)); }
    std::string GetString() const override 
    {
        std::string result = "atan(";

        if (m_Subnodes[0] != nullptr)
        {
            result += ToString(m_Subnodes[0]->GetString());
        }
        else
        {
            result += "...";
        }

        result += ")";
        return result;
    }
    std::string GetDesc() const override { return "atan(...)"; }
};

class Abs : public OpeNodeWithSub<1> 
{
public:
    float SubEvaluate(float t) const override { return std::abs(m_Subnodes[0]->Evaluate(t)); }
    std::string GetString() const override {
        std::string result = "abs(";

        if (m_Subnodes[0] != nullptr)
        {
            result += ToString(m_Subnodes[0]->GetString());
        }
        else
        {
            result += "...";
        }

        result += ")";
        return result;
    }
    std::string GetDesc() const override { return "abs(...)"; }
};

class Exp : public OpeNodeWithSub<1> 
{
public:
    float SubEvaluate(float t) const override { return std::exp(m_Subnodes[0]->Evaluate(t)); }
    std::string GetString() const override 
    {
        std::string result = "exp(";

        if (m_Subnodes[0] != nullptr)
        {
            result += ToString(m_Subnodes[0]->GetString());
        }
        else
        {
            result += "...";
        }

        result += ")";
        return result;
    }
    std::string GetDesc() const override { return "exp(...)"; }
};

class Ln : public OpeNodeWithSub<1> 
{
public:
    float SubEvaluate(float t) const override { return std::log(m_Subnodes[0]->Evaluate(t)); }
    std::string GetString() const override 
    {
        std::string result = "ln(";

        if (m_Subnodes[0] != nullptr)
        {
            result += ToString(m_Subnodes[0]->GetString());
        }
        else
        {
            result += "...";
        }

        result += ")";
        return result;
    }
    std::string GetDesc() const override { return "ln(...)"; }
};

class Log : public OpeNodeWithSub<1> 
{
public:
    float SubEvaluate(float t) const override { return std::log10(m_Subnodes[0]->Evaluate(t)); }
    std::string GetString() const override 
    {
        std::string result = "log(";

        if (m_Subnodes[0] != nullptr)
        {
            result += ToString(m_Subnodes[0]->GetString());
        }
        else
        {
            result += "...";
        }

        result += ")";
        return result;
    }
    std::string GetDesc() const override { return "log(...)"; }
};

class Sqrt : public OpeNodeWithSub<1> 
{
public:
    float SubEvaluate(float t) const override { return std::sqrt(m_Subnodes[0]->Evaluate(t)); }
    std::string GetString() const override 
    {
        std::string result = "sqrt(";

        if (m_Subnodes[0] != nullptr)
        {
            result += ToString(m_Subnodes[0]->GetString());
        }
        else
        {
            result += "...";
        }

        result += ")";
        return result;
    }
    std::string GetDesc() const override { return "sqrt(...)"; }
};

class Max : public OpeNodeWithSub<2> 
{
public:
    float SubEvaluate(float t) const override { return std::fmax(m_Subnodes[0]->Evaluate(t), m_Subnodes[1]->Evaluate(t)); }
    std::string GetString() const override 
    {
        std::string result = "max(";

        if (m_Subnodes[0] != nullptr)
        {
            result += ToString(m_Subnodes[0]->GetString());
        }
        else
        {
            result += "...";
        }

        result += ", ";

        if (m_Subnodes[1] != nullptr)
        {
            result += ToString(m_Subnodes[1]->GetString());
        }
        else
        {
            result += "...";
        }

        result += ")";
        return result;
    }
    std::string GetDesc() const override { return "max(..., ...)"; }
};

class Min : public OpeNodeWithSub<2> 
{
public:
    float SubEvaluate(float t) const override { return std::fmin(m_Subnodes[0]->Evaluate(t), m_Subnodes[1]->Evaluate(t)); }
    std::string GetString() const override 
    {
        std::string result = "min(";

        if (m_Subnodes[0] != nullptr)
        {
            result += ToString(m_Subnodes[0]->GetString());
        }
        else
        {
            result += "...";
        }

        result += ", ";

        if (m_Subnodes[1] != nullptr)
        {
            result += ToString(m_Subnodes[1]->GetString());
        }
        else
        {
            result += "...";
        }

        result += ")";
        return result;
    }
    std::string GetDesc() const override { return "min(..., ...)"; }
};

class Pow : public OpeNodeWithSub<2> 
{
public:
    float SubEvaluate(float t) const override { return std::pow(m_Subnodes[0]->Evaluate(t), m_Subnodes[1]->Evaluate(t)); }
    std::string GetString() const override 
    {
        std::string result = "pow(";

        if (m_Subnodes[0] != nullptr)
        {
            result += ToString(m_Subnodes[0]->GetString());
        }
        else
        {
            result += "...";
        }

        result += ", ";

        if (m_Subnodes[1] != nullptr)
        {
            result += ToString(m_Subnodes[1]->GetString());
        }
        else
        {
            result += "...";
        }

        result += ")";
        return result;
    }
    std::string GetDesc() const override { return "pow(..., ...)"; }
};

}  // namespace Mira
