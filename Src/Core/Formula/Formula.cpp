// Copyright RariumUnlimited - Licence : MIT
#include <Core/Formula/Formula.h>

#include <algorithm>
#include <cctype>
#include <vector>

namespace Mira 
{

Formula::Formula() 
{
    m_Functions["t"] = [](){ return new T(); };
    m_Functions["pi"] = [](){ return new Pi(); };
    m_Functions["+"] = [](){ return new Add(); };
    m_Functions["*"] = [](){ return new Mult(); };
    m_Functions["-"] = [](){ return new Sub(); };
    m_Functions["/"] = [](){ return new Div(); };
    m_Functions["sin"] = [](){ return new Sin(); };
    m_Functions["cos"] = [](){ return new Cos(); };
    m_Functions["tan"] = [](){ return new Tan(); };
    m_Functions["asin"] = [](){ return new ASin(); };
    m_Functions["acos"] = [](){ return new ACos(); };
    m_Functions["atan"] = [](){ return new ATan(); };
    m_Functions["abs"] = [](){ return new Abs(); };
    m_Functions["exp"] = [](){ return new Exp(); };
    m_Functions["ln"] = [](){ return new Ln(); };
    m_Functions["log"] = [](){ return new Log(); };
    m_Functions["sqrt"] = [](){ return new Sqrt(); };
    m_Functions["max"] = [](){ return new Max(); };
    m_Functions["min"] = [](){ return new Min(); };
    m_Functions["pow"] = [](){ return new Pow(); };

    m_LetterConstant.insert("t");
    m_LetterConstant.insert("pi");

    m_SimpleOperation.insert('*');
    m_SimpleOperation.insert('/');
    m_SimpleOperation.insert('+');
    m_SimpleOperation.insert('-');
}

Formula::~Formula() 
{
    if (m_Node != nullptr)
    {
        delete m_Node;
    }
}

void Formula::SetFormulaNode(OpeNode* newNode) 
{
    if (m_Node != nullptr)
    {
        delete m_Node;
    }
    m_Node = newNode;
}

bool Formula::GetNumber(std::string formula, float& result) 
{
    if (formula.size() == 0)
    {
        return false;
    }

    std::string number = "";
    bool foundDot = false;

    int i = 0;
    while (i < formula.size()) 
    {
        char current = formula[i];

        if (!std::isdigit(current) && current != '.')
        {
            return false;
        }

        if (current == '.' && !foundDot)
        {
            foundDot = true;
        }
        else if (current == '.' && foundDot)
        {
            return false;
        }

        number += formula[i];
        ++i;
    }

    if (std::isdigit(number[0]) && std::isdigit(number[number.size() - 1])) 
    {
        result = std::stof(number);
        return true;
    } 
    else 
    {
        return false;
    }
}

bool Formula::IsCharSimpleOperation(char c) 
{
    return c == '+' || c == '*' || c == '-' || c == '/';
}

OpeNode* Formula::InterpretInsideParenthesis(std::string formula) 
{
    if (formula.size() > 2 && formula[0] == '(' && formula.back() == ')')
    {
        return InterpretInternal(formula.substr(1, formula.size() - 2));
    }
    else
    {
        return nullptr;
    }
}

OpeNode* Formula::InterpretFunctionOperator(std::string formula) 
{
    if (formula.size() == 0)
    {
        return nullptr;
    }

    OpeNode* result = nullptr;

    int i = 0;
    std::string functionName = "";

    while (i < formula.size()) 
    {
        if (std::isalnum(formula[i])) 
        {
            functionName += formula[i];
            ++i;
        } 
        else 
        {
            break;
        }
    }

    functionName = formula.substr(0, i);

    auto it = m_Functions.find(functionName);
    if (it != m_Functions.end() && formula.size() > i) 
    {
        std::vector<OpeNode*> params;

        if (formula[i] == '(') 
        {
            int count = 1;

            std::string formulaParam = "";

            for (++i; i < formula.size(); ++i) 
            {
                if (formula[i] == '(') 
                {
                    ++count;
                    formulaParam += formula[i];
                } 
                else if (formula[i] == ')') 
                {
                    --count;

                    if (count == 0) 
                    {
                        OpeNode* param = InterpretInternal(formulaParam);

                        if (param == nullptr) 
                        {
                            for (OpeNode* node : params)
                                delete node;
                            return nullptr;
                        } 
                        else 
                        {
                            params.push_back(param);
                        }
                        break;
                    } 
                    else 
                    {
                        formulaParam += formula[i];
                    }
                } 
                else if (formula[i] == ',' && count == 1) 
                {
                    OpeNode* param = InterpretInternal(formulaParam);
                    if (param == nullptr) 
                    {
                        for (OpeNode* node : params)
                        {
                            delete node;
                        }
                        return nullptr;
                    } 
                    else 
                    {
                        params.push_back(param);
                        formulaParam = "";
                    }
                } 
                else 
                {
                    formulaParam += formula[i];
                }
            }  // For for params inside () of function

            if (params.size() > 0) 
            {
                result = it->second();
                if (result->GetSubNodesCount() != params.size()) 
                {
                    delete result;
                    result = nullptr;
                    for (OpeNode* node : params)
                    {
                        delete node;
                    }
                } 
                else 
                {
                    for (int i = 0; i < params.size(); ++i) 
                    {
                        result->SetSubnode(i, params[i]);
                    }
                }
            }
        }  // If next char is (
    }  // If function exist in map

    return result;
}

int Formula::GetNextHighPriorityOperationIndex(const std::vector<std::string>& operations) 
{
    for (int i = 0; i < operations.size(); ++i)
    {
        if (operations[i] == "*" || operations[i] == "/")
        {
            return i;
        }
    }
    return -1;
}

OpeNode* Formula::InterpretOperand(std::string operand) 
{
    if (operand.size() == 0)
    {
        return nullptr;
    }

    OpeNode* result = nullptr;

    float resultNumber;

    if (operand[0] == '(') 
    {
        // Parenthesis
        result = InterpretInsideParenthesis(operand);
    } 
    else if (GetNumber(operand, resultNumber)) 
    {
        // Float
        result = new C(resultNumber);
    } 
    else if (m_LetterConstant.find(operand) != m_LetterConstant.end()) 
    {
        // Letter constat (t, pi, ...)
        auto it = m_Functions.find(operand);
        if (it != m_Functions.end())
        {
            result = it->second();
        }
    } 
    else 
    {
        // Function
        result = InterpretFunctionOperator(operand);
    }

    return result;
}

OpeNode* Formula::InterpretInternal(std::string formula) 
{
    std::string currentStr = formula;

    if (currentStr.size() == 0)
    {
        return nullptr;
    }

    std::vector<std::string> operands;
    std::vector<std::string> operations;

    std::string currentOperand = "";

    /* Split operand and operation
     * Example is better than long explanation
     * Formula : sin(t) * 3 + 4
     * operands = [sin(t), 3, 4]
     * operations = [*, +]
     */
    int i = 0;
    int parenthesisLevel = 0;
    while (i < currentStr.size()) 
    {
        char currentChar = currentStr[i];
        if (m_SimpleOperation.find(currentChar) != m_SimpleOperation.end() && parenthesisLevel == 0) 
        {
            operands.push_back(currentOperand);
            operations.push_back(currentStr.substr(i, 1));
            currentOperand = "";
        } 
        else 
        {
            currentOperand += currentChar;
            if (currentChar == '(') 
            {
                ++parenthesisLevel;
            } 
            else if (currentChar == ')') 
            {
                --parenthesisLevel;
                if (parenthesisLevel < 0)
                {
                    return nullptr;
                }
            }
        }
        ++i;
    }

    operands.push_back(currentOperand);

    if (operands.size() != operations.size() + 1)
    {
        return nullptr;
    }

    if (operands.size() == 1)
    {
        return InterpretOperand(operands[0]);
    }

    // Construct Nodes corresponding to operands and operations
    std::vector<OpeNode*> operandNodes;
    std::vector<OpeNode*> operationNodes;

    for (const std::string& operandStr : operands) 
    {
        OpeNode* operandNode = InterpretInternal(operandStr);
        if (operandNode == nullptr) 
        {
            for (OpeNode* node : operandNodes)
            {
                delete node;
            }
            return nullptr;
        } 
        else 
        {
            operandNodes.push_back(operandNode);
        }
    }

    for (const std::string& operationStr : operations) 
    {
        auto it = m_Functions.find(operationStr);
        if (it == m_Functions.end()) 
        {
            for (OpeNode* node : operandNodes)
            {
                delete node;
            }
            for (OpeNode* node : operationNodes)
            {
                delete node;
            }
            return nullptr;
        } 
        else 
        {
            operationNodes.push_back(it->second());
        }
    }

    /* Fuse operand and operation depending on operation priority
     * Init
     * operandNodes = [sin(t), 3, 4]
     * operationNodes = [*, +]
     * After fusing
     * operandNodes = [sin(t) * 3, 4]
     * operationNodes = [+]
     */

    std::function<void(int)> fuseOperand = [&](int index) 
    {
        OpeNode* operationNode = operationNodes[index];
        OpeNode* leftOperandNode = operandNodes[index];
        OpeNode* rightOperandNode = operandNodes[index + 1];
        operationNode->SetSubnode(0, leftOperandNode);
        operationNode->SetSubnode(1, rightOperandNode);
        operandNodes[index] = operationNode;
        operandNodes.erase(operandNodes.begin() + index + 1);
        operationNodes.erase(operationNodes.begin() + index);
        operands.erase(operands.begin() + index + 1);
        operations.erase(operations.begin() + index);
    };

    int highPriorityOperationIndex = GetNextHighPriorityOperationIndex(operations);
    while (highPriorityOperationIndex != -1 && operations.size() > 0) 
    {
        fuseOperand(highPriorityOperationIndex);
        highPriorityOperationIndex = GetNextHighPriorityOperationIndex(operations);
    }

    /* Fuse remaining operand and operation
     * Init
     * operandNodes = [sin(t) * 3, 4]
     * operationNodes = [+]
     * After fusing
     * operandNodes = [(sin(t) * 3) + 2]
     */

    while (operations.size() > 0) 
    {
        fuseOperand(0);
    }

    if (operandNodes.size() != 1) 
    {
        for (OpeNode* node : operandNodes)
        {
            delete node;
        }
        for (OpeNode* node : operationNodes)
        {
            delete node;
        }
        return nullptr;
    }

    // Return the only remaining node :)
    return operandNodes[0];
}

bool Formula::Interpret(std::string formula) 
{
    std::string formulaCleaned = formula;
    std::size_t spacePosition = formulaCleaned.find(' ');
    while (spacePosition != std::string::npos) 
    {
        formulaCleaned.erase(spacePosition, 1);
        spacePosition = formulaCleaned.find(' ');
    }

    std::transform(formulaCleaned.begin(), formulaCleaned.end(), formulaCleaned.begin(),
        [](unsigned char c){ return std::tolower(c); });

    OpeNode* newNode = InterpretInternal(formulaCleaned);

    if (newNode != nullptr && newNode->Validate()) 
    {
        SetFormulaNode(newNode);
        return true;
    } 
    else 
    {
        return false;
    }
}

}  // namespace Mira
