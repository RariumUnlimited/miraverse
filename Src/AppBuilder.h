// Copyright RariumUnlimited - Licence : MIT
#pragma once

#include <memory>
#include <vector>

#include <Core/Application/Application.h>

namespace Mira {

class AppBuilder 
{
public:
    void GetAllApplication(std::vector<std::unique_ptr<Application>>& applicationList);
};

}  // namespace Mira
