Miraverse
=========

About
-----

A collection of some of my in c++/SFML stuff.


Build
-----

- Install [SFML 3](https://www.sfml-dev.org/download/sfml/3.0.0/)
- Clone using the `--recursive` flag to grab the dependencies

```
mkdir build
cd build
cmake ..
make
./Mira
```

CMake Tips
----------

Use -T ClangCL to use Clang Visual Studio build tools.


Misc. Infos
-------------------

Compilation and testing is sometimes done on a Debian Bullseye using gcc, sometimes on Windows with MSVC or Clang.
Last compilation test : MSVC.

Trivia
------

Mira is a reference to the planet in Xenoblade Chronicle X and to the Mirari an artifact from Magic the Gathering.

License
-------

All Miraverse source code is provided under the MIT license (See LICENSE file)



